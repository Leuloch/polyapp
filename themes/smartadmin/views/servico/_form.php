<?php
/* @var $this ServicoController */
/* @var $model Servico */
/* @var $form BSActiveForm */
/* @var $instrumento Instrumento */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'servico-form',
    'enableAjaxValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>
<fieldset>
    <legend>Descrição</legend>
    <?php echo $form->textFieldControlGroup($model,'nome_servico',array('maxlength'=>255)); ?>
    <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'instrumento-grid',
                        // @todo Filtrar o dataProvider para que exiba apenas os instrumentos sem serviço definido
                        // @todo Definir se vai continuar com a opção de registrar vários serviços para um mesmo instrumento (ou criar uma categoria de serviços, e.g. calibração, manutenção, etc)
			'dataProvider'=>$instrumento->search(),
			'filter'=>$instrumento,
			'columns'=>array(
                            array(
                                'class'=>'CCheckBoxColumn'
                            ),
                            array(
                                'header' => 'Categoria',
                                'name'=>'categoria_instrumento_id',
                                'value'=>'$data->categoriaInstrumento->nome'
                            ),
                            'nome',
                            //mais detalhes sobre essa configuração veja YiiBook pg301
                            array(
                                'header' => 'Fabricante',
                                'name'=>'fabricante_id',
                                'value'=>'$data->fabricante->nome_fabricante'
                            ),
                            'modelo',
                            array(
                                'header'=>'Sistemas de Medição',
                                'type'=>'raw',
                                'value'=>'$data->listData()',
                            ),
                        ),
                        'type'=>array(BsHtml::GRID_TYPE_BORDERED, BsHtml::GRID_TYPE_CONDENSED),
                        'ajaxUpdate'=>true, //pode usar falso caso a versão do jQuery dê problemas
                        'emptyTagName'=>'tr',
                        'emptyText'=>'<td class="dataTables_empty" valign="top" colspan="7"><i class="fa fa-warning text-warning"></i> Nenhum instrumento localizado.</td>',
                        'selectableRows'=>10,
                        'htmlOptions'=>array('class'=>'table-responsive'),
        )); ?>
</fieldset>
<fieldset>
    <legend>Valores</legend>
    <?php echo $form->textFieldControlGroup($model,'custo',array('maxlength'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'valor',array('maxlength'=>6)); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size'=>  BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>

<?php $this->endWidget(); ?>
