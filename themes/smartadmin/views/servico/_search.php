<?php
/* @var $this ServicoController */
/* @var $model Servico */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome_servico',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'instrumento_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'custo',array('maxlength'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'valor',array('maxlength'=>6)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
