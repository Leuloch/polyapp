<?php
/* @var $this ServicoController */
/* @var $model Servico */
/* @var $instrumento Instrumento */
?>

<?php
$this->breadcrumbs=array(
	'Servicos'=>array('index'),
	'Novo',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Servicos', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Servico') ?>

<?php

$this->widget('application.widgets.JarvisTabs', array(
    'tabs' => array(
        '<i class="fa fa-lg fa-list-alt"></i><span class="hidden-mobile hidden-tablet"> Serviço</span>' => array(
            'content' => $this->renderPartial(
                    '_form',
                    array('model'=>$model, 'instrumento'=>$instrumento),
                    true
                    ),
            'id'=>'instrumento'
        ),
    ),
    //'defaultTab'=>$defaultTab,
    'id' => 'servico-create-01',
    'articleOptions'=>array('class'=>'col-sm-12 col-md-12 col-lg-12'),
    'htmlOptions'=>array(
        JvHtml::DATA_WIDGET_COLORBUTTON => 'false',
        JvHtml::DATA_WIDGET_EDITBUTTON => 'false',
        JvHtml::DATA_WIDGET_CUSTOMBUTTON => 'false',
        JvHtml::DATA_WIDGET_DELETEBUTTON => 'false',
    ),
    'buttonLabel'=>'<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Opções</span>',
    'buttons'=>$this->menu,
    'buttonOptions'=>array(
        'split' => false,
        'size' => BsHtml::BUTTON_SIZE_SMALL,
        'color' => BsHtml::BUTTON_COLOR_PRIMARY,
        'dropup' => false,
        'menuOptions'=>array(
            'pull'=>  BsHtml::PULL_RIGHT,
        ),
        'url'=>'#',
    ),
));
?>