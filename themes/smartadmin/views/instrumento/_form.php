<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('DynamicTabularForm', array(
    'id'=>'instrumento-form',
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'defaultRowView'=>'sistemaMedicao/_rowSistemaMedicao',
    'focus'=>array($model, 'nome'),
)); ?>

<fieldset>
    <legend>Dados do Instrumento</legend>
    <?php echo $form->errorSummary($model); ?>
    <div class="form-group">
        <label class="control-label col-md-2">
            Categoria
        </label>
        <div class="col-md-5">
            <?php echo $form->dropDownList($model, 'categoria_instrumento_id', CHtml::listData(CategoriaInstrumento::model()->findAll(),'id','nome'), array('prompt'=>'Selecione uma Categoria')); ?>
        </div>
        <div class="col-md-5">
            <?php
                echo BsHtml::ajaxButton(
                    '<i class="fa fa-plus"></i>', 
                    $this->createUrl('categoriaInstrumento/addnew'), 
                    array(
                        'onclick' => '$("#categoriaDialog").dialog("open"); return false;',
                        'update' => '#categoriaDialog',
                    ),
                    array(
                        'id'=>'showCategoriaDialog',
                        'class' => 'btn-success'
                    ));
            ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'nome',array(
        'maxlength'=>100,
        'groupOptions'=>array('class'=>'col-md-12'),
        'labelOptions'=>array('class'=>'col-md-2'),
        'controlOptions'=>array('class'=>'col-md-10'),
    )); ?>
    <div class="form-group">
        <label class="control-label col-md-2">
            Fabricante
        </label>
        <div class="col-md-5">
            <?php echo $form->dropDownList($model, 'fabricante_id', CHtml::listData(Fabricante::model()->findAll(),'id','nome_fabricante'), array('prompt'=>'Selecione Fabricante')); ?>
        </div>
        <div class="col-md-5">
            <?php
                echo BsHtml::ajaxButton(
                    '<i class="fa fa-plus"></i>', 
                    $this->createUrl('fabricante/addnew'), 
                    array(
                        'onclick' => '$("#fabricanteDialog").dialog("open"); return false;',
                        'update' => '#fabricanteDialog',
                    ),
                    array(
                        'id'=>'showFabricanteDialog',
                        'class' => 'btn-success'
                    ));
            ?>
        </div>
    </div>
    <div id="fabricanteDialog"></div>
    <div id="categoriaDialog"></div>
    <?php echo $form->textFieldControlGroup($model,'modelo',array(
        'maxlength'=>50,
        'groupOptions'=>array('class'=>'col-md-12'),
        'labelOptions'=>array('class'=>'col-md-2'),
        'controlOptions'=>array('class'=>'col-md-10'),
    )); ?>
    <?php echo $form->textAreaControlGroup($model,'tag',array(
        'rows'=>6,
        'groupOptions'=>array('class'=>'col-md-12'),
        'labelOptions'=>array('class'=>'col-md-2'),
        'controlOptions'=>array('class'=>'col-md-10'),
    )); ?>
</fieldset>
<div id="unidadeDialog"></div>
<fieldset>
    <?php echo $form->rowForm($sistemaMedicoes,null,array(),'<i class="fa fa-plus-square"></i><span class="hidden-mobile hidden-tablet"> Adicionar Sistema Medição</span>', array('class'=>'btn-primary btn-block')); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget(); ?>
