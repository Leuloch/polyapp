<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */


$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	'Lista',
);
/*
 * @todo Inserir CRUD da tabela "Grandeza" no menu.
 */
$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Instrumento', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
// add the script to toggle the field list
$('.fields-button').click(function(){
        $('.fields-form').toggle();
        return false;
});
$('.search-form form').submit(function(){
	$('#instrumento-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('admin-instrumento-01', '<strong>Lista</strong><i> Instrumentos</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

    <div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Adicionar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'create',
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
            <?php echo BsHtml::button('<i class="fa fa-cogs"></i><span class="hidden-mobile hidden-tablet"> Colunas</span>', array('class' => 'fields-button', 'color'=>BsHtml::BUTTON_COLOR_INFO), '#'); ?>
    </div>
    <div class="search-form" style="display:none">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
    </div>
    <!-- search-form -->
    <div class="fields-form" style="display:none">
        <?php
        $this->renderPartial('_fields', array(
            'model' => $model,
            'columns' => $columns,
        ));
        ?>
    </div>
    
    <div class="row">
            <div class="col-md-12">
            <?php
            // here we create an array of columns for the grid view
            $modelColumns=$model->getMetaData()->columns;
            $myColumnsOptions = array(
                'id' => array(
                    'name'=>'id',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'fabricante_id' => array(
                    'header' => 'Fabricante',
                    'name'=>'fabricante_id',
                    'value'=>'$data->fabricante->nome_fabricante',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'categoria_instrumento_id' => array(
                    'header' => 'Categoria',
                    'name'=>'categoria_instrumento_id',
                    'value'=>'$data->categoriaInstrumento->nome',
                    'htmlOptions'=>array('width'=>'200'),
                ),
                'nome' => array(
                    'name'=>'nome',
                    'htmlOptions'=>array('width'=>'240'),
                ),
                'tag' => array(
                    'name'=>'tag',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'modelo' => array(
                    'name'=>'modelo',
                    'htmlOptions'=>array('width'=>'120'),
                ),
            );
            
            foreach ($myColumnsOptions AS $columnAttribute => $columnOptions) {
                if (in_array($columnAttribute, $columns)) {
                    $key=array_search($columnAttribute,$columns);
                    $columns[$key] = $columnOptions;
                }
            }
            
            $columns[]=array(
                            'class'=>'bootstrap.widgets.BsButtonColumn',
                            'htmlOptions'=>array('width'=>'145px'),
                            'viewButtonOptions'=>array('class'=>'btn btn-xs txt-color-green'),
                            'updateButtonOptions'=>array('class'=>'btn btn-xs txt-color-blue'),
                            'deleteButtonOptions'=>array('class'=>'btn btn-xs txt-color-red'),
                            'header'=>BsHtml::dropDownList(
                                    'pageSize',
                                    $pageSize,
                                    array(10=>10,20=>20,50=>50,100=>100), 
                                    array('onchange'=>"$.fn.yiiGridView.update('file-grid',{ data:{pageSize: $(this).val() }})")),
            );
            ?>
            </div>
        </div>

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'instrumento-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>$columns,
                        'type'=>array(BsHtml::GRID_TYPE_BORDERED, BsHtml::GRID_TYPE_CONDENSED),
                        'ajaxUpdate'=>true, //pode usar falso caso a versão do jQuery dê problemas
                        'emptyTagName'=>'tr',
                        'emptyText'=>'<td class="dataTables_empty" valign="top" colspan="7"><i class="fa fa-warning text-warning"></i> Nenhum resultado encontrado</td>',
                        'enableHistory'=>true, //??? não entendi!
                        'selectableRows'=>0,
                        'htmlOptions'=>array('class'=>'table-responsive'),
        )); ?>
    </div>
</div>

<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>



