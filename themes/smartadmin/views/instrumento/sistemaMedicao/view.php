<?php
/* @var $this SistemaMedicaoController */
/* @var $model SistemaMedicao */
?>

<?php
$this->breadcrumbs=array(
	'Sistema Medicaos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SistemaMedicao', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SistemaMedicao', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update SistemaMedicao', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete SistemaMedicao', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SistemaMedicao', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','SistemaMedicao '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome_sistema',
		'escala_min',
		'escala_max',
		'unidade_escala_id',
		'menor_divisao',
		'unidade_div_id',
		'instrumento_id',
	),
)); ?>