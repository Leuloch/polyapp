<?php $row_id = "sistemaMedicao-" . $key ?>
<div id="<?php echo $row_id; ?>">
    <?php 
        echo $form->hiddenField($model, "[$key]id");
        echo $form->updateTypeField($model, $key, "updateType", array('key' => $key));
    ?>
    <fieldset>
        <legend>Sistema Medição  <em><?php echo $model->nome_sistema; ?></em>
            <?php echo $form->deleteRowButton($row_id, $key, '<i class="fa fa-trash-o"></i><span class="hidden-mobile hidden-tablet"> Excluir</span>', array('class' => 'btn-danger btn-xs pull-right')); ?>
        </legend>
        <?php echo $form->errorSummary($model); ?>
        <?php echo $form->textFieldControlGroup($model,"[$key]nome_sistema",array(
            'maxlength'=>100,
            'groupOptions'=>array('class'=>'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-2'),
            'controlOptions'=>array('class'=>'col-md-10'),
        )); ?>
        <div class="col-md-12 form-group">
            <label class="col-md-2 control-label" for="Contato_tel_comercial">
                Capacidade Medição
            </label>
            <div class="col-md-3" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
                <?php echo $form->textField($model,"[$key]escala_min", array('maxlength'=>45)); ?>
                <?php echo $form->error($model, "[$key]escala_min"); ?>
            </div>
            <div class="col-md-3" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
                <?php echo $form->textField($model,"[$key]escala_max", array('maxlength'=>45)); ?>
                <?php echo $form->error($model, "[$key]escala_max"); ?>
            </div>
            <div class="col-md-3">
                <?php echo $form->dropDownList($model,"[$key]unidade_escala_id", CHtml::listData(Unidade::model()->findAll(),'id','simbolo'), array('prompt'=>'Unidade')); ?>
                <?php echo $form->error($model, "[$key]unidade_escala_id"); ?>
            </div>
            <div class="col-md-1">
                <?php
                    echo BsHtml::ajaxButton(
                        '<i class="fa fa-plus"></i>', 
                        $this->createUrl('unidade/addnew'), 
                        array(
                            'onclick' => '$("#unidadeDialog").dialog("open"); return false;',
                            'update' => '#unidadeDialog',
                        ),
                        array(
                            'id'=>$key .'_showUnidadeDialog',
                            'class' => 'btn-success'
                        ));
                ?>
            </div>
        </div>
        <div class="col-md-12 form-group">
            <label class="col-md-2 control-label" for="Contato_tel_comercial">
                Menor Divisão
            </label>
            <div class="col-md-3">
                <?php echo $form->textField($model,"[$key]menor_divisao", array('maxlength'=>45)); ?>
                <?php echo $form->error($model, "[$key]menor_divisao"); ?>
            </div>
            <label class="col-md-3 control-label" for="Contato_tel_comercial">
                Unidade
            </label>
            <div class="col-md-4">
                <?php echo $form->dropDownList($model,"[$key]unidade_div_id", CHtml::listData(Unidade::model()->findAll(),'id','simbolo'), array('prompt'=>'Unidade')); ?>
                <?php echo $form->error($model, "[$key]unidade_div_id"); ?>
            </div>
        </div>
    </fieldset>
</div>

