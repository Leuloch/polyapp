<?php
/* @var $this SistemaMedicaoController */
/* @var $sistemaMedicoes SistemaMedicao */
/* @var $instrumento Instrumento */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('DynamicTabularForm', array(
    'id'=>'sistema-medicao-form',
    'enableAjaxValidation'=>true,
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'defaultRowView'=>'sistemaMedicao/_rowSistemaMedicao',
)); ?>

    <?php echo $form->hiddenField($instrumento,'id'); ?>
    <?php echo $form->rowForm($sistemaMedicoes,null,array(),'<i class="fa fa-plus-square"></i><span class="hidden-mobile hidden-tablet"> Adicionar Sistema Medição</span>', array('class'=>'btn-primary btn-block')); ?>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget(); ?>
