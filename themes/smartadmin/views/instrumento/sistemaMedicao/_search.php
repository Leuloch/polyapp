<?php
/* @var $this SistemaMedicaoController */
/* @var $model SistemaMedicao */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome_sistema',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'escala_min',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'escala_max',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'unidade_escala_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'menor_divisao',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'unidade_div_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'instrumento_id'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
