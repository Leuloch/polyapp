<?php
/* @var $this SistemaMedicaoController */
/* @var $model SistemaMedicao */
?>

<?php
$this->breadcrumbs=array(
	'Sistema Medicaos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SistemaMedicao', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SistemaMedicao', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','SistemaMedicao') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>