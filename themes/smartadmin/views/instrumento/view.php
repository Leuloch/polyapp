<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */
?>

<?php
$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	$model->nome,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Instrumento', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-edit','label'=>'Editar', 'url'=>array('update', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Excluir', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Instrumentos', 'url'=>array('admin')),
);
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('view-instrumento-01', '<strong>Visualizar</strong><i> Instrumento</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-edit"></i><span class="hidden-mobile hidden-tablet"> Editar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'update/' . $model->id,
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
</div>
<?php echo BsHtml::pageHeader('Visualização',$model->nome) ?>

<?php $this->widget('bootstrap.widgets.BsDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fabricante_id',
		'nome',
		'tag',
		'modelo',
	),
)); ?>
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>
