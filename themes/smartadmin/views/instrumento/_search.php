<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'fabricante_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>100)); ?>
    <?php echo $form->textAreaControlGroup($model,'tag',array('rows'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'modelo',array('maxlength'=>50)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
