<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form BSActiveForm */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
	'id' => 'order-form',
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
	'enableAjaxValidation' => true,
));
?>

	<?php echo $form->errorSummary($model); ?>

        <fieldset>
            <legend>Entrada de Instrumentos</legend>
                <div class="col-md-12 form-group">
                    <label class="col-md-2 control-label" for="Order_cliente_id">
                        <?php echo $model->getAttributeLabel('cliente_id'); ?>
                    </label>
                    <div class="col-md-10">
                        <?php echo Select2::activeDropDownList($model, 'cliente_id', CHtml::listData(Empresa::model()->findAll(),'id','nome'), array(
                            'style'=>'width:100%',
                            'class'=>'select2',
                            'prompt'=>'', //para funcionar o placeholder, deve colocar um prompt vazio!
                            'placeholder'=>'Selecione Cliente'
                        )); ?>
                        <?php echo $form->error($model, 'cliente_id'); ?>
                    </div>
                </div>
                <?php echo $form->textAreaControlGroup($model,'observacao',array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>
                <?php echo $form->dropDownListControlGroup($model,'forma_entrada', $model->getFormaEntrada(),array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>
                <?php echo $form->textFieldControlGroup($model,'nota_fiscal',array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>
                <?php echo $form->textFieldControlGroup($model,'data_entrada',array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>
                <?php echo $form->textFieldControlGroup($model,'data_prev_saida',array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>
                <?php echo $form->textFieldControlGroup($model,'data_disponibilidade',array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>
                <?php echo $form->textFieldControlGroup($model,'data_retirada',array(
                    'groupOptions'=>array(
                        'class'=>'col-md-12',
                    ),
                    'labelOptions'=>array(
                        'class'=>'col-md-2',
                    ),
                    'controlOptions'=>array(
                        'class'=>'col-md-10',
                    ),
                )); ?>

		
        </fieldset>
        <div class="form-actions">
            <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
        </div>

<?php $this->endWidget(); ?> <!-- form -->