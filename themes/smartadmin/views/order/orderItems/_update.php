<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $orderItems array of OrderItems Model */
/* @var $form BSActiveForm */
?>

<?php $form = $this->beginWidget('DynamicTabularForm', array(
	'id' => 'order-items-form',
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
	'enableAjaxValidation' => true,
        'defaultRowView'=>'_orderItemsForm'
));
?>

	<?php echo $form->hiddenField($model, "id"); ?>
<?php echo $form->rowForm($orderItems,null,array(),'<i class="fa fa-plus-square"></i><span class="hidden-mobile hidden-tablet"> Adicionar Endereco</span>', array('class'=>'btn-primary btn-block')); ?>

<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget() ?>