<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>
        <?php echo $form->textFieldControlGroup($model,'id'); ?>
        <?php echo $form->textFieldControlGroup($model,'cliente_id'); ?>
        <?php echo $form->textFieldControlGroup($model,'contato_id'); ?>
        <?php echo $form->textFieldControlGroup($model,'status'); ?>
        <?php echo $form->textFieldControlGroup($model,'nota_fiscal'); ?>
        <?php echo $form->textFieldControlGroup($model,'data_entrada'); ?>
        <?php echo $form->textFieldControlGroup($model,'data_prev_saida'); ?>
        <?php echo $form->textFieldControlGroup($model,'data_disponibilidade'); ?>
        <?php echo $form->textFieldControlGroup($model,'data_retirada'); ?>

	<div class="form-actions">
            <?php echo BsHtml::submitButton('Pesquisar',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
        </div>

<?php $this->endWidget(); ?>
