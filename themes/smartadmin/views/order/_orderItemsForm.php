<?php $row_id = "orderItems-" . $key ?>
<div id="<?php echo $row_id; ?>">
<?php
    echo $form->hiddenField($model, "[$key]id");
    echo $form->updateTypeField($model, $key, "updateType", array('key' => $key));
    ?>
<fieldset>
    <legend>Instrumentos <?php echo $form->deleteRowButton($row_id, $key, '<i class="fa fa-trash-o"></i><span class="hidden-mobile hidden-tablet"> Excluir</span>', array('class' => 'btn-danger pull-right')); ?></legend>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]quantidade",array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->dropDownListControlGroup($model, "[$key]instrumento_id",CHtml::listData(Instrumento::model()->findAll(),'id',function ($data){return $data->nome . " - " . $data->tag;}), array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )) ; ?>
    <?php echo $form->textAreaControlGroup($model,"[$key]descricao",array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
</fieldset>
</div>