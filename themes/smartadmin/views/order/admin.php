<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $columns array */
/* @var $pageSize int */

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Listar'),
);

$this->menu = array(
		array('icon' => 'glyphicon glyphicon-plus-sign','label'=>Yii::t('app', 'Nova Entrada'), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('order-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('admin-order-01', '<strong>Lista</strong><i> Entrada de Instrumentos</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Adicionar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>Yii::app()->createUrl('order/create'),
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
            <?php //echo BsHtml::button('<i class="fa fa-cogs"></i><span class="hidden-mobile hidden-tablet"> Colunas</span>', array('class' => 'fields-button', 'color'=>BsHtml::BUTTON_COLOR_INFO), '#'); ?>
    </div>
<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id' => 'order-grid',
        'type'=>array(BsHtml::GRID_TYPE_BORDERED, BsHtml::GRID_TYPE_CONDENSED),
	'dataProvider' => $model->search(),
	'filter' => $model,
        'ajaxUpdate'=>true, //pode usar falso caso a versão do jQuery dê problemas
        'emptyTagName'=>'tr',
        'emptyText'=>'<td class="dataTables_empty" valign="top" colspan="7"><i class="fa fa-warning text-warning"></i> Nenhum resultado encontrado</td>',
        'enableHistory'=>true, //??? não entendi!
        'selectableRows'=>0,
        'htmlOptions'=>array('class'=>'table-responsive'),
	'columns' => array(
		'id',
		//'data_criacao',
		//'data_atualizacao',
		//'usuario_criacao',
		//'usuario_atualizacao',
		'cliente_id',
		//'contato_id',
		'status',
		//'observacao',
		'forma_entrada',
		'nota_fiscal',
		'data_entrada',
		//'data_prev_saida',
		//'data_disponibilidade',
		'data_retirada',
		array(
			'class'=>'bootstrap.widgets.BsButtonColumn',
                        'htmlOptions'=>array('width'=>'100px'),
                        'viewButtonOptions'=>array('class'=>'btn btn-xs txt-color-green'),
                        'updateButtonOptions'=>array('class'=>'btn btn-xs txt-color-blue'),
                        'deleteButtonOptions'=>array('class'=>'btn btn-xs txt-color-red'),
		),
	),
)); ?>

<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>