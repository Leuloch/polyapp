<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('data_criacao')); ?>:
	<?php echo GxHtml::encode($data->data_criacao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('data_atualizacao')); ?>:
	<?php echo GxHtml::encode($data->data_atualizacao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usuario_criacao')); ?>:
	<?php echo GxHtml::encode($data->usuario_criacao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('usuario_atualizacao')); ?>:
	<?php echo GxHtml::encode($data->usuario_atualizacao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('cliente_id')); ?>:
	<?php echo GxHtml::encode($data->cliente_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contato_id')); ?>:
	<?php echo GxHtml::encode($data->contato_id); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('observacao')); ?>:
	<?php echo GxHtml::encode($data->observacao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('forma_entrada')); ?>:
	<?php echo GxHtml::encode($data->forma_entrada); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nota_fiscal')); ?>:
	<?php echo GxHtml::encode($data->nota_fiscal); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('data_entrada')); ?>:
	<?php echo GxHtml::encode($data->data_entrada); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('data_prev_saida')); ?>:
	<?php echo GxHtml::encode($data->data_prev_saida); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('data_disponibilidade')); ?>:
	<?php echo GxHtml::encode($data->data_disponibilidade); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('data_retirada')); ?>:
	<?php echo GxHtml::encode($data->data_retirada); ?>
	<br />
	*/ ?>

</div>