<?php
/* @var $this OrderController */
/* @var $model Order */
/* @var $orderItems array of OrderItems Model */
/* @var $defaultTab Name of the default tab to be rendered */
?>
<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	$model->id => array('view', 'id' => $model->id),
	'Editar',
);

$this->menu = array(
	array('icon' => 'glyphicon glyphicon-plus-sign', 'label' => Yii::t('app', 'Nova Entrada Instrumentos'), 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-list-alt', 'label' => Yii::t('app', 'Visualizar OS') . ' ' . $model->id, 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
	array('icon' => 'glyphicon glyphicon-tasks', 'label' => Yii::t('app', 'Listar Entradas'), 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Editar ',$model->id) ?>
<?php if(Yii::app()->user->hasFlash('inserted')):?>
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-check"></i>
        <strong>Parabéns</strong> <?php echo Yii::app()->user->getFlash('inserted'); ?>
        <a href="javascript:void(0);" class="btn btn-primary btn-xs">Adicionar Mais Empresas  <i class="fa fa-arrow-right"></i></a>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-check"></i>
        <strong>Alterações Salvas</strong> <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('warning')):?>
    <div class="alert alert-warning fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-warning"></i>
        <strong>Atenção</strong> <?php echo Yii::app()->user->getFlash('warning'); ?>
    </div>
<?php endif; ?>

<?php

$this->widget('application.widgets.JarvisTabs', array(
    'tabs' => array(
        '<i class="fa fa-lg fa-arrow-circle-down"></i><span class="hidden-mobile hidden-tablet"> Entrada</span>' => array(
            'content' => $this->renderPartial(
                    '_form',
                    array('model'=>$model),
                    true
                    ),
            'id'=>'order'
        ),
        '<i class="fa fa-lg fa-list"></i><span class="hidden-mobile hidden-tablet"> Lista Instrumentos</span>' => array(
            'content' => $this->renderPartial(
                    'orderItems/_update',
                    array('model'=>$model, 'orderItems'=>$orderItems),
                    true
                    ),
            'id' => 'orderItems',
        ),
    ),
    'defaultTab'=>$defaultTab,
    'id' => 'order-update-01',
    'articleOptions'=>array('class'=>'col-sm-12 col-md-12 col-lg-12'),
    'htmlOptions'=>array(
        JvHtml::DATA_WIDGET_COLORBUTTON => 'false',
        JvHtml::DATA_WIDGET_EDITBUTTON => 'false',
        JvHtml::DATA_WIDGET_CUSTOMBUTTON => 'false',
        JvHtml::DATA_WIDGET_DELETEBUTTON => 'false',
    ),
    'buttonLabel'=>'<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Opções</span>',
    'buttons'=>$this->menu,
    'buttonOptions'=>array(
        'split' => false,
        'size' => BsHtml::BUTTON_SIZE_SMALL,
        'color' => BsHtml::BUTTON_COLOR_PRIMARY,
        'dropup' => false,
        'menuOptions'=>array(
            'pull'=>  BsHtml::PULL_RIGHT,
        ),
        'url'=>'#',
    ),
));
?>