<?php
/* @var $this OrderController */
/* @var $model Order */
?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('icon'=> 'glyphicon glyphicon-list', 'label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array('admin')),
);
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget'); ?>

<?php $jarvis->beginJarvisWidget('order-create-01', BsHtml::pageHeaderJarvis('Cadastrar',GxHtml::encode($model->label())), array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    )
); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'buttons'=> 'create')); ?>

<?php $jarvis->endJarvisWidget(); ?>

<?php $this->endWidget(); ?>