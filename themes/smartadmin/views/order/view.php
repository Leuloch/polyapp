<?php
/* @var $this OrderController */
/* @var $model Order */
?>

<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>Yii::t('app', 'Nova Entrada'), 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>Yii::t('app', 'Editar'), 'url'=>array('update', 'id' => $model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>Yii::t('app', 'Excluir'), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>Yii::t('app', 'Listas Entradas'), 'url'=>array('admin')),
);
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('view-order-01', '<strong>Visualizar</strong><i> Entradas de Instrumentos</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-edit"></i><span class="hidden-mobile hidden-tablet"> Editar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'update/' . $model->id,
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
</div>
<?php echo BsHtml::pageHeader('Visualização da Entrada N° ', $model->id) ?>

<?php $this->widget('bootstrap.widgets.BsDetailView', array(
	'data' => $model,
	'attributes' => array(
            'id',
            'data_criacao',
            'data_atualizacao',
            'usuario_criacao',
            'usuario_atualizacao',
            'cliente_id',
            'contato_id',
            'status',
            'observacao',
            'forma_entrada',
            'nota_fiscal',
            'data_entrada',
            'data_prev_saida',
            'data_disponibilidade',
            'data_retirada',
        ),
)); ?>
<?php
    $this->widget('bootstrap.widgets.BsGridView', array(
        'dataProvider' => $model->gridOrderItems(),
        'id' => uniqid('table_'),
        'columns' => array(
            'id',
            'ordem_id',
            'quantidade',
            'descricao',
            'instrumento_id'
        ),
        'type' => BsHtml::GRID_TYPE_BORDERED

        ));
?>
    
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>