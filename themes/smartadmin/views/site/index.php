<?php
/* 
 * @var $this SiteController
 * @var $Empresa Empresa Model
 *  
 */


$this->breadcrumbs=array(
	'Dashboard',
);
$this->pageTitle=Yii::app()->name;
?>

<?php 
    $jarvis = $this->beginWidget(
        'application.widgets.jarvisWidget', 
        array(
            'sectionOptions'=>array(
                'id'=>'widget-grid'
            )
        )
    ); 
?>

<?php 
    $jarvis->beginJarvisWidget(
        'index-principal-01', 
        '<strong>Clientes</strong><i> Últimos Clientes Adicionados</i>', 
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false'
        ),
    //option for body widget 'no-padding' in the class 
    array('class'=>''),
    //option for article widget
    array('class'=>'col-sm-6 col-md-6 col-lg-6')
); ?>
<?php
    $this->widget('bootstrap.widgets.BsGridView', array(
        'dataProvider' => $Empresa->lastEntered(),
        'id' => uniqid('table_'),
        'columns' => array(
            'id',
            'nome',
            'razao_social',
        ),
        'type' => BsHtml::GRID_TYPE_BORDERED

        ));
?>
<?php $jarvis->endJarvisWidget(); ?>

<?php 
    $jarvis->beginJarvisWidget(
        'index-principal-02', 
        '<strong>Orçamentos</strong><i> Aguardando Aprovação!</i>', 
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false'
        ),
    //option for body widget 'no-padding' in the class 
    array('class'=>''),
    //option for article widget
    array('class'=>'col-sm-6 col-md-6 col-lg-6')
); ?>

<?php $jarvis->endJarvisWidget(); ?>

<?php 
    $jarvis->beginJarvisWidget(
        'index-principal-03', 
        '<strong>Ordem Serviço</strong><i> Recentemente adicionadas</i>', 
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false'
        ),
    //option for body widget 'no-padding' in the class 
    array('class'=>''),
    //option for article widget
    array('class'=>'col-sm-6 col-md-6 col-lg-6')
); ?>
<?php
    $this->widget('bootstrap.widgets.BsGridView', array(
        'dataProvider' => $Order->lastEntered(),
        'id' => uniqid('table_'),
        'columns' => array(
            'id',
            'cliente_id',
            'data_prev_saida',
        ),
        'type' => BsHtml::GRID_TYPE_BORDERED

        ));
?>
<?php $jarvis->endJarvisWidget(); ?>

<?php 
    $jarvis->beginJarvisWidget(
        'index-principal-04', 
        '<strong>Gráfico</strong><i> Quantidade de Certificados Emitidos</i>', 
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false'
        ),
    //option for body widget 'no-padding' in the class 
    array('class'=>''),
    //option for article widget
    array('class'=>'col-sm-6 col-md-6 col-lg-6')
); ?>

<?php $jarvis->endJarvisWidget(); ?>

<?php $this->endWidget(); ?>
