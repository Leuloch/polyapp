<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title> <?php echo CHtml::encode(Yii::app()->name);?> </title>
        <meta name="description" content="sistema intranet para laboratório Polyafer">
        <meta name="author" content="oliah tecnologia - Ednei Oliveira">

        <!-- Use the correct meta names below for your web application Ref: http://davidbcalhoun.com/2010/viewport-metatag-->

        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/smartadmin-production.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/smartadmin-skins.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/datepicker-ed.css">

        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon/favicon.ico" type="image/x-icon">

        <!-- GOOGLE FONT -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    </head>
    <?php $settings = (Yii::app()->settings->get(Yii::app()->user->username))?implode(' ', Yii::app()->settings->get(Yii::app()->user->username)): ''; ?>
    <body class="fixed-width <?php echo $settings; ?>">
        <!-- HEADER -->
        <header id="header">
            <div id="logo-group">

                <!-- PLACE YOUR LOGO HERE -->
                <span id="logo"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logoPolyafer.png" alt="Polyafer"> </span>
                <!-- END LOGO PLACEHOLDER -->

                <!-- Note: The activity badge color changes when clicked and resets the number to 0
                Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->
                <span id="activity" class="activity-dropdown"> <i class="fa fa-user"></i> <b class="badge"> 21 </b> </span>

                <!-- AJAX-DROPDOWN : control this dropdown height, look and feel from the LESS variable file -->
                <div class="ajax-dropdown">

                    <!-- the ID links are fetched via AJAX to the ajax container "ajax-notifications" -->
                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/mail.html">
                            Msgs (14) </label>
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/notifications.html">
                            notify (3) </label>
                        <label class="btn btn-default">
                            <input type="radio" name="activity" id="ajax/notify/tasks.html">
                            Tasks (4) </label>
                    </div>

                    <!-- notification content -->
                    <div class="ajax-notifications custom-scroll">

                        <div class="alert alert-transparent">
                            <h4>Click a button to show messages here</h4>
                            This blank page message helps protect your privacy, or you can show the first message here automatically.
                        </div>

                        <i class="fa fa-lock fa-4x fa-border"></i>

                    </div>
                    <!-- end notification content -->

                    <!-- footer: refresh area -->
                    <span> Last updated on: 12/12/2013 9:43AM
                        <button type="button" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right">
                            <i class="fa fa-refresh"></i>
                        </button> </span>
                    <!-- end footer -->

                </div>
                <!-- END AJAX-DROPDOWN -->
            </div>

            <!-- projects dropdown -->
            <div id="project-context">

                <span class="label">Projects:</span>
                <span id="project-selector" class="popover-trigger-element dropdown-toggle" data-toggle="dropdown">Recent projects <i class="fa fa-angle-down"></i></span>

                <!-- Suggestion: populate this list with fetch and push technique -->
                <ul class="dropdown-menu">
                    <li>
                        <a href="javascript:void(0);">Online e-merchant management system - attaching integration with the iOS</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Notes on pipeline upgradee</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);">Assesment Report for merchant account</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);"><i class="fa fa-power-off"></i> Clear</a>
                    </li>
                </ul>
                <!-- end dropdown-menu-->

            </div>
            <!-- end projects dropdown -->

            <!-- pulled right: nav area -->
            <div class="pull-right">

                <!-- collapse menu button -->
                <div id="hide-menu" class="btn-header pull-right">
                    <span> <a href="javascript:void(0);" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
                </div>
                <!-- end collapse menu -->

                <!-- logout button -->
                <div id="logout" class="btn-header transparent pull-right">
                    <span> <a data-logout-msg="Tem certeza de que deseja SAIR?" href="<?php echo Yii::app()->createAbsoluteUrl('user/logout') ; ?>" title="Sign Out"><i class="fa fa-sign-out"></i></a> </span>
                </div>
                <!-- end logout button -->
                
                <div class="btn-header pull-right hidden-mobile">
                    <span>
                    <?php echo CHtml::link('<i class="fa fa-group"></i>', array('/user/admin'), array('title'=>'Usuários', 'class'=>'txt-color-red')); ?>
                    </span>
                </div>
                <!-- search mobile button (this is hidden till mobile view port) -->
                <div id="search-mobile" class="btn-header transparent pull-right">
                    <span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
                </div>
                <!-- end search mobile button -->

                <!-- input: search field -->
                <form action="#search.html" class="header-search pull-right">
                    <input type="text" placeholder="Find reports and more" id="search-fld">
                    <button type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                    <a href="javascript:void(0);" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>
                </form>
                <!-- end input: search field -->

                <!-- multiple lang dropdown : find all flags in the image folder -->
                <ul class="header-dropdown-list hidden-xs">
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/flags/us.png"> <span> US </span> <i class="fa fa-angle-down"></i> </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="active">
                                <a href="javascript:void(0);"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/flags/us.png"> US</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/flags/es.png"> Spanish</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><img alt="" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/flags/de.png"> German</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- end multiple lang -->
                
            </div>
            <!-- end pulled right: nav area -->

        </header>
        <!-- END HEADER -->
        <!-- Left panel : Navigation area -->
        <aside id="left-panel">
            <!-- User info -->
            <div class="login-info">
                <span>
                    <a href="javascript:void(0);" id="show-shortcut">
                        <?php $this->widget('ext.yiiuserimg.YiiUserImg', array(
                                            'htmlOptions'=>array(
                                                'alt'=>'me',
                                                'class'=>'online',
                                            ),
                                        )); ?>
                        <span>
                            <?php echo Yii::app()->user->name; ?>
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                </span>
            </div>
            <!-- END USER INFO -->
            
            <!-- NAVIGATION -->
            <nav>
            <?php $this->widget('zii.widgets.CMenu', array(
                'items' => array(
                    array(
                        'label'=>'<i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Principal</span>',
                        'url'=>array('/site/index'),
                    ),
                    array(
                        'label'=>'<i class="fa fa-lg fa-fw fa-calendar"></i> <span class="menu-item-parent">Calendário</span>',
                        'url'=>array('/Evento/index'),
                    ),
                    array(
                        'label'=>'<i class="fa fa-lg fa-fw fa-edit"></i> <span class="menu-item-parent">Cadastros</span>',
                        'url'=>'#',
                        'items'=>array(
                            array(
                                'label'=>'Empresas',
                                'url'=>array('/Empresa/admin'),
                            ),
                            array(
                                'label'=>'Contatos',
                                'url'=>array('/Contato/admin'),
                            ),
                            array(
                                'label'=>'Instrumentos',
                                'url'=>array('/Instrumento/admin'),
                            ),
                            array(
                                'label'=>'Serviços',
                                'url'=>array('/Servico/admin'),
                            ),
                        ),
                    ),
                    array(
                        'label'=>'<i class="fa fa-lg fa-fw fa-building"></i> <span class="menu-item-parent">Comercial</span>',
                        'url'=>'#',
                        'items'=>array(
                            array(
                                'label'=>'Entrada Instrumentos',
                                'url'=>array('/Order/admin'),
                            ),
                            array(
                                'label'=>'Vendas',
                                'url'=>'#',
                            ),
                            array(
                                'label'=>'Serviços',
                                'url'=>'#',
                            ),
                            array(
                                'label'=>'Produtos',
                                'url'=>'#',
                            ),
                        ),
                    ),
                    array(
                        'label'=>'<i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Administrativo</span>',
                        'url'=>'#',
                        'items'=>array(
                            array(
                                'label'=>'Usuários',
                                'url'=>array('/user/admin'),
                            ),
                            array(
                                'label'=>'Permissões',
                                'url'=>array('/auth'),
                            ),
                            array(
                                'label'=>'Meu Perfil',
                                'url'=>array('/user/profile'),
                            ),
                        ),
                    ),
                ),
                'encodeLabel'=>false,
            )); ?>
            </nav>
            <span class="minifyme">
                <i class="fa fa-arrow-circle-left hit"></i>
            </span>
        </aside>
        <!-- END NAVIGATION -->
        
        <!-- MAIN PANEL -->
        <div id="main" role="main">
            <!-- RIBBON -->
            <div id="ribbon">
                <span class="ribbon-button-alignment"> <span id="refresh" class="btn btn-ribbon" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Atenção! Ao clicar aqui, todos os painéis serão resetados ao estado original." data-html="true"><i class="fa fa-refresh"></i></span> </span>
                <!-- breadcrumbs -->
                <?php if(isset($this->breadcrumbs)):?>
                    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'=>$this->breadcrumbs,
                            'htmlOptions'=>array('class'=>'breadcrumb'),
                            'tagName'=>'ul',
                    )); ?><!-- end breadcrumbs -->
                <?php endif?>
            </div>
            <!-- END RIBBON -->
            
            <!-- MAIN CONTENT -->
            
                <?php echo $content; ?>
            
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN PANEL -->
        <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
        Note: These tiles are completely responsive,
        you can add as many as you like -->
        <div id="shortcut">
                <?php $this->widget('zii.widgets.CMenu', array(
                    'encodeLabel'=>false,
                    'items'=>array(
                        array(
                            'label'=>'<span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span>',
                            'url'=>'#',
                            'linkOptions'=>array('class'=>'jarvismetro-tile big-cubes bg-color-blue'),
                        ),
                        array(
                            'label'=>'<span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span>',
                            'url'=>'#',
                            'linkOptions'=>array('class'=>'jarvismetro-tile big-cubes bg-color-orangeDark'),
                        ),
                        array(
                            'label'=>'<span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span>',
                            'url'=>'#',
                            'linkOptions'=>array('class'=>'jarvismetro-tile big-cubes bg-color-purple'),
                        ),
                        array(
                            'label'=>'<span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span>',
                            'url'=>'#',
                            'linkOptions'=>array('class'=>'jarvismetro-tile big-cubes bg-color-blueDark'),
                        ),
                        array(
                            'label'=>'<span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span>',
                            'url'=>'#',
                            'linkOptions'=>array('class'=>'jarvismetro-tile big-cubes bg-color-greenLight'),
                        ),
                        array(
                            'label'=>'<span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span>',
                            'url'=>array('/user/profile'),
                            'linkOptions'=>array('class'=>'jarvismetro-tile big-cubes bg-color-pinkDark'),
                        ),
                    ),
                )); ?>
        </div>
        
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script>
                if (!window.jQuery) {
                        document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery-2.0.2.min.js"><\/script>');
                }
        </script>
        <script>
                if (!window.jQuery.ui) {
                        document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
                }
        </script>
        
        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
        
        <!-- BOOTSTRAP JS -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap/bootstrap.min.js"></script>
        
        <!-- JQUERY VALIDATE -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/jquery-validate/jquery.validate.min.js"></script>
                
        <!-- JQUERY UI + Bootstrap Slider -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
        
        <!-- browser msie issue fix -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

        <!-- FastClick: For mobile devices -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/fastclick/fastclick.js"></script>
        
        <!-- JQUERY MASKED INPUT -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
        
        <!--[if IE 7]>

        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->
        
        <!-- MAIN APP JS FILE -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/app.js"></script>
        
        <!-- Notification Js -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/notification/SmartNotification.min.js"></script>
        
        
        <script type="text/javascript">

        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        $(document).ready(function() {

                pageSetUp();






        });

        </script>
    </body>
</html>