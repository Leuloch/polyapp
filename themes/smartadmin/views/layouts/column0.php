<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/mainLogin'); ?>
<div id="content" class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                <h1 class="txt-color-red login-header-big">Intranet Polyafer</h1>
                <div class="hero">

                        <div class="pull-left login-desc-box-l">
                                <h4 class="paragraph-header">Bem vindo ao projeto! Para começar o uso do sistema de Intranet, crie sua conta.</h4>
                                <div class="login-app-icons">
                                        <a href="http://polyafer.com.br/"  target="_blank" class="btn btn-danger btn-sm">Site Polyafer</a>
                                        <a href="javascript:void(0);" class="btn btn-danger btn-sm">Descubra mais</a>
                                </div>
                        </div>
                    <?php if ($this->getUniqueId()=='user/login') { ?>
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/demo/multimeter.png" class="pull-right display-image" alt="" style="width:240px">
                    <?php } else { ?>
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/demo/termometer.png" class="pull-right display-image" alt="" style="width:230px">
                    <?php }; ?>
                </div>

                <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h5 class="about-heading">Sobre Intranet Polyafer</h5>
                                <p>
                                        Este sistema tem como principal objetivo contribuir para a execução das rotinas internas da Polyafer Metrologia, trazendo mais agilidade e segurança nas informações.
                                </p>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h5 class="about-heading">Dê sua opinião!</h5>
                                <p>
                                        Este é um sistema que ainda está em desenvolvimento, e ainda há muito a ser feito. Contribua com críticas e sugestões.
                                </p>
                        </div>
                </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>