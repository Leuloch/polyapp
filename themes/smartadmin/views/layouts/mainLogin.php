<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title> <?php echo CHtml::encode(Yii::app()->name);?> </title>
        <meta name="description" content="sistema intranet para laboratório Polyafer">
        <meta name="author" content="oliah tecnologia - Ednei Oliveira">

        <!-- Use the correct meta names below for your web application Ref: http://davidbcalhoun.com/2010/viewport-metatag--> 

        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/smartadmin-production.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/smartadmin-skins.css">

        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/favicon/favicon.ico" type="image/x-icon">

        <!-- GOOGLE FONT -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
    </head>
    <body id="login" class="animated fadeInDown">
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
        <header id="header">
                <!--<span id="logo"></span>-->

                <div id="logo-group">
                        <span id="logo"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/logoPolyafer.png" alt="Polyafer App"> </span>

                        <!-- END AJAX-DROPDOWN -->
                </div>
            <?php if ($this->getUniqueId()=='user/login') { ?>
            <span id="login-header-space"> <span class="hidden-mobile">Precisa de uma conta?</span> <a href="<?php echo Yii::app()->createUrl('user/registration') ;?>" class="btn btn-danger">CRIAR CONTA</a> </span>
            <?php } else { ?>
            <span id="login-header-space"> <span class="hidden-mobile">Já possui um cadastro?</span> <a href="<?php echo Yii::app()->createUrl('user/login') ;?>" class="btn btn-danger">FAZER LOGIN</a> </span>
            <?php }; ?>
        </header>
        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <?php echo $content ?>
            <!-- END FOR MAIN CONTENT -->
        </div>
        
        <!--================================================== -->
        
         <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script>
                if (!window.jQuery) {
                        document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery-2.0.2.min.js"><\/script>');
                }
        </script>

        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script>
                if (!window.jQuery.ui) {
                        document.write('<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
                }
        </script>
        
        <!-- BOOTSTRAP JS -->		
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap/bootstrap.min.js"></script>

        <!-- CUSTOM NOTIFICATION -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/notification/SmartNotification.min.js"></script>
        
        <!-- FastClick: For mobile devices -->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugin/fastclick/fastclick.js"></script>
        
        <!--[if IE 7]>

                <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->
        
        <!-- MAIN APP JS FILE -->
        <script src="js/app.js"></script>
    </body>
</html>
