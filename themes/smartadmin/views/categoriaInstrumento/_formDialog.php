<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'categoria-instrumento-form',
    'enableAjaxValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>
<fieldset>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>255)); ?>
    <?php echo $form->textAreaControlGroup($model,'descricao',array('rows'=>6)); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::ajaxSubmitButton(
            'Adicionar',
            CHtml::normalizeUrl(array('categoriaInstrumento/addnew','render'=>false)),array('success'=>'js: function(data) {
                        $("#Instrumento_categoria_instrumento_id").append(data);
                        $("#categoriaDialog").dialog("close");
                    }'),
            array(
                'id'=>'closeCategoriaDialog',
                'color'=>  BsHtml::BUTTON_COLOR_PRIMARY,
            )); 
    ?>
</div>

<?php $this->endWidget(); ?>

    