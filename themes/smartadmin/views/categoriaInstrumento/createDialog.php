<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'categoriaDialog',
                'options'=>array(
                    'title'=>'Adicionar Categoria de Instrumento',
                    'closeOnEscape'=>true,
                    'autoOpen'=>true,
                    'modal'=>'true',
                    'width'=>'auto',
                    'height'=>'auto',
                    'close'=>"js:function(e,ui){jQuery('body').undelegate('#closeCategoriaDialog','click'); jQuery('#categoriaDialog').empty();}"
                ),
                ));
echo $this->renderPartial('_formDialog', array('model'=>$model)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
