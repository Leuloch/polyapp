<?php
/* @var $this ConfigCertificadoController */
/* @var $model ConfigCertificado */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'empresa_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome_certificado',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'endereco',array('maxlength'=>270)); ?>
    <?php echo $form->textFieldControlGroup($model,'cidade',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'estado',array('maxlength'=>2)); ?>
    <?php echo $form->textFieldControlGroup($model,'cep'); ?>
    <?php echo $form->textFieldControlGroup($model,'validade'); ?>
    <?php echo $form->textFieldControlGroup($model,'aprovacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'opta_digital'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
