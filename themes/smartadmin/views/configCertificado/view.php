<?php
/* @var $this ConfigCertificadoController */
/* @var $model ConfigCertificado */
?>

<?php
$this->breadcrumbs=array(
	'Config Certificados'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List ConfigCertificado', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create ConfigCertificado', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update ConfigCertificado', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete ConfigCertificado', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage ConfigCertificado', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','ConfigCertificado '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'empresa_id',
		'nome_certificado',
		'endereco',
		'cidade',
		'estado',
		'cep',
		'validade',
		'aprovacao',
		'opta_digital',
	),
)); ?>