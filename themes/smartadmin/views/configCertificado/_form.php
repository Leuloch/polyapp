<?php
/* @var $this ConfigCertificadoController */
/* @var $model ConfigCertificado */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'config-certificado-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'empresa_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome_certificado',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'endereco',array('maxlength'=>270)); ?>
    <?php echo $form->textFieldControlGroup($model,'cidade',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'estado',array('maxlength'=>2)); ?>
    <?php echo $form->textFieldControlGroup($model,'cep'); ?>
    <?php echo $form->textFieldControlGroup($model,'validade'); ?>
    <?php echo $form->textFieldControlGroup($model,'aprovacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'opta_digital'); ?>

    <?php echo BsHtml::submitButton('Submit', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
