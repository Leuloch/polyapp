<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'fabricante-form',
    'enableAjaxValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>
<fieldset>
    <?php echo $form->textFieldControlGroup($model,'nome_fabricante',array('maxlength'=>50)); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::ajaxSubmitButton(
            'Adicionar',
            CHtml::normalizeUrl(array('fabricante/addnew','render'=>false)),array('success'=>'js: function(data) {
                        $("#Instrumento_fabricante_id").append(data);
                        $("#fabricanteDialog").dialog("close");
                    }'),
            array(
                'id'=>'closeFabricanteDialog',
                'color'=>  BsHtml::BUTTON_COLOR_PRIMARY,
            )); 
    ?>
</div>

<?php $this->endWidget(); ?>

    