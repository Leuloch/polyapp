<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'fabricanteDialog',
                'options'=>array(
                    'title'=>'Adicionar Fabricante',
                    'closeOnEscape'=>true,
                    'autoOpen'=>true,
                    'modal'=>'true',
                    'width'=>'auto',
                    'height'=>'auto',
                    'close'=>"js:function(e,ui){jQuery('body').undelegate('#closeFabricanteDialog','click'); jQuery('#fabricanteDialog').empty();}"
                ),
                ));
echo $this->renderPartial('_formDialog', array('model'=>$model)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
