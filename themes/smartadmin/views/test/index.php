<?php
/* @var $this TestController */
?>

<?php
$this->breadcrumbs=array(
	'Testes',
);
?>
<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget', array('sectionOptions'=>array('id'=>'widget-grid'))); ?>
<?php $jarvis->beginJarvisWidget(
        'index-test-01', 
        '<strong>WIDGET</strong><i> para Testes</i>',
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
            JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false'
        ),
        //option for body widget 'no-padding' in the class 
        array('class'=>''),
        //option for article widget
        array('class'=>'col-sm-12 col-md-12 col-lg-12')
); ?>

<p> Widget comes with a default 10 padding to the body which can be removed by adding the class <code>.no-padding</code> 
to the <code>.widget-body</code> class. The default widget also comes with 5 widget buttons as displayed on top right corner of the widget header. </p>
<a href="javascript:void(0);" class="btn btn-default btn-lg"> <strong>Big</strong> <i>Button</i> </a>
                                                                                
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>