<?php
/* @var $this ContatoController */
/* @var $model Contato */


$this->breadcrumbs=array(
	'Contatos'=>array('index'),
	'Listar',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Contato', 'url'=>array('create')),
        array('label'=>'<i class="fa fa-table"></i> Exportar Excel', 'url'=>array('exportExcel')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
// add the script to toggle the field list
$('.fields-button').click(function(){
        $('.fields-form').toggle();
        return false;
});
$('.search-form form').submit(function(){
	$('#contato-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('admin-contato-01', '<strong>Lista</strong><i> Contatos</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

    <div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Adicionar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'create',
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
            <?php echo BsHtml::button('<i class="fa fa-cogs"></i><span class="hidden-mobile hidden-tablet"> Colunas</span>', array('class' => 'fields-button', 'color'=>BsHtml::BUTTON_COLOR_INFO), '#'); ?>
    </div>

        <div class="search-form" style="display:none">
            <?php $this->renderPartial('_search',array(
                'model'=>$model,
            )); ?>
        </div>
        <!-- search-form -->
        <div class="fields-form" style="display:none">
            <?php
            $this->renderPartial('_fields', array(
                'model' => $model,
                'columns' => $columns,
            ));
            ?>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
            // here we create an array of columns for the grid view
            $modelColumns=$model->getMetaData()->columns;
            $myColumnsOptions = array(
                'id' => array(
                    'name'=>'id',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'data_criacao' => array(
                    'name'=>'data_criacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'data_atualizacao' => array(
                    'name'=>'data_atualizacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'usuario_criacao' => array(
                    'name'=>'usuario_criacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'usuario_atualizacao' => array(
                    'name'=>'usuario_atualizacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'nome' => array(
                    'name'=>'nome',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'cargo' => array(
                    'name'=>'cargo',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'setor' => array(
                    'name'=>'setor',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'ddd_tel_comercial' => array(
                    'name'=>'ddd_tel_comercial',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'tel_comercial' => array(
                    'name'=>'tel_comercial',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'tel_comercial_ramal' => array(
                    'name'=>'tel_comercial_ramal',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'ddd_tel_outro' => array(
                    'name'=>'ddd_tel_outro',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'tel_outro' => array(
                    'name'=>'tel_outro',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'tel_outro_ramal' => array(
                    'name'=>'tel_outro_ramal',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'ddd_cel_comercial' => array(
                    'name'=>'ddd_cel_comercial',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'cel_comercial' => array(
                    'name'=>'cel_comercial',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'ddd_cel_outro' => array(
                    'name'=>'ddd_cel_outro',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'cel_outro' => array(
                    'name'=>'cel_outro',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'email' => array(
                    'name'=>'email',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'email_outro' => array(
                    'name'=>'email_outro',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'contato_administrativo' => array(
                    'name'=>'contato_administrativo',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'contato_comercial' => array(
                    'name'=>'contato_comercial',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'contato_financeiro' => array(
                    'name'=>'contato_financeiro',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'contato_tecnico' => array(
                    'name'=>'contato_tecnico',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'empresa_id' => array(
                    'name'=>'empresa_id',
                    'htmlOptions'=>array('width'=>'60'),
                ),
            );
            
            foreach ($myColumnsOptions AS $columnAttribute => $columnOptions) {
                if (in_array($columnAttribute, $columns)) {
                    $key=array_search($columnAttribute,$columns);
                    $columns[$key] = $columnOptions;
                }
            }
            
            $columns[]=array(
                            'class'=>'bootstrap.widgets.BsButtonColumn',
                            'htmlOptions'=>array('width'=>'145px'),
                            'viewButtonOptions'=>array('class'=>'btn btn-xs txt-color-green'),
                            'updateButtonOptions'=>array('class'=>'btn btn-xs txt-color-blue'),
                            'deleteButtonOptions'=>array('class'=>'btn btn-xs txt-color-red'),
                            'header'=>BsHtml::dropDownList(
                                    'pageSize',
                                    $pageSize,
                                    array(10=>10,20=>20,50=>50,100=>100), 
                                    array('onchange'=>"$.fn.yiiGridView.update('file-grid',{ data:{pageSize: $(this).val() }})")),
            );
            ?>
            </div>
        </div>
        
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'contato-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>$columns,
                        'type'=>array(BsHtml::GRID_TYPE_BORDERED, BsHtml::GRID_TYPE_CONDENSED),
                        'ajaxUpdate'=>true, //pode usar falso caso a versão do jQuery dê problemas
                        'emptyTagName'=>'tr',
                        'emptyText'=>'<td class="dataTables_empty" valign="top" colspan="7"><i class="fa fa-warning text-warning"></i> Nenhum resultado encontrado</td>',
                        'enableHistory'=>true, //??? não entendi!
                        'selectableRows'=>0,
                        'htmlOptions'=>array('class'=>'table-responsive'),
        )); ?>
    </div>
</div>

<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>


