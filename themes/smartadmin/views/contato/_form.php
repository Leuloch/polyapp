<?php
/* @var $this ContatoController */
/* @var $model Contato */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'contato-form',
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'focus'=>array($model, 'nome'),
    'enableAjaxValidation'=>true,
)); ?>


    <?php echo $form->errorSummary($model); ?>
<fieldset>
    <legend>Contato</legend>
    <div class="col-md-12 form-group">
        <div class="col-md-offset-2 col-md-2">
            <?php echo $form->checkBox($model,'contato_administrativo',array('label'=>$model->getAttributeLabel('contato_administrativo'))); ?>
            <?php echo $form->error($model, 'contato_administrativo'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,'contato_comercial',array('label'=>$model->getAttributeLabel('contato_comercial'))); ?>
            <?php echo $form->error($model, 'contato_comercial'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,'contato_financeiro',array('label'=>$model->getAttributeLabel('contato_financeiro'))); ?>
            <?php echo $form->error($model, 'contato_financeiro'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,'contato_tecnico',array('label'=>$model->getAttributeLabel('contato_tecnico'))); ?>
            <?php echo $form->error($model, 'contato_tecnico'); ?>
        </div>
    </div>
    
    <?php echo $form->textFieldControlGroup($model,'nome',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Contato_empresa_id">
            <?php echo $model->getAttributeLabel('empresa_id'); ?>
        </label>
        <div class="col-md-10">
            <?php echo Select2::activeDropDownList($model, 'empresa_id', CHtml::listData(Empresa::model()->findAll(),'id','nome'), array(
                'style'=>'width:100%',
                'class'=>'select2',
                'prompt'=>'', //para funcionar o placeholder, deve colocar um prompt vazio!
                'placeholder'=>'Selecione Empresa'
            )); ?>
            <?php echo $form->error($model, 'empresa_id'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'cargo',array(
        'maxlength'=>50,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'setor',array(
        'maxlength'=>50,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
</fieldset>
<fieldset>
    <legend>Telefones</legend>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Contato_tel_comercial">
            <?php echo $model->getAttributeLabel('tel_comercial'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_tel_comercial', array('placeholder'=>'DDD')); ?>
            <?php echo $form->error($model, 'ddd_tel_comercial'); ?>
        </div>
        <div class="col-md-6" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'tel_comercial',array('maxlength'=>8)); ?>
            <?php echo $form->error($model, 'tel_comercial'); ?>
        </div>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'tel_comercial_ramal',array('maxlength'=>8)); ?>
            <?php echo $form->error($model, 'tel_comercial_ramal'); ?>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Contato_tel_outro">
            <?php echo $model->getAttributeLabel('tel_outro'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_tel_outro', array('placeholder'=>'DDD')); ?>
            <?php echo $form->error($model, 'ddd_tel_outro'); ?>
        </div>
        <div class="col-md-6" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'tel_outro',array('maxlength'=>8)); ?>
            <?php echo $form->error($model, 'tel_outro'); ?>
        </div>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'tel_outro_ramal',array('maxlength'=>8)); ?>
            <?php echo $form->error($model, 'tel_outro_ramal'); ?>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Contato_cel_comercial">
            <?php echo $model->getAttributeLabel('cel_comercial'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_cel_comercial', array('placeholder'=>'DDD')); ?>
            <?php echo $form->error($model, 'ddd_cel_comercial'); ?>
        </div>
        <div class="col-md-8" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'cel_comercial',array('maxlength'=>9)); ?>
            <?php echo $form->error($model, 'cel_comercial'); ?>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Contato_cel_outro">
            <?php echo $model->getAttributeLabel('cel_outro'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_cel_outro', array('placeholder'=>'DDD')); ?>
            <?php echo $form->error($model, 'ddd_cel_outro'); ?>
        </div>
        <div class="col-md-8" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'cel_outro',array('maxlength'=>9)); ?>
            <?php echo $form->error($model, 'cel_outro'); ?>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Emails</legend>
    <?php echo $form->textFieldControlGroup($model,'email',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'email_outro',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>

<?php $this->endWidget(); ?>
