<?php
/* @var $this ContatoController */
/* @var $data Contato */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_criacao')); ?>:</b>
	<?php echo CHtml::encode($data->data_criacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_atualizacao')); ?>:</b>
	<?php echo CHtml::encode($data->data_atualizacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_criacao')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_criacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_atualizacao')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_atualizacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cargo')); ?>:</b>
	<?php echo CHtml::encode($data->cargo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('setor')); ?>:</b>
	<?php echo CHtml::encode($data->setor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ddd_tel_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->ddd_tel_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->tel_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_comercial_ramal')); ?>:</b>
	<?php echo CHtml::encode($data->tel_comercial_ramal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ddd_tel_outro')); ?>:</b>
	<?php echo CHtml::encode($data->ddd_tel_outro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_outro')); ?>:</b>
	<?php echo CHtml::encode($data->tel_outro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel_outro_ramal')); ?>:</b>
	<?php echo CHtml::encode($data->tel_outro_ramal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ddd_cel_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->ddd_cel_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cel_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->cel_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ddd_cel_outro')); ?>:</b>
	<?php echo CHtml::encode($data->ddd_cel_outro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cel_outro')); ?>:</b>
	<?php echo CHtml::encode($data->cel_outro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_outro')); ?>:</b>
	<?php echo CHtml::encode($data->email_outro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contato_administrativo')); ?>:</b>
	<?php echo CHtml::encode($data->contato_administrativo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contato_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->contato_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contato_financeiro')); ?>:</b>
	<?php echo CHtml::encode($data->contato_financeiro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contato_tecnico')); ?>:</b>
	<?php echo CHtml::encode($data->contato_tecnico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('empresa_id')); ?>:</b>
	<?php echo CHtml::encode($data->empresa_id); ?>
	<br />

	*/ ?>

</div>