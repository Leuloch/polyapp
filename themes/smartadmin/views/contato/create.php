<?php
/* @var $this ContatoController */
/* @var $model Contato */
?>

<?php
$this->breadcrumbs=array(
	'Contatos'=>array('index'),
	'Adicionar',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Contatos', 'url'=>array('admin')),
);
?>
<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget'); ?>

<?php $jarvis->beginJarvisWidget('contato-create-01', BsHtml::pageHeaderJarvis('Cadastrar','Contato'), array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    )
); ?>
<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-tasks"></i><span class="hidden-mobile hidden-tablet"> Listar Contatos</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'admin',
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
    </div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $jarvis->endJarvisWidget(); ?>

<?php $this->endWidget(); ?>