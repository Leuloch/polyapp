<?php
/* @var $this ContatoController */
/* @var $model Contato */
?>

<?php
$this->breadcrumbs=array(
	'Contatos'=>array('index'),
	$model->nome,
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Contato', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Editar', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Excluir', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Contatos', 'url'=>array('admin')),
);
?>
<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('view-contato-01', '<strong>Visualizar</strong><i> Contato</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-edit"></i><span class="hidden-mobile hidden-tablet"> Editar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'update/' . $model->id,
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
</div>
<?php echo BsHtml::pageHeader('Visualização',$model->nome) ?>

<?php $this->widget('bootstrap.widgets.BsDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'data_criacao',
		'data_atualizacao',
		'usuario_criacao',
		'usuario_atualizacao',
		'nome',
		'cargo',
		'setor',
		'ddd_tel_comercial',
		'tel_comercial',
		'tel_comercial_ramal',
		'ddd_tel_outro',
		'tel_outro',
		'tel_outro_ramal',
		'ddd_cel_comercial',
		'cel_comercial',
		'ddd_cel_outro',
		'cel_outro',
		'email',
		'email_outro',
		'contato_administrativo',
		'contato_comercial',
		'contato_financeiro',
		'contato_tecnico',
		'empresa_id',
	),
)); ?>
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>