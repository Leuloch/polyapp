<?php
/* @var $this ContatoController */
/* @var $model Contato */
?>

<?php
$this->breadcrumbs=array(
	'Contatos'=>array('index'),
	$model->nome=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Contato', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'Visualizar Contato', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Contatos', 'url'=>array('admin')),
);
?>
<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget'); ?>

<?php $jarvis->beginJarvisWidget('contato-update-01', BsHtml::pageHeaderJarvis('Editar',$model->nome), array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    )
); ?>
<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-tasks"></i><span class="hidden-mobile hidden-tablet"> Listar Contatos</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>Yii::app()->createUrl('contato/admin'),
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
    </div>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<?php $jarvis->endJarvisWidget(); ?>

<?php $this->endWidget(); ?>