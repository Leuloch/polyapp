<?php
/* @var $this EnderecoController */
/* @var $model Endereco */
?>

<?php
$this->breadcrumbs=array(
	'Enderecos'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Endereco', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Endereco', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Endereco', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Endereco', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Endereco '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>