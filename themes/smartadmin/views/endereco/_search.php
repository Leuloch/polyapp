<?php
/* @var $this EnderecoController */
/* @var $model Endereco */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'endereco',array('maxlength'=>150)); ?>
    <?php echo $form->textFieldControlGroup($model,'empresa_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'numero',array('maxlength'=>20)); ?>
    <?php echo $form->textFieldControlGroup($model,'complemento',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'bairro',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'cidade',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'estado',array('maxlength'=>2)); ?>
    <?php echo $form->textFieldControlGroup($model,'cep'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_endereco_faturamento'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_endereco_entrega'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_endereco_cobranca'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
