<?php
/* @var $this EnderecoController */
/* @var $model Endereco */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'endereco-form',
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
<fieldset>
    <legend>Endereço</legend>
    <div class="col-md-12 form-group">
        <div class="col-md-offset-2 col-md-2">
            <?php echo $form->checkBox($model,'tipo_endereco_faturamento',array('label'=>$model->getAttributeLabel('tipo_endereco_faturamento'))); ?>
            <?php echo $form->error($model, 'tipo_endereco_faturamento'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,'tipo_endereco_entrega',array('label'=>$model->getAttributeLabel('tipo_endereco_entrega'))); ?>
            <?php echo $form->error($model, 'tipo_endereco_entrega'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,'tipo_endereco_cobranca',array('label'=>$model->getAttributeLabel('tipo_endereco_cobranca'))); ?>
            <?php echo $form->error($model, 'tipo_endereco_cobranca'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'endereco',array(
        'maxlength'=>150,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'numero',array(
        'maxlength'=>20,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'complemento',array(
        'maxlength'=>50,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'bairro',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'cidade',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'estado',array(
        'maxlength'=>2,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'cep', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
</fieldset>
<div class="form-actions">
    <?php //echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); 
        echo BsHtml::linkButton('Excluir', array('onclick' => 'deleteEndereco(this, ' . $index . '); return false;'));
    ?>
</div>

<?php $this->endWidget(); ?> <!-- form -->

<?php
Yii::app()->clientScript->registerScript('deleteEndereco', "
function deleteEndereco(elm, index)
{
    element=$(elm).parent().parent();
    /* animate div */
    $(element).animate(
    {
        opacity: 0.25,
        left: '+=50',
        height: 'toggle'
    }, 500,
    function() {
        /* remove div */
        $(element).remove();
    });
}", CClientScript::POS_END);