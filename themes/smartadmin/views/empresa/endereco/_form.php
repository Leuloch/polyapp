<?php
/* @var $this EnderecoController */
/* @var $endereco Endereco */
/* @var $form BSActiveForm */
?>
    <?php //echo BsHtml::errorSummary($endereco); ?>

    <div class="col-md-12 form-group">
        <div class="col-md-offset-2 col-md-2">
            <?php echo BsHtml::activeCheckBox($endereco,'tipo_endereco_faturamento',array('label'=>$endereco->getAttributeLabel('tipo_endereco_faturamento'))); ?>
            <?php echo BsHtml::error($endereco, 'tipo_endereco_faturamento'); ?>
        </div>
        <div class="col-md-2">
            <?php echo BsHtml::activeCheckBox($endereco,'tipo_endereco_entrega',array('label'=>$endereco->getAttributeLabel('tipo_endereco_entrega'))); ?>
            <?php echo BsHtml::error($endereco, 'tipo_endereco_entrega'); ?>
        </div>
        <div class="col-md-2">
            <?php echo BsHtml::activeCheckBox($endereco,'tipo_endereco_cobranca',array('label'=>$endereco->getAttributeLabel('tipo_endereco_cobranca'))); ?>
            <?php echo BsHtml::error($endereco, 'tipo_endereco_cobranca'); ?>
        </div>
    </div>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'endereco',array(
        'maxlength'=>150,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'numero',array(
        'maxlength'=>20,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'complemento',array(
        'maxlength'=>50,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'bairro',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'cidade',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'estado',array(
        'maxlength'=>2,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo BsHtml::activeTextFieldControlGroup($endereco,'cep', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
