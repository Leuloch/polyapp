<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $enderecos array of Endereco Model */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('DynamicTabularForm', array(
    'id'=>'endereco-form',
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation'=>true,
    'focus'=>array($model, 'endereco'),
    'defaultRowView'=>'_enderecoForm'
)); ?>

<?php echo $form->hiddenField($model, "id"); ?>
<?php echo $form->rowForm($enderecos,null,array(),'<i class="fa fa-plus-square"></i><span class="hidden-mobile hidden-tablet"> Adicionar Endereco</span>', array('class'=>'btn-primary btn-block')); ?>

<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget() ?>