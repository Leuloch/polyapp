<?php
/* @var $this EmpresaController */
/* @var $data Empresa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_criacao')); ?>:</b>
	<?php echo CHtml::encode($data->data_criacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_atualizacao')); ?>:</b>
	<?php echo CHtml::encode($data->data_atualizacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_criacao')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_criacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_atualizacao')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_atualizacao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome')); ?>:</b>
	<?php echo CHtml::encode($data->nome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cliente')); ?>:</b>
	<?php echo CHtml::encode($data->cliente); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fornecedor')); ?>:</b>
	<?php echo CHtml::encode($data->fornecedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_cadastro')); ?>:</b>
	<?php echo CHtml::encode($data->num_cadastro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razao_social')); ?>:</b>
	<?php echo CHtml::encode($data->razao_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_pessoa')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_pessoa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cnpj_cpf')); ?>:</b>
	<?php echo CHtml::encode($data->cnpj_cpf); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ddd_tel_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->ddd_tel_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefone_comercial')); ?>:</b>
	<?php echo CHtml::encode($data->telefone_comercial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ddd_celular')); ?>:</b>
	<?php echo CHtml::encode($data->ddd_celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular')); ?>:</b>
	<?php echo CHtml::encode($data->celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inscricao_estadual')); ?>:</b>
	<?php echo CHtml::encode($data->inscricao_estadual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isencao_ie')); ?>:</b>
	<?php echo CHtml::encode($data->isencao_ie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inscricao_municipal')); ?>:</b>
	<?php echo CHtml::encode($data->inscricao_municipal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inscricao_suframa')); ?>:</b>
	<?php echo CHtml::encode($data->inscricao_suframa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('opta_simples')); ?>:</b>
	<?php echo CHtml::encode($data->opta_simples); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isencao_icms')); ?>:</b>
	<?php echo CHtml::encode($data->isencao_icms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacao')); ?>:</b>
	<?php echo CHtml::encode($data->observacao); ?>
	<br />

	*/ ?>

</div>