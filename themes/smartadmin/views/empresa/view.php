<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
?>

<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	$model->nome,
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Empresa', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Editar', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Excluir', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Empresas', 'url'=>array('admin')),
);
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('view-empresa-01', '<strong>Visualizar</strong><i> Empresas</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-edit"></i><span class="hidden-mobile hidden-tablet"> Editar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>'update/' . $model->id,
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
</div>
<?php echo BsHtml::pageHeader('Visualização', $model->nome) ?>

<?php $this->widget('bootstrap.widgets.BsDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'data_criacao',
		'data_atualizacao',
		'usuario_criacao',
		'usuario_atualizacao',
		'nome',
		'cliente',
		'fornecedor',
		'num_cadastro',
		'razao_social',
		'tipo_pessoa',
		'cnpj_cpf',
		'ddd_tel_comercial',
		'telefone_comercial',
		'ddd_celular',
		'celular',
		'email',
		'website',
		'inscricao_estadual',
		'isencao_ie',
		'inscricao_municipal',
		'inscricao_suframa',
		'opta_simples',
		'isencao_icms',
		'observacao',
	),
)); ?>
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>