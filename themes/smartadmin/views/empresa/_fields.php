<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $form CActiveForm */
?>

<div>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'empresa-columns',
        'enableAjaxValidation'=>false,
));

$modelColumns=$model->getMetaData()->columns;
$attributeLabels = $model->attributeLabels();

echo "<fieldset>";
$i=0;
foreach ($modelColumns AS $column => $columnData) {
    
    // group fields into columns for big tables (optional)
    if ($i==0) {
        echo '<div class="col-md-12 form-group">';
    }
    
    // set selected columns to be checked
    $checked=0;
    if (in_array($column,$columns)) {
        $checked=1;
    }

    // restrict sensitive columns that should not be viewed or searched
    if ($column!='password' && $column!='secret') {
            echo '<div class="col-md-3">';
            echo CHtml::checkBox("columns[]",$checked,array('value'=>$column,'id'=>"column_$column"));
            echo CHtml::label(" " . $attributeLabels[$column],"column_$column");
            echo "</div>\r\n";
    }

    // group columns
    $i++;
    if ($i==4) {
        echo '</div>';
        $i=0;
    }
}
// group columns
if ($i!==0) {
    echo '</div>';
}

echo "</fieldset>";
?>
        <div class="form-actions">
                <?php echo BsHtml::submitButton('<i class="fa fa-refresh"></i><span class="hidden-mobile hidden-tablet"> Atualizar Colunas</span>', array('class'=>'btn-success')); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
