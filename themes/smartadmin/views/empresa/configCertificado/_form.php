<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $configCertificado ConfigCertificado */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'config-certificado-form',
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'focus'=>array($configCertificado, 'nome_certificado'),
    'enableAjaxValidation'=>true,
)); ?>


    <?php echo $form->errorSummary($configCertificado); ?>
    
    <?php echo $form->hiddenField($model, 'id'); ?>
<fieldset>
    <legend>Dados para Emissão Certificado</legend>
    <?php echo $form->textFieldControlGroup($configCertificado,'nome_certificado',array(
        'maxlength'=>255,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($configCertificado,'endereco',array(
        'maxlength'=>270,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    
    <?php echo $form->textFieldControlGroup($configCertificado,'numero',array(
        'maxlength'=>20,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    
    <?php echo $form->textFieldControlGroup($configCertificado,'cidade',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($configCertificado,'estado',array(
        'maxlength'=>2,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($configCertificado,'cep', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($configCertificado,'opta_digital', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
</fieldset>
<fieldset>
    <legend>Dados de Validação</legend>
    <?php echo $form->checkBoxControlGroup($configCertificado,'validade', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
    <?php echo $form->checkBoxControlGroup($configCertificado,'aprovacao', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>

<?php $this->endWidget(); ?>
