<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'empresa-form',
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation'=>true,
    'focus'=>array($model, 'nome'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<fieldset>
    <legend>
        Dados Principais
        <div class="pull-right">
        <a id="yt0" class="btn-success btn btn-default" href="<?php echo Yii::app()->createUrl('empresa/view',array('id'=>$model->id))?>">
            <i class="fa fa-eye"></i><span class="hidden-mobile hidden-tablet"> Visualizar</span>
        </a>
        <?php echo BsHtml::submitButton('<i class="fa fa-save"></i><span class="hidden-mobile hidden-tablet"> Salvar</span>', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
        <?php echo BsHtml::linkButton(
                '<i class="fa fa-trash-o"></i><span class="hidden-mobile hidden-tablet"> Excluir</span>',                  
                array(
                    'submit'=>array('empresa/delete', 'id'=>$model->id),
                    'class'=>'btn-danger delete',
                    'confirm'=>'Tem certeza que deseja Excluir a Empresa: ' . $model->nome
                )
            ); ?>
        </div>
    </legend>
    <?php $this->widget('ext.ecollapser.ECollapser', array(
        'target'=>'.collapser',
        'options' => array(
            'mode' => 'block',
            'effect'=>'slide',
        )
    )); ?>
    <div>
    <?php echo $form->textFieldControlGroup($model,'nome',array(
        'maxlength'=>255,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
            'data-html'=>'true',
            'data-original-title'=>'<i class=\'fa fa-check text-success\'></i> Nome apenas para referência pela Polyafer',
            'data-placement'=>'top',
            'rel'=>'tooltip',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'razao_social',array(
        'maxlength'=>255,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'cliente', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'fornecedor', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
    </div>
</fieldset>
<fieldset>
    <legend>Informações para Contato</legend>
    <?php echo BsHtml::button('<i class="fa fa-eye"></i>', array('class'=>'btn-info collapser')) ?>
    <div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Empresa_telefone_comercial">
            <?php echo $model->getAttributeLabel('telefone_comercial'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_tel_comercial', array('placeholder'=>'DDD')); ?>
            <?php // $this->widget('application.widgets.BsMaskedTextField', array(
                //'model'=>$model,
                //'attribute'=>'ddd_tel_comercial',
                //'mask'=>'(99)',
                //'htmlOptions'=>array('placeholder'=>'DDD'),
            //)); ?>
            <?php echo $form->error($model, 'ddd_tel_comercial'); ?>
        </div>
        <div class="col-md-8" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'telefone_comercial',array('maxlength'=>8)); ?>
            <?php echo $form->error($model, 'telefone_comercial'); ?>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Empresa_celular">
            <?php echo $model->getAttributeLabel('celular'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_celular', array('placeholder'=>'DDD')); ?>
            <?php echo $form->error($model, 'ddd_celular'); ?>
        </div>
        <div class="col-md-8" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'celular',array('maxlength'=>9)); ?>
            <?php echo $form->error($model, 'celular'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'email',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'website',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    </div>
</fieldset>
<fieldset>
    <legend>Dados para Nota Fiscal</legend>
    <?php echo BsHtml::button('<i class="fa fa-eye"></i>', array('class'=>'btn-info collapser')) ?>
    <div>
    <?php echo $form->dropDownListControlGroup($model,'tipo_pessoa', $model->getPessoaOptions(), array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'cnpj_cpf',array(
        'maxlength'=>15,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Empresa_inscricao_estadual">
            <?php echo $model->getAttributeLabel('inscricao_estadual'); ?>
        </label>
        <div class="col-md-4" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'inscricao_estadual', array('maxlength'=>14)); ?>
            <?php echo $form->error($model, 'inscricao_estadual'); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->checkBox($model,'isencao_ie',array('label'=>$model->getAttributeLabel('isencao_ie'))); ?>
            <?php echo $form->error($model, 'isencao_ie'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'inscricao_municipal',array(
        'maxlength'=>18,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-4',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'inscricao_suframa',array(
        'maxlength'=>9,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-4',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'opta_simples',array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'isencao_icms', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
    </div>
</fieldset>
<fieldset>
    <legend>Outros</legend>
    <?php echo BsHtml::button('<i class="fa fa-eye"></i>', array('class'=>'btn-info collapser')) ?>
    <div>
    <?php echo $form->textAreaControlGroup($model,'observacao',array('rows'=>6)); ?>
    </div>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>

<?php $this->endWidget(); ?> <!-- form -->
