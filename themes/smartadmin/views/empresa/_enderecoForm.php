<?php $row_id = "endereco-" . $key ?>
<div id="<?php echo $row_id; ?>">
<?php
    echo $form->hiddenField($model, "[$key]id");
    echo $form->updateTypeField($model, $key, "updateType", array('key' => $key));
    ?>
<fieldset>
    <legend>Endereço <?php echo $form->deleteRowButton($row_id, $key, '<i class="fa fa-trash-o"></i><span class="hidden-mobile hidden-tablet"> Excluir</span>', array('class' => 'btn-danger pull-right')); ?></legend>
    <?php echo $form->errorSummary($model); ?>
    <div class="col-md-12 form-group">
        <div class="col-md-offset-2 col-md-2">
            <?php echo $form->checkBox($model,"[$key]tipo_endereco_faturamento",array('label'=>$model->getAttributeLabel('tipo_endereco_faturamento'))); ?>
            <?php echo $form->error($model, "[$key]tipo_endereco_faturamento"); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,"[$key]tipo_endereco_entrega",array('label'=>$model->getAttributeLabel('tipo_endereco_entrega'))); ?>
            <?php echo $form->error($model, "[$key]tipo_endereco_entrega"); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($model,"[$key]tipo_endereco_cobranca",array('label'=>$model->getAttributeLabel('tipo_endereco_cobranca'))); ?>
            <?php echo $form->error($model, "[$key]tipo_endereco_cobranca"); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,"[$key]endereco",array(
        'maxlength'=>150,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]numero",array(
        'maxlength'=>20,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]complemento",array(
        'maxlength'=>50,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]bairro",array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]cidade",array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]estado",array(
        'maxlength'=>2,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,"[$key]cep", array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
</fieldset>
</div>

