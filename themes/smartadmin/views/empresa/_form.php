<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $endereco Endereco */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'empresa-form',
    'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation'=>true,
    'focus'=>array($model, 'nome'),
)); ?>

<?php echo $form->errorSummary(array($model, $endereco)); ?>

<fieldset>
    <legend>Dados Principais
        <div class="pull-right">
            <?php echo BsHtml::submitButton('<i class="fa fa-save"></i><span class="hidden-mobile hidden-tablet"> Salvar</span>', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
        </div>
    </legend>
    <?php $this->widget('ext.ecollapser.ECollapser', array(
        'target'=>'.collapser',
        'options' => array(
            'mode' => 'block',
            'effect'=>'slide',
        )
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array(
        'maxlength'=>255,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
            'data-html'=>'true',
            'data-original-title'=>'<i class=\'fa fa-check text-success\'></i> Nome apenas para referência pela Polyafer',
            'data-placement'=>'top',
            'rel'=>'tooltip',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'razao_social',array(
        'maxlength'=>255,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'cliente', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'fornecedor', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
</fieldset>
<fieldset>
    <legend>Endereço</legend>
    <div class="col-md-12 form-group">
        <div class="col-md-offset-2 col-md-2">
            <?php echo $form->checkBox($endereco,'tipo_endereco_faturamento',array('label'=>$endereco->getAttributeLabel('tipo_endereco_faturamento'))); ?>
            <?php echo $form->error($endereco, 'tipo_endereco_faturamento'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($endereco,'tipo_endereco_entrega',array('label'=>$endereco->getAttributeLabel('tipo_endereco_entrega'))); ?>
            <?php echo $form->error($endereco, 'tipo_endereco_entrega'); ?>
        </div>
        <div class="col-md-2">
            <?php echo $form->checkBox($endereco,'tipo_endereco_cobranca',array('label'=>$endereco->getAttributeLabel('tipo_endereco_cobranca'))); ?>
            <?php echo $form->error($endereco, 'tipo_endereco_cobranca'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($endereco,'endereco',array(
        'maxlength'=>150,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($endereco,'numero',array(
        'maxlength'=>20,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($endereco,'complemento',array(
        'maxlength'=>50,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($endereco,'bairro',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($endereco,'cidade',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($endereco,'estado',array(
        'maxlength'=>2,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($endereco,'cep', array(
        //'data-mask'=>'99.999-999',
        //'data-mask-placeholder'=>'_',
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
</fieldset>
<fieldset>
    <legend>Informações para Contato</legend>
    <?php echo BsHtml::button('<i class="fa fa-eye"></i>', array('class'=>'btn-info collapser')) ?>
    <div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Empresa_telefone_comercial">
            <?php echo $model->getAttributeLabel('telefone_comercial'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_tel_comercial', array('placeholder'=>'DDD')); ?>
            <?php // $this->widget('application.widgets.BsMaskedTextField', array(
                //'model'=>$model,
                //'attribute'=>'ddd_tel_comercial',
                //'mask'=>'(99)',
                //'htmlOptions'=>array('placeholder'=>'DDD'),
            //)); ?>
            <?php echo $form->error($model, 'ddd_tel_comercial'); ?>
        </div>
        <div class="col-md-8" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'telefone_comercial',array('maxlength'=>8)); ?>
            <?php echo $form->error($model, 'telefone_comercial'); ?>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Empresa_celular">
            <?php echo $model->getAttributeLabel('celular'); ?>
        </label>
        <div class="col-md-2" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'ddd_celular', array('placeholder'=>'DDD')); ?>
            <?php echo $form->error($model, 'ddd_celular'); ?>
        </div>
        <div class="col-md-8" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'celular',array('maxlength'=>9)); ?>
            <?php echo $form->error($model, 'celular'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'email',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'website',array(
        'maxlength'=>100,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    </div>
</fieldset>
<fieldset>
    <legend>Dados para Nota Fiscal</legend>
    <?php echo BsHtml::button('<i class="fa fa-eye"></i>', array('class'=>'btn-info collapser')) ?>
    <div>
    <?php echo $form->dropDownListControlGroup($model,'tipo_pessoa', $model->getPessoaOptions(), array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'cnpj_cpf',array(
        'maxlength'=>15,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
    )); ?>
    <div class="col-md-12 form-group">
        <label class="col-md-2 control-label" for="Empresa_inscricao_estadual">
            <?php echo $model->getAttributeLabel('inscricao_estadual'); ?>
        </label>
        <div class="col-md-4" data-html="true" data-original-title="<i class='fa fa-check text-success'></i> Apenas números." data-placement="top" rel="tooltip">
            <?php echo $form->textField($model,'inscricao_estadual', array('maxlength'=>14)); ?>
            <?php echo $form->error($model, 'inscricao_estadual'); ?>
        </div>
        <div class="col-md-6">
            <?php echo $form->checkBox($model,'isencao_ie',array('label'=>$model->getAttributeLabel('isencao_ie'))); ?>
            <?php echo $form->error($model, 'isencao_ie'); ?>
        </div>
    </div>
    <?php echo $form->textFieldControlGroup($model,'inscricao_municipal',array(
        'maxlength'=>18,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-4',
        ),
    )); ?>
    <?php echo $form->textFieldControlGroup($model,'inscricao_suframa',array(
        'maxlength'=>9,
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'labelOptions'=>array(
            'class'=>'col-md-2',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-4',
        ),
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'opta_simples',array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
    <?php echo $form->checkBoxControlGroup($model,'isencao_icms', array(
        'groupOptions'=>array(
            'class'=>'col-md-12',
        ),
        'controlOptions'=>array(
            'class'=>'col-md-10',
        ),
        'checkBoxOptions'=>array(
            'class'=>'col-md-offset-2 col-md-10',
        )
    )); ?>
    </div>
</fieldset>
<fieldset>
    <legend>Outros</legend>
    <?php echo BsHtml::button('<i class="fa fa-eye"></i>', array('class'=>'btn-info collapser')) ?>
    <div>
    <?php echo $form->textAreaControlGroup($model,'observacao',array('rows'=>6)); ?>
    </div>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>

<?php $this->endWidget(); ?> <!-- form -->
