<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $enderecos array of Endereco Model */
/* @var $defaultTab Name of the default tab to be rendered */
?>

<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	$model->nome=>array('view','id'=>$model->id),
	'Editar',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Empresa', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'Visualizar ' . $model->nome, 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Listar Empresas', 'url'=>array('admin')),
);
?>


<?php echo BsHtml::pageHeader('Editar ',$model->nome) ?>
<?php if(Yii::app()->user->hasFlash('inserted')):?>
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-check"></i>
        <strong>Parabéns</strong> <?php echo Yii::app()->user->getFlash('inserted'); ?>
        <a href="javascript:void(0);" class="btn btn-primary btn-xs">Adicionar Mais Empresas  <i class="fa fa-arrow-right"></i></a>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-check"></i>
        <strong>Alterações Salvas</strong> <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('warning')):?>
    <div class="alert alert-warning fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-warning"></i>
        <strong>Atenção</strong> <?php echo Yii::app()->user->getFlash('warning'); ?>
    </div>
<?php endif; ?>


<?php

$this->widget('application.widgets.JarvisTabs', array(
    'tabs' => array(
        '<i class="fa fa-lg fa-list-alt"></i><span class="hidden-mobile hidden-tablet"> Empresa</span>' => array(
            'content' => $this->renderPartial(
                    '_update',
                    array('model'=>$model),
                    true
                    ),
            'id'=>'empresa'
        ),
        '<i class="fa fa-lg fa-truck"></i><span class="hidden-mobile hidden-tablet"> Endereço</span>' => array(
            'content' => $this->renderPartial(
                    'endereco/_update',
                    array('model'=>$model, 'enderecos'=>$enderecos),
                    true
                    ),
            'id' => 'endereco',
        ),
        '<i class="fa fa-lg fa-gears"></i><span class="hidden-mobile hidden-tablet"> Configurações</span>' => array(
            'content' => $this->renderPartial(
                    'configCertificado/_form',
                    array('model'=>$model, 'configCertificado'=>$configCertificado),
                    true
                    ),
            'id' => 'configCertificado',
        ),
    ),
    'defaultTab'=>$defaultTab,
    'id' => 'empresa-wid-01',
    'articleOptions'=>array('class'=>'col-sm-12 col-md-12 col-lg-12'),
    'htmlOptions'=>array(
        JvHtml::DATA_WIDGET_COLORBUTTON => 'false',
        JvHtml::DATA_WIDGET_EDITBUTTON => 'false',
        JvHtml::DATA_WIDGET_CUSTOMBUTTON => 'false',
        JvHtml::DATA_WIDGET_DELETEBUTTON => 'false',
    ),
    'buttonLabel'=>'<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Opções</span>',
    'buttons'=>$this->menu,
    'buttonOptions'=>array(
        'split' => false,
        'size' => BsHtml::BUTTON_SIZE_SMALL,
        'color' => BsHtml::BUTTON_COLOR_PRIMARY,
        'dropup' => false,
        'menuOptions'=>array(
            'pull'=>  BsHtml::PULL_RIGHT,
        ),
        'url'=>'#',
    ),
));
?>