<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $columns array */
/* @var $pageSize int */


$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	'Listar',
);

$this->menu=array(
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Adicionar Empresa', 'url'=>array('create')),
        array('label'=>'<i class="fa fa-table"></i> Exportar Excel', 'url'=>array('exportExcel')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
// add the script to toggle the field list
$('.fields-button').click(function(){
        $('.fields-form').toggle();
        return false;
});
$('.search-form form').submit(function(){
	$('#empresa-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('admin-empresa-01', '<strong>Lista</strong><i> Empresas</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>
    <div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Adicionar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>Yii::app()->createUrl('empresa/create'),
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
            <?php echo BsHtml::button('<i class="fa fa-cogs"></i><span class="hidden-mobile hidden-tablet"> Colunas</span>', array('class' => 'fields-button', 'color'=>BsHtml::BUTTON_COLOR_INFO), '#'); ?>
    </div>
    
    <div class="search-form" style="display:none">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
    </div><!-- search-form -->
    <div class="fields-form" style="display:none">
        <?php
        $this->renderPartial('_fields', array(
            'model' => $model,
            'columns' => $columns,
        ));
        ?>
    </div>

    <div class="row">
        <div class="col-md-12">
        <?php 
        /**
         * Exist a extension that use Datatables.net (the same plugin used by the theme SmartAdmin)
         * @see https://github.com/nineinchnick/edatatables
         *  
         */ 
        ?>
        <?php
            // here we create an array of columns for the grid view
            $modelColumns=$model->getMetaData()->columns;
            $myColumnsOptions = array(
                'id' => array(
                    'name'=>'id',
                    'htmlOptions'=>array('width'=>'80'),
                ),
                'data_criacao' => array(
                    'name'=>'data_criacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'data_atualizacao' => array(
                    'name'=>'data_atualizacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'usuario_criacao' => array(
                    'name'=>'usuario_criacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'usuario_atualizacao' => array(
                    'name'=>'usuario_atualizacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'nome' => array(
                    'name'=>'nome',
                    'htmlOptions'=>array('width'=>'240'),
                ),
                'cliente' => array(
                    'name'=>'cliente',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'fornecedor' => array(
                    'name'=>'fornecedor',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'num_cadastro' => array(
                    'name'=>'num_cadastro',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'razao_social' => array(
                    'name'=>'razao_social',
                    'htmlOptions'=>array('width'=>'300'),
                ),
                'tipo_pessoa' => array(
                    'name'=>'tipo_pessoa',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'cnpj_cpf' => array(
                    'name'=>'cnpj_cpf',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'ddd_tel_comercial' => array(
                    'name'=>'ddd_tel_comercial',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'telefone_comercial' => array(
                    'name'=>'telefone_comercial',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'ddd_celular' => array(
                    'name'=>'ddd_celular',
                    'htmlOptions'=>array('width'=>'60'),
                ),
                'celular' => array(
                    'name'=>'celular',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'email' => array(
                    'name'=>'email',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'website' => array(
                    'name'=>'website',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'inscricao_estadual' => array(
                    'name'=>'inscricao_estadual',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'isencao_ie' => array(
                    'name'=>'isencao_ie',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'inscricao_municipal' => array(
                    'name'=>'inscricao_municipal',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'inscricao_suframa' => array(
                    'name'=>'inscricao_suframa',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'opta_simples' => array(
                    'name'=>'opta_simples',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'isencao_icms' => array(
                    'name'=>'isencao_icms',
                    'htmlOptions'=>array('width'=>'120'),
                ),
                'observacao' => array(
                    'name'=>'data_criacao',
                    'htmlOptions'=>array('width'=>'120'),
                ),
            );
            
            foreach ($myColumnsOptions AS $columnAttribute => $columnOptions) {
                if (in_array($columnAttribute, $columns)) {
                    $key=array_search($columnAttribute,$columns);
                    $columns[$key] = $columnOptions;
                }
            }

//            foreach ($modelColumns AS $columnName => $columnData) {
//
//                if (in_array($columnName,$columns)) {
//                    $key=array_search($columnName,$columns);
//                    
//                    if ($columnData->dbType=='tinyint(1)') {
//                        // (mysql) if it's set as a boolean, filter with a select
//                        $columns[$key]=array('name'=>$columnName,'filter'=>array('1'=>'Yes','0'=>'No'),'value'=>'($data->'.$columnName.'=="1")?("Yes"):("No")');
//                    } elseif ($columnData->dbType=='date') {
//                        //if it's a date field, filter with a date picker
//                        $columns[$key]=array(
//                            'name' => $columnName,
//                            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                'model'=>$model, 
//                                'attribute'=>$columnName, 
//                                'language' => 'ja',
//                                'i18nScriptFile' => 'jquery.ui.datepicker-ja.js',
//                                'htmlOptions' => array(
//                                    'id' => 'datepicker_for_'.$columnName,
//                                    'size' => '10',
//                                ),
//                                'defaultOptions' => array(
//                                    'showOn' => 'focus', 
//                                    'dateFormat' => 'yy-mm-dd',
//                                    'showOtherMonths' => true,
//                                    'selectOtherMonths' => true,
//                                    'changeMonth' => true,
//                                    'changeYear' => true,
//                                    'showButtonPanel' => true,
//                                )
//                            ), 
//                            true),
//                        );
//                    } elseif (stristr($columnData->dbType,'decimal')) {
//                        // if it's a decimal, align text to the right
//                        $columns[$key]=array('name'=>$columnName,'htmlOptions'=>array('style' => 'text-align: right;'));
//                    } elseif (stristr($columnName,'email')) {
//                        // if it's an email, link it to the email
//                        $columns[$key]="$columnName:email";
//                    }
//                    // add more custom column types here
//                }
//            }

            // add the button column
            // set the button column header to be a drop down to select page size
            // don't forget to change 'my-model-grid' to reflect your grid 'id'
            $columns[]=array(
                            'class'=>'bootstrap.widgets.BsButtonColumn',
                            'htmlOptions'=>array('width'=>'145px'),
                            'viewButtonOptions'=>array('class'=>'btn btn-xs txt-color-green'),
                            'updateButtonOptions'=>array('class'=>'btn btn-xs txt-color-blue'),
                            'deleteButtonOptions'=>array('class'=>'btn btn-xs txt-color-red'),
                            'header'=>BsHtml::dropDownList(
                                    'pageSize',
                                    $pageSize,
                                    array(10=>10,20=>20,50=>50,100=>100), 
                                    array('onchange'=>"$.fn.yiiGridView.update('file-grid',{ data:{pageSize: $(this).val() }})")),
                    );

            $this->widget('bootstrap.widgets.BsGridView',array(
                        'id'=>'empresa-grid',
                        'type'=>array(BsHtml::GRID_TYPE_BORDERED, BsHtml::GRID_TYPE_CONDENSED),
                        'dataProvider'=>$model->search(),
                        'filter'=>$model,
                        'ajaxUpdate'=>true, //pode usar falso caso a versão do jQuery dê problemas
                        'emptyTagName'=>'tr',
                        'emptyText'=>'<td class="dataTables_empty" valign="top" colspan="7"><i class="fa fa-warning text-warning"></i> Nenhum resultado encontrado</td>',
                        'enableHistory'=>true, //??? não entendi!
                        'selectableRows'=>0,
                        'htmlOptions'=>array('class'=>'table-responsive'),
                        'columns'=>$columns,
                        //Dicas de tabela responsiva: http://elvery.net/demo/responsive-tables/
                        /*
                        'columns'=>array(
                                array(
                                    'name'=>'id',
                                    'htmlOptions'=>array('width'=>'80'),
                                ),
                                //'data_criacao',
                                //'data_atualizacao',
                                //'usuario_criacao',
                                //'usuario_atualizacao',
                                array(
                                    'name'=>'nome',
                                    'htmlOptions'=>array('width'=>'240'),
                                ),
                                //'cliente',
                                //'fornecedor',
                                //'num_cadastro',
                                array(
                                    'name'=>'razao_social',
                                    'htmlOptions'=>array('width'=>'300'),
                                ),
                                //'tipo_pessoa',
                                //'cnpj_cpf',
                                array(
                                    'name'=>'ddd_tel_comercial',
                                    'htmlOptions'=>array('width'=>'60'),
                                ),
                                array(
                                    'name'=>'telefone_comercial',
                                    'htmlOptions'=>array('width'=>'120'),
                                ),
                                //'ddd_celular',
                                //'celular',
                                'email',
                                //'website',
                                //'inscricao_estadual',
                                //'isencao_ie',
                                //'inscricao_municipal',
                                //'inscricao_suframa',
                                //'opta_simples',
                                //'isencao_icms',
                                //'observacao',
                                array(
                                        'class'=>'bootstrap.widgets.BsButtonColumn',
                                        'htmlOptions'=>array('width'=>'145px'),
                                        'viewButtonOptions'=>array('class'=>'btn btn-xs txt-color-green'),
                                        'updateButtonOptions'=>array('class'=>'btn btn-xs txt-color-blue'),
                                        'deleteButtonOptions'=>array('class'=>'btn btn-xs txt-color-red'),
                                        'header'=>BsHtml::dropDownList(
                                                'pageSize',
                                                $pageSize,
                                                array(10=>10,20=>20,50=>50,100=>100), 
                                                array('onchange'=>"$.fn.yiiGridView.update('file-grid',{ data:{pageSize: $(this).val() }})")),
                                ),
                        ),
                         * 
                         */
        )); ?>
        </div>
</div>
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>




