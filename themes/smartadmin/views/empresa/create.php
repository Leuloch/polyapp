<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $endereco Endereco */
?>

<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	'Adicionar',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Empresa', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Empresa', 'url'=>array('admin')),
);
?>
<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget'); ?>

<?php $jarvis->beginJarvisWidget('empresa-create-01', BsHtml::pageHeaderJarvis('Cadastrar','Empresa'), array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    )
); ?>

<?php $this->renderPartial('_form', array('model'=>$model, 'endereco'=>$endereco)); ?>

<?php $jarvis->endJarvisWidget(); ?>

<?php $this->endWidget(); ?>