<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'unidadeDialog',
                'options'=>array(
                    'title'=>'Adicionar Unidade de Medida',
                    'closeOnEscape'=>true,
                    'autoOpen'=>true,
                    'modal'=>'true',
                    'width'=>'auto',
                    'height'=>'auto',
                    'close'=>"js:function(e,ui){jQuery('body').undelegate('#closeUnidadeDialog','click'); jQuery('#unidadeDialog').empty();}"
                ),
                ));
echo $this->renderPartial('_formDialog', array('model'=>$model)); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
