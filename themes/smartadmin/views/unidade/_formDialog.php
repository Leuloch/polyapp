<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'unidade-form',
    'enableAjaxValidation'=>true,
)); ?>

    <?php echo $form->errorSummary($model); ?>
<fieldset>
    <?php echo $form->textFieldControlGroup($model,'simbolo',array('maxlength'=>5)); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>45)); ?>
    <?php echo $form->dropDownListControlGroup($model,'grandeza_id', CHtml::listData(Grandeza::model()->findAll(),'id','grandeza'), array('prompt'=>'Selecione a Grandeza')); ?>
</fieldset>
<div class="form-actions">
    <?php echo BsHtml::ajaxSubmitButton(
            'Adicionar',
            CHtml::normalizeUrl(array('unidade/addnew','render'=>false)),array('success'=>'js: function(data) {
                        $("[id$=\'unidade_escala_id\']").append(data);
                        $("[id$=\'unidade_div_id\']").append(data);
                        $("#unidadeDialog").dialog("close");
                    }'),
            array(
                'id'=>'closeUnidadeDialog',
                'color'=>  BsHtml::BUTTON_COLOR_PRIMARY,
            )); 
    ?>
</div>
<?php $this->endWidget(); ?>

    