<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Restore");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Restore"),
);
?>

<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
<div class="well">
    <div class="alert alert-success fade in">
        <button class="close" data-dismiss="alert">×</button>
        <i class="fa-fw fa fa-check"></i>
        <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
    </div>
    <?php echo BsHtml::linkButton('<i class="fa fa-arrow-circle-left"></i> Retornar para Login', array('url'=>Yii::app()->createUrl('user/login'), 'color'=>  BsHtml::BUTTON_COLOR_PRIMARY)); ?>
</div>
<?php else: ?>
<div class="well no-padding">
<?php echo BsHtml::beginFormBs(BsHtml::FORM_LAYOUT_HORIZONTAL,'','post',array('class'=>'smart-form client-form')); ?>
    <header><h1><?php echo UserModule::t("Restore"); ?></h1></header>
     <fieldset>
        <section>
            <?php echo BsHtml::errorSummary($form); ?>
        </section>
	<section>
		<?php echo CHtml::activeLabel($form,'login_or_email', array('class'=>'label')); ?>
            <label class="input">
                <i class="icon-append fa fa-envelope"></i>
		<?php echo CHtml::activeTextField($form,'login_or_email') ?>
		<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> <?php echo UserModule::t("Please enter your login or email addres."); ?></b>
            </label>
            
	</section>
    </fieldset>
    <footer>
        
        <?php echo BsHtml::submitButton(UserModule::t("Restore"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
        
    </footer>

<?php echo BsHtml::endForm(); ?>
</div><!-- form -->
<?php endif; ?>