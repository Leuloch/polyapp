<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	$model->username,
);


$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create'), 'icon' => 'glyphicon glyphicon-plus-sign'),
    array('label'=>UserModule::t('Update User'), 'url'=>array('update','id'=>$model->id), 'icon' => 'glyphicon glyphicon-edit'),
    array('label'=>UserModule::t('Delete User'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?')), 'icon' => 'glyphicon glyphicon-trash'),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'glyphicon glyphicon-user'),
    //array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'), 'icon' => 'icon-edit'),
    //array('label'=>UserModule::t('List User'), 'url'=>array('/user'), 'icon' => 'icon-th-list'),
);

$this->layout='//layouts/column1';
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('view-user-01', '<strong>Visualizar</strong><i> Usuário</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

<div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-edit"></i><span class="hidden-mobile hidden-tablet"> Editar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>Yii::app()->createUrl('/user/admin/update') . "/id/" . $model->id,
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
</div>
<h1><?php echo UserModule::t('View User').' "'.$model->username.'"'; ?></h1>
<?php
 
	$attributes = array(
		'id',
		'username',
	);
	
	$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
	if ($profileFields) {
		foreach($profileFields as $field) {
			array_push($attributes,array(
					'label' => UserModule::t($field->title),
					'name' => $field->varname,
					'type'=>'raw',
					'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
				));
		}
	}
	
	array_push($attributes,
		'password',
		'email',
		'activkey',
		'create_at',
		'lastvisit_at',
		array(
			'name' => 'superuser',
			'value' => User::itemAlias("AdminStatus",$model->superuser),
		),
		array(
			'name' => 'status',
			'value' => User::itemAlias("UserStatus",$model->status),
		)
	);
	
	$this->widget('bootstrap.widgets.BsDetailView', array(
		'data'=>$model,
		'attributes'=>$attributes,
	));
	

?>

<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>