<?php $form=$this->beginWidget('BsActiveForm', array(
	'id'=>'user-form',
        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>

	<?php echo $form->errorSummary(array($model,$profile)); ?>
<fieldset>
    <legend>Dados Principais</legend>
	<?php echo $form->textFieldControlGroup($model,'username',array(
            'size'=>20,
            'maxlength'=>20,
            'groupOptions'=>array('class'=>'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-2'),
            'controlOptions'=>array('class'=>'col-md-10'),
        )); ?>

        <?php echo $form->passwordFieldControlGroup($model,'password',array(
            'size'=>60,
            'maxlength'=>128,
            'groupOptions'=>array('class'=>'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-2'),
            'controlOptions'=>array('class'=>'col-md-10'),
        )); ?>

        <?php echo $form->textFieldControlGroup($model,'email',array(
            'size'=>60,
            'maxlength'=>128,
            'groupOptions'=>array('class'=>'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-2'),
            'controlOptions'=>array('class'=>'col-md-10'),
        )); ?>

</fieldset>
<fieldset>
    <legend>Acesso</legend>
        <?php echo $form->dropDownListControlGroup($model,'superuser',User::itemAlias('AdminStatus'), array(
            'groupOptions'=>array('class'=>'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-2'),
            'controlOptions'=>array('class'=>'col-md-10'),
        )); ?>

        <?php echo $form->dropDownListControlGroup($model,'status',User::itemAlias('UserStatus'), array(
            'groupOptions'=>array('class'=>'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-2'),
            'controlOptions'=>array('class'=>'col-md-10'),
        )); ?>
		
</fieldset>
<fieldset>
    <legend>Demais Campos</legend>
<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
		<?php 
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownListControlGroup($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo $form->textAreaControlGroup($profile,$field->varname,array('rows'=>6, 'cols'=>50));
		} else {
			echo $form->textFieldControlGroup($profile,$field->varname,array(
                            'size'=>60,
                            'maxlength'=>(($field->field_size)?$field->field_size:255),
                            'groupOptions'=>array('class'=>'col-md-12'),
                            'labelOptions'=>array('class'=>'col-md-2'),
                            'controlOptions'=>array('class'=>'col-md-10'),
                        ));
		}
		 ?>
			<?php
			}
		}
?>
</fieldset>
<div class="form-actions">
        <?php echo BsHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)) ;?>
</div>

<?php $this->endWidget(); ?>