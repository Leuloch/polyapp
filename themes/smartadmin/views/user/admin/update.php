<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('admin'),
	$model->username=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create'), 'icon' => 'glyphicon glyphicon-plus-sign'),
    array('label'=>UserModule::t('View User'), 'url'=>array('view','id'=>$model->id), 'icon' => 'glyphicon glyphicon-eye-open'),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'glyphicon glyphicon-user'),
    //array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'), 'icon' => 'icon-edit'),
    //array('label'=>UserModule::t('List User'), 'url'=>array('/user'), 'icon' => 'icon-th-list'),
);
$this->layout='//layouts/column1';

?>
<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget'); ?>

<?php $jarvis->beginJarvisWidget('user-update-01', BsHtml::pageHeaderJarvis(UserModule::t('Update User'),$model->id), array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    )
); ?>
    <div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-tasks"></i><span class="hidden-mobile hidden-tablet"> Listar Usuários</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>Yii::app()->createUrl('/user/admin'),
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
    </div>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>

<?php $jarvis->endJarvisWidget(); ?>

<?php $this->endWidget(); ?>