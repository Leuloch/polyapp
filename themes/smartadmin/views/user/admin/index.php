<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('/user'),
	UserModule::t('Manage'),
);

$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create'), 'icon' => 'glyphicon glyphicon-plus-sign'),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'glyphicon glyphicon-user'),
    array('label'=>'Editar Permissões', 'url'=>array('/auth'), 'icon' => 'glyphicon glyphicon-edit'),
    //array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'), 'icon' => 'icon-edit'),
    //array('label'=>UserModule::t('List User'), 'url'=>array('/user'), 'icon' => 'icon-th-list'),
);
$this->layout='//layouts/column1';

?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget') ?>
<?php $jarvis->beginJarvisWidget('admin-user-01', '<strong>Lista</strong><i> Usuários</i>', array(
    JvHtml::DATA_WIDGET_COLLAPSED=>'false',
    JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
    JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
    JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
    JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false',
    ),
    //option 'no-padding' in the class 
    array('class'=>'')
); ?>

    <div class="widget-body-toolbar">
            <?php echo BsHtml::buttonDropdown('<i class="fa fa-plus"></i><span class="hidden-mobile hidden-tablet"> Adicionar</span>', $this->menu, array(
                'split' => true,
                'size' => BsHtml::BUTTON_SIZE_DEFAULT,
                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                'url'=>Yii::app()->createUrl('user/admin/create'),
                'menuOptions'=>array(
                    'pull'=>  BsHtml::PULL_LEFT,
                ),
            )); ?>
    </div>

<?php $this->widget('bootstrap.widgets.BsGridView', array(
        'type' => 'striped hover',
        'filter'=>$model,
        'dataProvider'=>$model->search(),
        'type'=>array(BsHtml::GRID_TYPE_BORDERED, BsHtml::GRID_TYPE_CONDENSED),
        'ajaxUpdate'=>true, //pode usar falso caso a versão do jQuery dê problemas
        'emptyTagName'=>'tr',
        'emptyText'=>'<td class="dataTables_empty" valign="top" colspan="7"><i class="fa fa-warning text-warning"></i> Nenhum resultado encontrado</td>',
        'enableHistory'=>true, //??? não entendi!
        'selectableRows'=>0,
        'htmlOptions'=>array('class'=>'table-responsive'),
        'columns'=>array(
		array(
			'name' => 'id',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
		),
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(UHtml::markSearch($data,"username"),array("admin/view","id"=>$data->id))',
		),
		array(
			'name'=>'email',
			'type'=>'raw',
			'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
		),
		'create_at',
		'lastvisit_at',
		array(
			'name'=>'superuser',
			'value'=>'User::itemAlias("AdminStatus",$data->superuser)',
			'filter'=>User::itemAlias("AdminStatus"),
		),
		array(
			'name'=>'status',
			'value'=>'User::itemAlias("UserStatus",$data->status)',
			'filter' => User::itemAlias("UserStatus"),
		),
		array(
			'class'=>'bootstrap.widgets.BsButtonColumn',
                        'htmlOptions'=>array('width'=>'100px'),
                        'viewButtonOptions'=>array('class'=>'btn btn-xs txt-color-green'),
                        'updateButtonOptions'=>array('class'=>'btn btn-xs txt-color-blue'),
                        'deleteButtonOptions'=>array('class'=>'btn btn-xs txt-color-red'),
		),
	),
)); ?>

<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>