<?php
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login");
?>


<div class="well no-padding">
    <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'login-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array('class'=>'smart-form client-form'),
        'focus'=>array($model, 'username'),
    )); ?>
    
    <header>
        <h1><?php echo UserModule::t("Login"); ?></h1>
    </header>
    <fieldset>
        <section>
            <?php //echo $form->errorSummary($model); ?>
        </section>
        <section>
            <?php echo $form->textFieldControlGroup($model, 'username', array(
                'append'=>  BsHtml::icon(BsHtml::GLYPHICON_USER),
                'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
                'maxlength'=>32,
            )); ?>
        </section>
        <section>
            <?php echo $form->passwordFieldControlGroup($model, 'password', array(
                'append'=>  BsHtml::icon(BsHtml::GLYPHICON_LOCK),
                'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
                'maxlength'=>32,
            )); ?>
            <div class="note">
                <?php echo BsHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl); ?>
            </div>
        </section>
        <section>
            <?php //echo $form->checkBoxControlGroup($model, 'rememberMe'); ?>
        </section>
        <section>
            <label class="checkbox">
                <input type="checkbox" name="UserLogin[rememberMe]" value="1">
                <i></i>
                Manter-me conectado por 30 dias.
            </label>
        </section>
    </fieldset>
    <footer>
        <?php echo BsHtml::submitButton(UserModule::t("Login"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
    </footer>
</div>
<?php $this->endWidget(); ?>