<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
	UserModule::t("Registration"),
);
?>
<div class="well no-padding">
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'layout'=>  BsHtml::FORM_LAYOUT_VERTICAL,
        'htmlOptions'=>array(
                'class'=>'smart-form client-form',
        //configurações originais do form
                'enctype'=>'multipart/form-data',
        ),
        'id'=>'registration-form',
	'enableAjaxValidation'=>true,	
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
                'validateOnChange'=>true,
        ),
));?>

<header>
    <h1><?php echo UserModule::t("Registration"); ?></h1>
</header>
<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>
<fieldset>
        <section>
        <?php echo $form->textFieldControlGroup($model, 'username', array(
            'append'=>  BsHtml::icon(BsHtml::GLYPHICON_USER),
            'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
        )); ?>
        </section>
        <section>
        <?php echo $form->passwordFieldControlGroup($model, 'password', array(
            'help'=>UserModule::t("Minimal password length 4 symbols."),
            'append'=>  BsHtml::icon(BsHtml::GLYPHICON_LOCK),
            'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
        )); ?>
	</section>
        <section>
        <?php echo $form->passwordFieldControlGroup($model, 'verifyPassword', array(
            'append'=>  BsHtml::icon(BsHtml::GLYPHICON_LOCK),
            'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
        )); ?>        
	</section>
        <section>
        <?php echo $form->textFieldControlGroup($model, 'email', array(
            'append'=>  BsHtml::icon(BsHtml::GLYPHICON_ENVELOPE),
            'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
        )); ?>        	
	</section>
</fieldset>
<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
                    echo "<fieldset>";
			foreach($profileFields as $field) {
			?>
    <section class="col col-6">
		<?php 
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownListControlGroup($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo $form->textAreaControlGroup($profile,$field->varname);
		} else {
			echo $form->textFieldControlGroup($profile,$field->varname, array(
                            'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
                            //'groupOptions'=>array('class'=>'col-xs-12 col-sm-12 col-md-6 col-lg-6')
                            ));
		}
		 ?>
        </section>
			<?php
			}
                    echo "</fieldset>";
		}
?>
    <fieldset>
	<?php if (UserModule::doCaptcha('registration')): ?>		
		
                <?php echo $form->textFieldControlGroup($model, 'verifyCode',array(
                    'help'=>UserModule::t("Please enter the letters as they are shown in the image above.").'<br/>'.
                        UserModule::t("Letters are not case-sensitive."),
                    'labelOptions'=>array('class'=>'hidden-desktop hidden-mobile hidden-tablet'),
                )); ?>
                <?php $this->widget('CCaptcha'); ?>
	<?php endif; ?>
    </fieldset>
    <footer>
    <?php echo BsHtml::submitButton(UserModule::t("Register"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY));?>	
    </footer>
<?php $this->endWidget(); ?>
</div>
<?php endif; ?>