<h1><?php echo UserModule::t("Change password"); ?></h1>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
	'id'=>'changepassword-form',
        'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
        'action'=>array('/user/profile/changepassword'),
	'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>
<fieldset>
	<?php echo $form->passwordFieldControlGroup($model,'oldPassword', array(
            'controlOptions'=>array('class'=> 'col-md-12'),
            'groupOptions'=>array('class'=> 'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-12')
        )); ?>
	
	<?php echo $form->passwordFieldControlGroup($model,'password', array(
            'help'=>UserModule::t("Minimal password length 4 symbols."),
            'controlOptions'=>array('class'=> 'col-md-12'),
            'groupOptions'=>array('class'=> 'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-12')
        )); ?>

	<?php echo $form->passwordFieldControlGroup($model,'verifyPassword', array(
            'controlOptions'=>array('class'=> 'col-md-12'),
            'groupOptions'=>array('class'=> 'col-md-12'),
            'labelOptions'=>array('class'=>'col-md-12')
        )); ?>
</fieldset>
<div class="form-actions">
	<?php echo BsHtml::submitButton(UserModule::t("Save"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget(); ?><!-- form -->
<?php if (Yii::app()->user->hasFlash('profileMessage')) {
    Yii::app()->clientScript->registerScript('changePassword',"
        $.smallBox({
                title : 'James Simmons liked your comment',
                content : '<i class='fa fa-clock-o'></i> <i>2 seconds ago...</i>',
                color : '#296191',
                iconSmall : 'fa fa-thumbs-up bounce animated',
                timeout : 4000
        });
    ", CClientScript::POS_READY);
};
?>