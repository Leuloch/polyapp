<?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
    'id'=>'preferences-form',
    'enableAjaxValidation'=>true,
    'action'=>array('/user/profile/preferences'),
)); 
?>

<fieldset>
    <legend>Configurações de Layout do Sistema</legend>
    <?php echo $form->checkBoxControlGroup($model, 'fixed_header', array(
        'checkBoxOptions'=>array('class'=> 'col-md-offset-2 col-md-10'),
        'groupOptions'=>array('class'=> 'col-md-12'),
        )); 
    ?>
    
    <?php echo $form->checkBoxControlGroup($model, 'fixed_ribbon', array(
        'checkBoxOptions'=>array('class'=> 'col-md-offset-2 col-md-10'),
        'groupOptions'=>array('class'=> 'col-md-12'),
        )); 
    ?>
    
    <?php echo $form->checkBoxControlGroup($model, 'fixed_navigation', array(
        'checkBoxOptions'=>array('class'=> 'col-md-offset-2 col-md-10'),
        'groupOptions'=>array('class'=> 'col-md-12'),
        )); 
    ?>
    
    <?php echo $form->dropDownListControlGroup($model, 'skin', $model->getSkinOptions() ,array(
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-12'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
</fieldset>

<div class="form-actions">
        <?php echo BsHtml::submitButton('Salvar e Aplicar Mudanças', array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget(); ?>