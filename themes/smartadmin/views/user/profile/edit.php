<h1><?php echo UserModule::t('Edit profile'); ?></h1>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<!-- Form in bootstrap version -->
<?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'layout' => BsHtml::FORM_LAYOUT_VERTICAL,
    'id'=>'profile-bootstrap-form',
    'enableAjaxValidation'=>true,
    'htmlOptions'=>array(
        'enctype'=>'multipart/form-data',
    ),
    'action'=>array('/user/profile/edit'),
)); ?>

<fieldset>
<?php echo $form->errorSummary(array($model,$profile)); ?>

<?php 
    $profileFields = $profile->getFields();
    if ($profileFields) {
        foreach ($profileFields as $field) {
            if ($widgetEdit = $field->widgetEdit($profile)) {
                echo $widgetEdit;
            } elseif ($field->range) {
                echo $form->dropDownListControlGroup($profile, $field->varname, Profile::range($field->range), array(
                    'controlOptions'=>array('class'=> 'col-md-12'),
                    'groupOptions'=>array('class'=> 'col-md-6'),
                    'labelOptions'=>array('class'=>'col-md-12')
                ));
            } elseif ($field->field_type == "TEXT") {
                echo $form->textAreaControlGroup($profile, $field->varname, array(
                    'rows'=>6,
                    'cols'=>50,
                ));
            } else {
                echo $form->textFieldControlGroup($profile, $field->varname, array(
                    'maxlength'=>(($field->field_size)?$field->field_size:255),
                    'controlOptions'=>array('class'=> 'col-md-12'),
                    'groupOptions'=>array('class'=> 'col-md-6'),
                    'labelOptions'=>array('class'=>'col-md-12')
                ));
            }
        }
    }
?>
<?php echo $form->textFieldControlGroup($model, 'username', array(
    'maxlength'=>20,
    'controlOptions'=>array('class'=> 'col-md-12'),
    'groupOptions'=>array('class'=> 'col-md-12'),
    'labelOptions'=>array('class'=>'col-md-12')
    
    )); 
?>

<?php echo $form->fileFieldControlGroup($model, 'filename', array(
    'controlOptions'=>array('class'=> 'col-md-12'),
    'groupOptions'=>array('class'=> 'col-md-12'),
    'labelOptions'=>array('class'=>'col-md-12')
)); 
?>    
    
<?php echo $form->textFieldControlGroup($model, 'email', array(
    'maxlength'=>128,
    'controlOptions'=>array('class'=> 'col-md-12'),
    'groupOptions'=>array('class'=> 'col-md-12'),
    'labelOptions'=>array('class'=>'col-md-12')
    )); 
?>
    
</fieldset>
<fieldset>
    <legend>Demais campos</legend>
    <?php echo $form->textFieldControlGroup($model, 'ddd_phone', array(
        'maxlength'=>128,
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-6'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
    
    <?php echo $form->textFieldControlGroup($model, 'phone', array(
        'maxlength'=>128,
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-6'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
    
    <?php echo $form->textFieldControlGroup($model, 'ddd_mobile', array(
        'maxlength'=>128,
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-6'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
    
    <?php echo $form->textFieldControlGroup($model, 'mobile', array(
        'maxlength'=>128,
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-6'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
    
    <?php echo $form->textFieldControlGroup($model, 'skype', array(
        'maxlength'=>128,
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-12'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
    
    <?php echo $form->textFieldControlGroup($model, 'facebook', array(
        'maxlength'=>128,
        'controlOptions'=>array('class'=> 'col-md-12'),
        'groupOptions'=>array('class'=> 'col-md-12'),
        'labelOptions'=>array('class'=>'col-md-12')
        )); 
    ?>
</fieldset>

<div class="form-actions">
        <?php echo BsHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY, 'size' => BsHtml::BUTTON_SIZE_LARGE)); ?>
</div>
<?php $this->endWidget(); ?>
<!-- End of Form bootstrap -->
