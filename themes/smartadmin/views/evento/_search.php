<?php
/* @var $this EventoController */
/* @var $model Evento */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'search-event-form',
    'enableAjaxValidation'=>false,
    'action'=>array('Evento/search'),
    'method'=>'get'
)); ?>

<?php echo $form->textFieldControlGroup($model,'nome',array(
    'maxlength'=>100,
    'groupOptions'=>array(
        'class'=>'col-md-4',
    ),
    'labelOptions'=>array(
        'class'=>'col-md-2 hide',
    ),
    'controlOptions'=>array(
        'class'=>'col-md-10',
    ),
)); ?>
<?php echo $form->textFieldControlGroup($model,'tipo', array(
    'groupOptions'=>array(
        'class'=>'col-md-4',
    ),
    'labelOptions'=>array(
        'class'=>'col-md-2 hide',
    ),
    'controlOptions'=>array(
        'class'=>'col-md-10',
    ),
)); ?>
<?php echo $form->textFieldControlGroup($model,'status',array(
    'groupOptions'=>array(
        'class'=>'col-md-3',
    ),
    'labelOptions'=>array(
        'class'=>'col-md-2 hide',
    ),
    'controlOptions'=>array(
        'class'=>'col-md-10',
    ),
)); ?>

<?php echo BsHtml::Button('Filtrar', array(
    'color' => BsHtml::BUTTON_COLOR_PRIMARY,
    'id'=> 'filterEvents'
)); ?>

<?php $this->endWidget(); ?>
