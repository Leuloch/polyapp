<?php
/* @var $this EventoController */
/* @var $model Evento */
?>

<?php
$this->breadcrumbs=array(
	'Eventos',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Evento', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Evento', 'url'=>array('admin')),
);
?>

<?php if (Yii::app()->user->hasFlash('eventEditSuccess')) {
    $msg = Yii::app()->user->getFlash('eventEditSuccess');
    Yii::app()->clientScript->registerScript('editEventModal',"
        $.smallBox({
                title : 'Alerta!',
                content : '$msg',
                color : '#296191',
                iconSmall : 'fa fa-thumbs-up bounce animated',
                timeout : 4000
        });
    ", CClientScript::POS_READY);
};
?>
<?php if (Yii::app()->user->hasFlash('eventEditError')) {
    $msg = Yii::app()->user->getFlash('eventEditError');
    Yii::app()->clientScript->registerScript('editEventModal',"
        $.smallBox({
                title : 'Erro!',
                content : '$msg',
                color : '#C46A69',
                iconSmall : 'fa fa-warning shake animated',
                timeout : 4000
        });
    ", CClientScript::POS_READY);
};
?>

<?php $jarvis = $this->beginWidget('application.widgets.jarvisWidget', array('sectionOptions'=>array('id'=>'widget-grid'))); ?>
<?php $jarvis->beginJarvisWidget(
        'index-evento-01', 
        '<strong>Adicionar</strong><i> Eventos</i>', 
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false'
        ),
    //option for body widget 'no-padding' in the class 
    array('class'=>''),
    //option for article widget
    array('class'=>'col-sm-12 col-md-12 col-lg-12')
); ?>

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<form id="add-event-form2">
        <fieldset>
                <div class="form-group">
                        <label>Titulo para o Evento</label>
                        <input class="form-control"  id="title" name="title" maxlength="40" type="text" placeholder="Título do Evento">
                </div>
                <div class="form-group">
                        <label>Descrição do evento</label>
                        <textarea class="form-control" placeholder="Por favor, seja breve" rows="3" maxlength="40" id="description"></textarea>
                        <p class="note">Tamanho máximo de 40 caracteres.</p>
                </div>

        </fieldset>
        <div class="form-actions">
                <div class="row">
                        <div class="col-md-12">
                                <button class="btn btn-default" type="button" id="add-event" >
                                        Criar evento
                                </button>
                        </div>
                </div>
        </div>
</form>

<div class="well well-sm" id="event-container">
        <form>
                <fieldset>
                        <legend>
                                Arraste os eventos para o calendário
                        </legend>
                        <ul id='external-events' class="list-unstyled">
                                <li>
                                        <span class="bg-color-darken txt-color-white" data-description="Em qual Empresa?" data-icon="fa-calendar">CALIBRAÇÃO <i class="fa fa-calendar txt-color-white"></i></span>
                                </li>
                                <li>
                                        <span class="bg-color-blue txt-color-white" data-description="Em qual Empresa?" data-icon="fa-calendar">QUALIFICAÇÃO <i class="fa fa-calendar txt-color-white"></i></span>
                                </li>
                                <li>
                                        <span class="bg-color-orange txt-color-white" data-description="Forneça breve descrição" data-icon="fa-calendar">MANUTENÇÃO <i class="fa fa-calendar txt-color-white"></i></span>
                                </li>
                                <li>
                                        <span class="bg-color-greenLight txt-color-white" data-description="Qual o assunto?" data-icon="fa-calendar">TREINAMENTO <i class="fa fa-calendar txt-color-white"></i></span>
                                </li>
                                <li>
                                        <span class="bg-color-blueLight txt-color-white" data-description="Quem irá visitar a Polyafer?" data-icon="fa-calendar">VISITA <i class="fa fa-calendar txt-color-white"></i></span>
                                </li>
                                <li>
                                        <span class="bg-color-red txt-color-white" data-description="Interna? Externa?" data-icon="fa-calendar">AUDITORIA <i class="fa fa-calendar txt-color-white"></i></span>
                                </li>
                        </ul>
                </fieldset>
        </form>

</div>
<?php $jarvis->endJarvisWidget(); ?>

<?php $jarvis->beginJarvisWidget(
        'index-evento-02', 
        '<strong>Calendário</strong><i> Eventos</i>', 
        array(
            JvHtml::DATA_WIDGET_COLLAPSED=>'false',
            JvHtml::DATA_WIDGET_DELETEBUTTON=>'false',
            JvHtml::DATA_WIDGET_EDITBUTTON=>'false',
            JvHtml::DATA_WIDGET_COLORBUTTON=>'false',
            JvHtml::DATA_WIDGET_TOGGLEBUTTON=>'false'
        ),
        //options for body widget 'no-padding' in the class 
        array('class'=>''),
        //option for article widget
        array('class'=>'col-sm-12 col-md-12 col-lg-12')
); ?>
<?php
// Veja o arquivo de exemplo default.html em http://developer-paradize.blogspot.com.br/2013/06/jquery-fullcalendar-integration-with.html
// Lá, no meio do script jQuery, veja como implementou o exemplo de ajax para atualizar calendario.
?>
<div class="widget-body-toolbar">
<?php $this->renderPartial('_search', array('model'=>$model)); ?>
</div>
<div class="row">
<?php $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
    //'themeCssFile'=>'cupertino/jquery-ui.min.css',
    'id'=>'calendar1',
    'options'=>array(
        'header'=>array(
            'left'=>'prev,next,today',
            'center'=>'title',
            'right'=>'month,agendaWeek,agendaDay',
        ),
        'events'=>array(
            'url'=>$this->createUrl('/evento/calendarEvents'),
            'type'=>'POST',
            'data'=>new CJavaScriptExpression("function() {return { filter: $('#Evento_nome').val()};}")
        ),
        'drop'=>'js:function(date){

            // retrieve the dropped elements stored Event Object
            var originalEventObject = $(this).data(\'eventObject\');

            // we need to copy it, so that multiple events don\'t have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            
            // invoke ajax to save the event in database
            var newEvent = {
                nome: copiedEventObject.title,
                date_start: $.fullCalendar.moment(copiedEventObject.start).format("YYYY[-]MM[-]DD HH:mm:ss"),
                allday: copiedEventObject.allDay,
                descricao: copiedEventObject.description,
                icon: copiedEventObject.icon,
                color: copiedEventObject.className
            };
            var start = $.fullCalendar.moment(copiedEventObject.start).format("YYYY[-]MM[-]DD HH:mm:ss");
            $.ajax({
                url: "'. $this->createUrl('/evento/AddCalendarAjax') .'",
                data: {newEvent : newEvent},
                type: "POST",
                dataType: "json",
                success: function(html) {
                
                    $.extend(copiedEventObject, {eventId: html[1]});
                    //Insert the default class properties for event
                    copiedEventObject.className = copiedEventObject.className + " event fc-corner-left fc-corner-right fc-event-skin";
                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $(\'#calendar1\').fullCalendar(\'renderEvent\', copiedEventObject, false);

                    // is the "remove after drop" checkbox checked?
                    if ($(\'#drop-remove\').is(\':checked\')) {
                            // if so, remove the element from the "Draggable Events" list
                            $(this).remove();
                    }
                    
                    // Exibe o alerta
                    $.smallBox({
                            title : \'Alerta!\',
                            content : html[0],
                            color : \'#296191\',
                            iconSmall : \'fa fa-thumbs-up bounce animated\',
                            timeout : 4000
                    });
                },
            });
        }',
        'eventDrop'=>'js:function(event){
            var updateEvent = {
                id: event.eventId,
                nome: event.title,
                date_start: $.fullCalendar.moment(event.start).format("YYYY[-]MM[-]DD HH:mm:ss"),
                allday: event.allDay,
                descricao: event.description,
                icon: event.icon,
                color: event.color
            };
            if (event.hasOwnProperty("end")) {
                $.extend(updateEvent, {date_end: $.fullCalendar.moment(event.end).format("YYYY[-]MM[-]DD HH:mm:ss")});
            }
            $.ajax({
                url: "'. $this->createUrl('/evento/UpdateCalendarAjax') .'",
                data: { updateEvent : updateEvent },
                type: "POST",
                dataType: "json",
                success: function(html) {
                    $.smallBox({
                            title : \'Alerta!\',
                            content : html[0],
                            color : \'#296191\',
                            iconSmall : \'fa fa-thumbs-up bounce animated\',
                            timeout : 4000
                    });
                },
            });
        }',
        'eventResize'=>'js:function(event, delta, revertFunc, jsEvent, ui, view){
            var updateEvent = {
                id: event.eventId,
                nome: event.title,
                date_start: $.fullCalendar.moment(event.start).format("YYYY[-]MM[-]DD HH:mm:ss"),
                allday: event.allDay,
                descricao: event.description,
                icon: event.icon,
                color: event.color
            };
            if (event.hasOwnProperty("end")) {
                $.extend(updateEvent, {date_end: $.fullCalendar.moment(event.end).format("YYYY[-]MM[-]DD HH:mm:ss")});
            }
            $.ajax({
                url: "'. $this->createUrl('/evento/UpdateCalendarAjax') .'",
                data: { updateEvent : updateEvent },
                type: "POST",
                dataType: "json",
                success: function(html) {
                    $.smallBox({
                            title : \'Alerta!\',
                            content : html[0],
                            color : \'#296191\',
                            iconSmall : \'fa fa-thumbs-up bounce animated\',
                            timeout : 4000
                    });
                },
            });
        }',
        'eventRender'=>'js:function(event, element, icon){
            //var modal = document.createElement("a");
            //modal.innerHTML = "Editar";
            //modal.setAttribute("href","javascript:void(0);");
            //modal.setAttribute("data-target","#myModal");
            //modal.setAttribute("data-toggle","modal");
            if (!event.description == "") {
                element.find(\'.fc-event-title\').append("<br/><span class=\'ultra-light\'>" + event.description +
                    "</span>");
            }
            if (!event.icon == "") {
                element.find(\'.fc-event-title\').append("<i class=\'air air-top-right fa " + event.icon +
                    " \'></i>");
            }
            //element.find(\'.fc-event-title\').append(modal);
        }',
        'eventClick'=>'js:function(event, mouse, view){
            $.ajax({
                url: "'. $this->createUrl('/evento/UpdateModalCalendarAjax') .'/" + event.eventId,
                type: "POST",
                success: function(data) {
                    $(".modal-body").html(data);
                    $("#myModal").modal("show");
                },
            });
        }',
        'editable'=>true,
        'droppable'=>true,
        'timeFormat'=>'H:mm' // uppercase H for 24-hour clock
    )));
?>
</div>
<?php $jarvis->endJarvisWidget(); ?>
<?php $this->endWidget(); ?>

<?php 
Yii::app()->clientScript->registerScript('externalEvents',"
        var initDrag = function (e) {
        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end

        var eventObject = {
            title: $.trim(e.children().text()), // use the element's text as the event title
            description: $.trim(e.children('span').attr('data-description')),
            icon: $.trim(e.children('span').attr('data-icon')),
            className: $.trim(e.children('span').attr('class')) // use the element's children as the event class
        };
        // store the Event Object in the DOM element so we can get to it later
        e.data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        e.draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0 //  original position after the drag
        });
    };

    var addEvent = function (title, priority, description, icon) {
        title = title.length === 0 ? 'Sem Título' : title;
        description = description.length === 0 ? 'Sem descrição' : description;
        icon = icon.length === 0 ? ' ' : icon;
        priority = priority.length === 0 ? 'label label-default' : priority;

        var html = $('<li><span class=\"' + priority + '\" data-description=\"' + description + '\" data-icon=\"' +
            icon + '\">' + title + ' <i class=\"fa fa-calendar txt-color-white\"></i></span></li>').prependTo('ul#external-events').hide().fadeIn();

        $(\"#event-container\").effect(\"highlight\", 800);

        initDrag(html);
    };

    /* initialize the external events
         -----------------------------------------------------------------*/

    $('#external-events > li').each(function () {
        initDrag($(this));
    });

    $('#add-event').click(function () {
        var title = $('#title').val(),
            priority = 'bg-color-teal txt-color-white',
            description = $('#description').val(),
            icon = 'fa-calendar';

        addEvent(title, priority, description, icon);
    });
    
    /* filter events based on user input
         -----------------------------------------------------------------*/ 
    $('#filterEvents').click(function(event) {
        $('#calendar1').fullCalendar( 'refetchEvents' );
        event.preventDefault();
        return false;
    });
    
    ", CClientScript::POS_READY);
?>

<?php
$this->widget('bootstrap.widgets.BsModal', array(
    'id' => 'myModal',
    'header' => 'Editar Evento do Calendário',
    'content' => '',//$this->renderPartial('_form', array('model'=>$model)),
    'footer' => array(
        BsHtml::button('Salvar Alterações', array(
            'url'=>'#',
            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
            'onclick'=>'$("#add-event-form").submit()'
        )),
        BsHtml::button('Cancelar', array(
            'data-dismiss' => 'modal'
        ))
    )
));
?>