<?php
/* @var $this EventoController */
/* @var $model Evento */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'add-event-form',
    'enableAjaxValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true
    )
)); ?>

    <p class="help-block">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php //echo $form->textFieldControlGroup($model,'group_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>100)); ?>
    <?php echo $form->textAreaControlGroup($model,'descricao',array('rows'=>6)); ?>
    <?php //echo $form->textFieldControlGroup($model,'url',array('maxlength'=>255)); ?>
    <?php //echo $form->textFieldControlGroup($model,'icon',array('maxlength'=>45)); ?>
    <?php echo $form->checkBoxControlGroup($model,'allday'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo'); ?>
    <?php echo $form->textFieldControlGroup($model,'status'); ?>
    <?php //echo $form->textFieldControlGroup($model,'color',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'date_start'); ?>
    <?php echo $form->textFieldControlGroup($model,'date_end'); ?>
    <?php echo $form->dropDownListControlGroup($model, 'tblUsers', CHtml::listData(User::model()->findAll(),'id','username'), array('multiple'=>true) ); ?>
    <?php //echo $form->textFieldControlGroup($model,'order_id'); ?>
    <?php //print_r(CHtml::listData(User::model()->findAll(),'id','username')) ?>
    <?php //echo BsHtml::submitButton('Salvar', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
