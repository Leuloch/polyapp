<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Polyafer App',
        'theme'=>'smartadmin',
        'language'=>'pt_br',

	// preloading 'log' component
	'preload'=>array('log'),
    
        'aliases'=>array(
            'bootstrap'=>'application.modules.bootstrap',
            //'booster'=>  'ext.yiibooster'
        ),

	// autoloading model and component classes
	'import'=>array(
                // Giix extension modules
                'ext.giix-components.*',
                // Yii models and components
		'application.models.*',
		'application.components.*',
                // Module bootstrap
                'bootstrap.*',
                'bootstrap.components.*',
                'bootstrap.models.*',
                'bootstrap.controllers.*',
                'bootstrap.helpers.*',
                'bootstrap.widgets.*',
                'bootstrap.extensions.*',
                'bootstrap.behaviors.*',
                // Yiibooster
                //'booster.components.*',
                // My Helpers for JarvisWidget
                'application.helpers.JvHtml',
                // Extension for DataTables.js
                'ext.EDataTables.*',
                // USER module
                'application.modules.user.models.*',
                'application.modules.user.components.*',
                'ext.dynamictabularform.*',
                'application.vendor.phpexcel.PHPExcel.*',
                'ext.yiireport.*',
                'ext.yiiselect2.Select2',
                // Date Time Picker @see http://www.yiiframework.com/extension/juidatetimepicker/
	        //'ext.datetimepicker.EJuiDateTimePicker'
        ),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'gii',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                        'generatorPaths'=>array(
                            'bootstrap.gii',
                            'ext.giix-core'
                        ),
		),
            
                'bootstrap' => array(
                    'class'=>'bootstrap.BootStrapModule',
                ),
            
                'user' => array(
                    'tableUsers'=>'tbl_users' ,
                    'tableProfiles'=> 'tbl_profiles',
                    'tableProfileFields'=> 'tbl_profiles_fields',
                    # encrypting method (php hash function)
                    'hash' => 'md5',

                    # send activation email
                    'sendActivationMail' => false,

                    # allow access for non-activated users
                    'loginNotActiv' => false,

                    # activate user on registration (only sendActivationMail = false)
                    'activeAfterRegister' => true,

                    # automatically login from registration
                    'autoLogin' => true,

                    # registration path
                    'registrationUrl' => array('/user/registration'),

                    # recovery password path
                    'recoveryUrl' => array('/user/recovery'),

                    # login form path
                    'loginUrl' => array('/user/login'),

                    # page after login
                    'returnUrl' => array('/user/profile'),

                    # page after logout
                    'returnLogoutUrl' => array('/user/login'),
                ),
            
                'auth'=>array(
                        'strictMode' => true, // when enabled authorization items cannot be assigned children of the same type.
                        'userClass' => 'User', // the name of the user model class.
                        'userIdColumn' => 'id', // the name of the user id column.
                        'userNameColumn' => 'username', // the name of the user name column.
                        'defaultLayout' => 'webroot.themes.smartadmin.views.layouts.main', 
                        //'defaultLayout' => 'application.views.layouts.main', // the layout used by the module.
                        'viewDir' => null, // the path to view files to use with this module.
                ),
	),

	// application components
	'components'=>array(
//                'booster'=>array(
//                    'class'=>'booster.components.Booster',
//                    //'fontAwesomeCss'=>true,
//                    //'minify'=>true,
//                ),
                // Uso de skins para widget @see http://danaluther.blogspot.com.br/2012/02/leveraging-widgets-widget-factory-and.html
                // Mais informações também em @see http://www.yiiframework.com/wiki/107/theming-your-zii-widgets/
                'widgetFactory'=>array(
                    'class'=>'CWidgetFactory',
                    'widgets'=>array(
                        'EDataTables' => array(
                            'htmlOptions' => array(
                                'class' => '',
                            ),
                            'bootstrap' => true,
                            'itemsCssClass' => 'table table-striped table-bordered table-condensed items',
                            'pagerCssClass' => 'dataTables_paginate paging_bootstrap_full',
                            'buttons' => array(
                                'refresh' =>array(
					'label' => Yii::t('EDataTables.edt',"Refresh"),
					'text' => false,
					'htmlClass' => 'refreshButton',
					'icon' => 'glyphicon glyphicon-refresh',
					'callback' => null //default will be used, if possible
				),
                            ),
                            'datatableTemplate' => "<><'row'<'span3'l><'dataTables_toolbar'><'pull-right'f>r>t<'row'<'span3'i><'pull-right'p>>",
                            //'registerJUI' => false,
//                            'options' => array(
//                                'bJQueryUI' => false,
//                                'sPaginationType' => 'bootstrap',
//                                'fnDrawCallbackCustom' => "js:function(){\$('a[rel=tooltip]').tooltip(); \$('a[rel=popover]').popover();}",
//                            ),
                            //'cssFiles' => array('bootstrap.dataTables.css'),
//                            'jsFiles' => array(
//                               'bootstrap.dataTables.js',
//                               'jquery.fnSetFilteringDelay.js',
//                               'jdatatable.js' => CClientScript::POS_END,
//                            ),
                        ),
                    ),
                ),
                
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
                        'class'=>'auth.components.AuthWebUser',
                        //'class'=>'WebUser',
                        'loginUrl' => array('user/login'),
                        'admins'=>array('admin'), //users with full access
		),
            
                'authManager'=>array(
                        'class'=>'CDbAuthManager', //for yii-auth + database
                        'connectionID'=>'db', //for yii-auth + database
                        // A config. abaixo (behaviors) usa-se qdo se faz o download da versão yii-auth-1.6.0
                        'behaviors'=>array(
                                'auth'=>array(
                                        'class'=>'auth.components.AuthBehavior',
                                        //'admins'=>array('admin'), //users with full access
                                ),
                        ),
                ),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
            
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=polyafer_dev',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
                'bsHtml' => array('class'=>'bootstrap.components.BSHtml'),
                
                'cache'=>array(
                            'class'=>'system.caching.CFileCache',
                ),
            
                'settings'=>array(
                        'class'             => 'CmsSettings',
                        'cacheComponentId'  => 'cache',
                        'cacheId'           => 'user_settings',
                        'cacheTime'         => 84000,
                        'tableName'         => 'tbl_settings',
                        'dbComponentId'     => 'db',
                        'createTable'       => true,
                        'dbEngine'          => 'InnoDB',
                ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'leuloch@gmail.com',
	),
);