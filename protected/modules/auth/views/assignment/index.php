<?php
/* @var $this AssignmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('AuthModule.main', 'Assignments'),
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="well well-sm">
            <div class="well well-light well-sm no-margin no-padding">
                <div class="row">
                    <div class="col-sm-12">
                        
                            <?php
                            $this->widget('bootstrap.widgets.BsNav', array(
                                'type' => BsHtml::NAV_TYPE_TABS,
                                'items' => $this->menu,
                            ));
                            ?>
                        
                        <h1><?php echo Yii::t('AuthModule.main', 'Assignments'); ?></h1>

                        <?php
                        $this->widget('bootstrap.widgets.BsGridView', array(
                            'htmlOptions' => array('class' => 'col-sm-10'),
                            'type' => 'striped hover',
                            'dataProvider' => $dataProvider,
                            'emptyText' => Yii::t('AuthModule.main', 'No assignments found.'),
                            'template' => "{items}\n{pager}",
                            'columns' => array(
                                array(
                                    'header' => Yii::t('AuthModule.main', 'User'),
                                    'class' => 'AuthAssignmentNameColumn',
                                ),
                                array(
                                    'header' => Yii::t('AuthModule.main', 'Assigned items'),
                                    'class' => 'AuthAssignmentItemsColumn',
                                ),
                                array(
                                    'class' => 'AuthAssignmentViewColumn',
                                ),
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>