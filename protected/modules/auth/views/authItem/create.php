<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $form TbActiveForm */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)) => array('index'),
	Yii::t('AuthModule.main', 'New {type}', array('{type}' => $this->getTypeText())),
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="well well-sm">
            <div class="well well-light well-sm no-margin no-padding">
                <div class="row">
                    <div class="col-sm-12">
                       
                            <?php
                            $this->widget('bootstrap.widgets.BsNav', array(
                                'type' => BsHtml::NAV_TYPE_TABS,
                                'items' => $this->menu,
                            ));
                            ?>
                       
                        <div class="row">
                            <div class="col-sm-10">
                        <h1><?php echo Yii::t('AuthModule.main', 'New {type}', array('{type}' => $this->getTypeText())); ?></h1>

                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                            'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
                        ));
                        ?>

                            <?php echo $form->hiddenField($model, 'type'); ?>
                            <?php echo $form->textFieldControlGroup($model, 'name'); ?>
                            <?php echo $form->textFieldControlGroup($model, 'description'); ?>

                        <div class="form-actions">
                            <?php
                            echo BsHtml::submitButton(Yii::t('AuthModule.main', 'Create'), array(
                                'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                            ));
                            ?>
                        <?php
                        echo BsHtml::linkButton(Yii::t('AuthModule.main', 'Cancel'), array(
                            'type' => BsHtml::BUTTON_TYPE_LINK,
                            'url' => array('index'),
                        ));
                        ?>
                        </div>

                    <?php $this->endWidget(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>