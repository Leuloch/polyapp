<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $dataProvider AuthItemDataProvider */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)),
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="well well-sm">
            <div class="well well-light well-sm no-margin no-padding">
                <div class="row">
                    <div class="col-sm-12">
                        
                            <?php
                            $this->widget('bootstrap.widgets.BsNav', array(
                                'type' => BsHtml::NAV_TYPE_TABS,
                                'items' => $this->menu,
                                'htmlOptions' => array('class' => 'tabs-left'),
                            ));
                            ?>
                        
                        <h1><?php echo $this->capitalize($this->getTypeText(true)); ?></h1>
                        
                            <div class="col-sm-10">
                        <?php
                        echo BsHtml::linkButton(Yii::t('AuthModule.main', 'Add {type}', array('{type}' => $this->getTypeText())), array(
                            'color' => BsHtml::BUTTON_COLOR_PRIMARY,
                            'icon'=> BsHtml::GLYPHICON_PLUS,
                            'url' => array('create'),
                        ));
                        ?>
                            </div>
                        
                        <?php
                        $this->widget('bootstrap.widgets.BsGridView', array(
                            'htmlOptions' => array('class' => 'col-sm-10'),
                            'type' => 'striped hover',
                            'dataProvider' => $dataProvider,
                            'emptyText' => Yii::t('AuthModule.main', 'No {type} found.', array('{type}' => $this->getTypeText(true))),
                            'template' => "{items}\n{pager}",
                            'columns' => array(
                                array(
                                    'name' => 'name',
                                    'type' => 'raw',
                                    'header' => Yii::t('AuthModule.main', 'System name'),
                                    'htmlOptions' => array('class' => 'item-name-column'),
                                    'value' => "CHtml::link(\$data->name, array('view', 'name'=>\$data->name))",
                                ),
                                array(
                                    'name' => 'description',
                                    'header' => Yii::t('AuthModule.main', 'Description'),
                                    'htmlOptions' => array('class' => 'item-description-column'),
                                ),
                                array(
                                    'class' => 'bootstrap.widgets.BsButtonColumn',
                                    'viewButtonLabel' => Yii::t('AuthModule.main', 'View'),
                                    'viewButtonUrl' => "Yii::app()->controller->createUrl('view', array('name'=>\$data->name))",
                                    'updateButtonLabel' => Yii::t('AuthModule.main', 'Edit'),
                                    'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('name'=>\$data->name))",
                                    'deleteButtonLabel' => Yii::t('AuthModule.main', 'Delete'),
                                    'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('name'=>\$data->name))",
                                    'deleteConfirmation' => Yii::t('AuthModule.main', 'Are you sure you want to delete this item?'),
                                ),
                            ),
                        ));
                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>