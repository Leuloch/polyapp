<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $item CAuthItem */
/* @var $ancestorDp AuthItemDataProvider */
/* @var $descendantDp AuthItemDataProvider */
/* @var $formModel AddAuthItemForm */
/* @var $form TbActiveForm */
/* @var $childOptions array */

$this->breadcrumbs = array(
	$this->capitalize($this->getTypeText(true)) => array('index'),
	$item->description,
);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="well well-sm">
            <div class="well well-light well-sm no-margin no-padding">
                <div class="row">
                    <div class="col-sm-12">
                        
                            <?php
                            $this->widget('bootstrap.widgets.BsNav', array(
                                'type' => BsHtml::NAV_TYPE_TABS,
                                'items' => $this->menu,
                            ));
                            ?>

                            <div class="title-row clearfix">

                                <h1 class="pull-left">
                                    <?php echo CHtml::encode($item->description); ?>
                                    <small><?php echo $this->getTypeText(); ?></small>
                                </h1>

                                <?php
                                echo BsHtml::buttonGroup(array(
                                    array(
                                        'own'=>  BsHtml::linkButton(Yii::t('AuthModule.main', 'Edit'), array(
                                            'url' => array('update', 'name' => $item->name),
                                        )),
                                    ),
                                    array(
                                        'own'=> BsHtml::linkButton('',array(
                                            'icon' => 'trash',
                                            'color'=>  BsHtml::BUTTON_COLOR_DANGER,
                                            'url' => array('delete', 'name' => $item->name),
                                            'confirm' => Yii::t('AuthModule.main', 'Are you sure you want to delete this item?'),
                                        )),
                                    ),
                                        ), array('class' => 'pull-right'));
                                ?>

                            </div>

                            <?php
                            //$this->widget('bootstrap.widgets.TbDetailView', array(
                            $this->widget('zii.widgets.CDetailView', array(
                                'data' => $item,
                                'attributes' => array(
                                    array(
                                        'name' => 'name',
                                        'label' => Yii::t('AuthModule.main', 'System name'),
                                    ),
                                    array(
                                        'name' => 'description',
                                        'label' => Yii::t('AuthModule.main', 'Description'),
                                    ),
                                /*
                                  array(
                                  'name' => 'bizrule',
                                  'label' => Yii::t('AuthModule.main', 'Business rule'),
                                  ),
                                  array(
                                  'name' => 'data',
                                  'label' => Yii::t('AuthModule.main', 'Data'),
                                  ),
                                 */
                                ),
                            ));
                            ?>

                            <hr />

                            <div class="row">

                                <div class="col-sm-6">

                                    <h3>
                                    <?php echo Yii::t('AuthModule.main', 'Ancestors'); ?>
                                        <small><?php echo Yii::t('AuthModule.main', 'Permissions that inherit this item'); ?></small>
                                    </h3>

                                    <?php
                                    $this->widget('bootstrap.widgets.BsGridView', array(
                                        'type' => 'striped condensed hover',
                                        'dataProvider' => $ancestorDp,
                                        'emptyText' => Yii::t('AuthModule.main', 'This item does not have any ancestors.'),
                                        'template' => "{items}",
                                        'hideHeader' => true,
                                        'columns' => array(
                                            array(
                                                'class' => 'AuthItemDescriptionColumn',
                                                'itemName' => $item->name,
                                            ),
                                            array(
                                                'class' => 'AuthItemTypeColumn',
                                                'itemName' => $item->name,
                                            ),
                                            array(
                                                'class' => 'AuthItemRemoveColumn',
                                                'itemName' => $item->name,
                                            ),
                                        ),
                                    ));
                                    ?>

                                </div>

                                <div class="col-sm-6">

                                    <h3>
                                    <?php echo Yii::t('AuthModule.main', 'Descendants'); ?>
                                        <small><?php echo Yii::t('AuthModule.main', 'Permissions granted by this item'); ?></small>
                                    </h3>

                                    <?php
                                    $this->widget('bootstrap.widgets.BsGridView', array(
                                        'type' => 'striped condensed hover',
                                        'dataProvider' => $descendantDp,
                                        'emptyText' => Yii::t('AuthModule.main', 'This item does not have any descendants.'),
                                        'hideHeader' => true,
                                        'template' => "{items}",
                                        'columns' => array(
                                            array(
                                                'class' => 'AuthItemDescriptionColumn',
                                                'itemName' => $item->name,
                                            ),
                                            array(
                                                'class' => 'AuthItemTypeColumn',
                                                'itemName' => $item->name,
                                            ),
                                            array(
                                                'class' => 'AuthItemRemoveColumn',
                                                'itemName' => $item->name,
                                            ),
                                        ),
                                    ));
                                    ?>

                                </div>

                            </div>

                            <div class="row">

                                <div class="span6 offset6">

                                    <?php if (!empty($childOptions)): ?>

                                        <h4><?php echo Yii::t('AuthModule.main', 'Add child'); ?></h4>

                                        <?php
                                        $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
                                            'layout' => TbHtml::FORM_LAYOUT_INLINE,
                                        ));
                                        ?>

                                        <?php echo $form->dropDownListControlGroup($formModel, 'items', $childOptions, array('label' => false)); ?>

                                        <?php
                                        echo BsHtml::submitButton(Yii::t('AuthModule.main', 'Add'), array(
                                            'type' => BsHtml::BUTTON_COLOR_PRIMARY,
                                        ));
                                        ?>

                                        <?php $this->endWidget(); ?>

                                    <?php endif; ?>
                                </div>

                            </div>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>