<?php

class ProfileController extends Controller
{
	public $defaultAction = 'profile';
	public $layout='//layouts/column1';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	/**
	 * Shows a particular model.
	 */
	public function actionProfile()
	{
		$model = $this->loadUser();
	    $this->render('profile',array(
	    	'model'=>$model,
		'profile'=>$model->profile,
                'changePassword'=>new UserChangePassword,
                'preferences'=>$this->loadPreferences(),
	    ));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionEdit()
	{
		$model = $this->loadUser();
		$profile=$model->profile;
		
		// ajax validator
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo UActiveForm::validate(array($model,$profile));
			Yii::app()->end();
		}
		
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$profile->attributes=$_POST['Profile'];
			
			if($model->validate()&&$profile->validate()) {
				$model->save();
				$profile->save();
                Yii::app()->user->updateSession();
				Yii::app()->user->setFlash('profileMessage',UserModule::t("Changes is saved."));
				$this->redirect(array('/user/profile'));
			} else $profile->validate();
		}

		$this->render('profile',array(
			'model'=>$model,
			'profile'=>$profile,
                        'changePassword'=>new UserChangePassword,
                        'preferences'=>$this->loadPreferences(),
		));
	}
	
	/**
	 * Change password
	 */
	public function actionChangepassword() {
		$model = new UserChangePassword;
		if (Yii::app()->user->id) {
			
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='changepassword-form')
			{
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}
			
			if(isset($_POST['UserChangePassword'])) {
					$model->attributes=$_POST['UserChangePassword'];
					if($model->validate()) {
						$new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
						$new_password->password = UserModule::encrypting($model->password);
						$new_password->activkey=UserModule::encrypting(microtime().$model->password);
						$new_password->save();
						Yii::app()->user->setFlash('profileMessage',UserModule::t("New password is saved."));
						$this->redirect(array("profile"));
					}
			}
			$this->render('changepassword',array('model'=>$model));
	    }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=Yii::app()->controller->module->user();
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}
        
        
        private function loadPreferences()
        {
            
            $model = new Preferences;
            
            if (Yii::app()->user->id) {
                $settings = Yii::app()->settings->get(Yii::app()->user->username, array('fixed_header', 'fixed_ribbon', 'fixed_navigation', 'skin'));
                if ($settings['fixed_header'])
                    $model->fixed_header=1;
                if ($settings['fixed_ribbon'])
                    $model->fixed_ribbon=1;
                if ($settings['fixed_navigation'])
                    $model->fixed_navigation=1;
                if ($settings['skin'])
                    $model->skin = $settings['skin'];
            }
            
            return $model;
            
        }
        
        public function actionPreferences()
        {
            $model = $this->loadPreferences();
            
            if (Yii::app()->user->id) {
                // ajax validator
                if(isset($_POST['ajax']) && $_POST['ajax']==='preferences-form')
                {
                        echo UActiveForm::validate($model);
                        Yii::app()->end();
                }
                
                if(isset($_POST['Preferences'])) {
                                $model->attributes=$_POST['Preferences'];
                                if($model->validate()) {
                                        //Set settings using extension @see http://www.yiiframework.com/extension/settings/
                                        if ($model->fixed_header)
                                            Yii::app()->settings->set(Yii::app()->user->username, 'fixed_header', 'fixed-header', true);
                                        else
                                            Yii::app()->settings->set(Yii::app()->user->username, 'fixed_header', '', true);
                                        if ($model->fixed_ribbon)
                                            Yii::app()->settings->set(Yii::app()->user->username, 'fixed_ribbon', 'fixed-ribbon', true);
                                        else
                                            Yii::app()->settings->set(Yii::app()->user->username, 'fixed_ribbon', '', true);
                                        if ($model->fixed_navigation)
                                            Yii::app()->settings->set(Yii::app()->user->username, 'fixed_navigation', 'fixed-navigation', true);
                                        else
                                            Yii::app()->settings->set(Yii::app()->user->username, 'fixed_navigation', '', true);
                                        
                                        Yii::app()->settings->set(Yii::app()->user->username, 'skin', $model->skin, true);
                                        
                                        Yii::app()->user->setFlash('profileMessage','Alterações no Layout salvas com sucesso.');
                                        $this->redirect(array("profile"));
                                }
                }
            }
        }
}