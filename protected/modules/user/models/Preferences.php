<?php

class Preferences extends CFormModel {
    
    const SKIN_SMART_DEFAULT = 'smart-style-0';
    const SKIN_DARK_ELEGANCE = 'smart-style-1';
    const SKIN_ULTRA_LIGHT = 'smart-style-2';
    const SKIN_GOOGLE_SKIN = 'smart-style-3';
    
    public $fixed_header;
    public $fixed_ribbon;
    public $fixed_navigation;
    public $skin;
    
    public function rules() {
        return array(
            array('skin', 'required'),
            array('fixed_header, fixed_ribbon, fixed_navigation', 'in', 'range'=>array(0,1)),
            array('skin', 'in', 'range'=>array(self::SKIN_SMART_DEFAULT, self::SKIN_DARK_ELEGANCE, self::SKIN_ULTRA_LIGHT, self::SKIN_GOOGLE_SKIN))
        );
    }
    
    public function attributeLabels() {
        return array(
            'fixed_header'=>'Cabeçalho Fixo',
            'fixed_ribbon'=>'Ribbon Fixo',
            'fixed_navigation' => 'Menu Lateral Fixo',
            'skin'=>'Cor do Layout'
        );
    }
    
    public function getSkinOptions() {
        return array(
            self::SKIN_SMART_DEFAULT => 'Padrão',
            self::SKIN_DARK_ELEGANCE => 'Escuro',
            self::SKIN_ULTRA_LIGHT => 'Claro',
            self::SKIN_GOOGLE_SKIN => 'Orange'
        );
    } 
}

