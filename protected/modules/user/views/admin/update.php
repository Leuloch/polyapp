<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('admin'),
	$model->username=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create'), 'icon' => 'icon-plus-sign'),
    array('label'=>UserModule::t('View User'), 'url'=>array('view','id'=>$model->id), 'icon' => 'icon-eye-open'),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'icon-user'),
    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'), 'icon' => 'icon-edit'),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user'), 'icon' => 'icon-th-list'),
);
$this->layout='//layouts/column2';

?>

<h1><?php echo  UserModule::t('Update User')." ".$model->id; ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>