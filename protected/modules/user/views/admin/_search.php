<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'layout'=>  TbHtml::FORM_LAYOUT_VERTICAL,
        'htmlOptions'=>array(
                'class'=>'text-center',
        ),
));?>
<div class="wide form">
<fieldset>
    <?php echo $form->textFieldControlGroup($model, 'id'); ?>
    
    <?php echo $form->textFieldControlGroup($model, 'username'); ?>
    
    <?php echo $form->textFieldControlGroup($model, 'email'); ?>
    
    <?php echo $form->textFieldControlGroup($model, 'activkey'); ?>
    
    <?php echo $form->textFieldControlGroup($model, 'create_at'); ?>
    
    <?php echo $form->textFieldControlGroup($model, 'lastvisit_at'); ?>
    
    <?php echo $form->dropDownListControlGroup($model, 'superuser', $model->itemAlias('AdminStatus')) ?>
    
    <?php echo $form->dropDownListControlGroup($model, 'status', $model->itemAlias('UserStatus')) ?>
</fieldset>
    <?php echo BsHtml::formActions(array(
        TbHtml::submitButton(UserModule::t('Search'), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)),      
));?>

<?php $this->endWidget(); ?>

</div><!-- search-form -->