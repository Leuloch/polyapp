<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	UserModule::t('Create'),
);

$this->menu=array(
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'icon-user'),
    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'), 'icon' => 'icon-edit'),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user'), 'icon' => 'icon-th-list'),
);
$this->layout='//layouts/column2';

?>
<h1><?php echo UserModule::t("Create User"); ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>