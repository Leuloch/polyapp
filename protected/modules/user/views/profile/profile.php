<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
	UserModule::t("Profile"),
); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="well well-sm">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="well well-light well-sm no-margin no-padding">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="myCarousel" class="carousel fade profile-carousel">
                                    <div class="air air-bottom-right padding-10">
                                            <a href="javascript:void(0);" class="btn txt-color-white bg-color-blue btn-lg">
                                                <i class="fa fa-edit"></i> Editar</a>
                                    </div>
                                    <div class="air air-top-left padding-10">
                                        <h4 class="txt-color-white font-md">Membro desde <?php echo strtoupper(Yii::app()->dateFormatter->format('MMM d, yyyy',$model->create_at)); ?></h4>
                                    </div>
                                    <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                                            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                                    </ol>
                                    <div class="carousel-inner">
                                            <!-- Slide 1 -->
                                            <div class="item active">
                                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/demo/s1.jpg" alt="">
                                            </div>
                                            <!-- Slide 2 -->
                                            <div class="item">
                                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/demo/s2.jpg" alt="">
                                            </div>
                                            <!-- Slide 3 -->
                                            <div class="item">
                                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/demo/m3.jpg" alt="">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-3 profile-pic">
                                        <?php $this->widget('ext.yiiuserimg.YiiUserImg', array(
                                            'htmlOptions'=>array(
                                                'style'=> 'width: 120px;'
                                            ),
                                        )); ?>
                                        <div class="padding-10">
                                            <h4 class="font-md"><strong>1,543</strong>
                                            <br>
                                            <small>Followers</small></h4>
                                            <br>
                                            <h4 class="font-md"><strong>419</strong>
                                            <br>
                                            <small>Connections</small></h4>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <h1><?php echo $model->profile->firstname; ?> <span class="semi-bold"><?php echo $model->profile->lastname; ?></span>
                                        <br>
                                        <small> Administrator, Polyafer</small></h1>
                                        <ul class="list-unstyled">
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-phone"></i>&nbsp;&nbsp;(<span class="txt-color-darken">313</span>) <span class="txt-color-darken">464</span> - <span class="txt-color-darken">6473</span>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:<?php echo $model->email; ?>"><?php echo $model->email; ?></a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-skype"></i>&nbsp;&nbsp;<span class="txt-color-darken">john12</span>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="text-muted">
                                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span class="txt-color-darken">Free after <a href="javascript:void(0);" rel="tooltip" title="" data-placement="top" data-original-title="Create an Appointment">4:30 PM</a></span>
                                                </p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr>
                                <div class="padding-10">
                                    <?php $this->widget('zii.widgets.jui.CJuiTabs', array(
                                        'tabs' => array(
                                            'StaticTab' => 'Content for tab 1',
                                            'StaticTab With ID' => array(
                                                'content'=>'Content for tab 2 With Id',
                                                'id'=>'tab2',
                                            ),
                                            'Render Partial'=>array(
                                                'id'=>'test-id',
                                                'content'=>$this->renderPartial('edit', array(
                                                    'model'=>$model,
                                                    'profile'=>$profile,
                                                ), true),
                                            ),
                                            'AjaxTab'=>array(
                                                'ajax'=>$this->createUrl('ajax')
                                            ),
                                        ),
                                        'options'=>array(
                                            'collapsible'=>true,
                                            'class'=>'nav-tabs'
                                        ),
                                        'id'=>'MyTab-Menu',
                                    )); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
