<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Restore");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Restore"),
);
?>

<h1><?php echo UserModule::t("Restore"); ?></h1>

<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
<div class="success">
<?php Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, Yii::app()->user->getFlash('recoveryMessage')); ?>
</div>
<?php else: ?>

<div class="form">
<?php echo CHtml::beginForm(); ?>

	<?php echo CHtml::errorSummary($form); ?>
	
	<div class="control-group">
		<?php echo CHtml::activeLabel($form,'login_or_email'); ?>
            <div class="controls">
		<?php echo CHtml::activeTextField($form,'login_or_email') ?>
		<p class="hint"><?php echo UserModule::t("Please enter your login or email addres."); ?></p>
            </div>
	</div>
	
        <?php echo BsHtml::formActions(array(
                BsHtml::submitButton(UserModule::t("Restore"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)),      
        ));?>    

<?php echo CHtml::endForm(); ?>
</div><!-- form -->
<?php endif; ?>