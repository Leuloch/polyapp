<?php
$this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login");
?>


<div class="well no-padding">
    <?php $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id'=>'login-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array('class'=>'smart-form client-form')
    )); ?>
    
    <header>
        <?php echo UserModule::t("Login"); ?>
    </header>
    <fieldset>
        <?php //echo $form->errorSummary($model); ?>
        <section>
            <?php echo $form->textFieldControlGroup($model, 'username', array(
                'append'=>  BsHtml::icon(BsHtml::GLYPHICON_USER),
                'maxlength'=>32,
            )); ?>
        </section>
        <section>
            <?php echo $form->passwordFieldControlGroup($model, 'password', array(
                'append'=>  BsHtml::icon(BsHtml::GLYPHICON_LOCK),
                'maxlength'=>32,
            )); ?>
            <div class="note">
                <?php echo BsHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl); ?>
            </div>
        </section>
        <section>
            <?php echo $form->checkBoxControlGroup($model, 'rememberMe', array(
                'class'=>'checkbox',
            )); ?>
        </section>
    </fieldset>
    <footer>
        <?php echo BsHtml::submitButton(UserModule::t("Login"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
    </footer>
</div>
<?php $this->endWidget(); ?>