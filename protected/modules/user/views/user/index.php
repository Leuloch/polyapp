<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
if(UserModule::isAdmin()) {
	$this->layout='//layouts/column2';
	$this->menu=array(
	    array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'), 'icon' => 'icon-user'),
	    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin'), 'icon' => 'icon-edit'),
	);
        $this->title=UserModule::t("List User");
}
?>

<h1><?php echo UserModule::t("List User"); ?></h1>

<!-- Meu grid view -->
<?php $this->widget('bootstrap.widgets.BsGridView', array(
        'type' => 'striped hover',
        'dataProvider'=>$dataProvider,
        'columns'=>array(
                array(
                        'name'=>'username',
                        'type'=>'raw', //esta linha é responsável por exibir item como um link!
                        'htmlOptions' => array('class'=>'item-name-column'),
                        'value'=> "CHtml::link(CHtml::encode(\$data->username), array('user/view','id'=>\$data->id))",                       
                ),
                'create_at',
                'lastvisit_at',
        ),
)); ?>
<!-- Meu grid view -->