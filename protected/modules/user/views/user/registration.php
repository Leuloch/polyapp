<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
	UserModule::t("Registration"),
);
?>
<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'layout'=>  BsHtml::FORM_LAYOUT_VERTICAL,
        'htmlOptions'=>array(
                'class'=>'text-center',
        //configurações originais do form
                'enctype'=>'multipart/form-data',
        ),
        'id'=>'registration-form',
	'enableAjaxValidation'=>true,	
	'clientOptions'=>array(
		'validateOnSubmit'=>true,	
        ),
));?>
<fieldset>
<legend>
    <h1><?php echo UserModule::t("Registration"); ?></h1>
</legend>
<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>

	<p class="alert-info"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	
        <?php echo $form->textFieldControlGroup($model, 'username'); ?>	
	
        <?php echo $form->passwordFieldControlGroup($model, 'password', array(
            'help'=>'Minimal password length 4 symbols.',
        )); ?>
	
        <?php echo $form->passwordFieldControlGroup($model, 'verifyPassword'); ?>        
	
        <?php echo $form->textFieldControlGroup($model, 'email'); ?>        	
	
<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>			
		<?php 
		if ($widgetEdit = $field->widgetEdit($profile)) {
			echo $widgetEdit;
		} elseif ($field->range) {
			echo $form->dropDownListControlGroup($profile,$field->varname,Profile::range($field->range));
		} elseif ($field->field_type=="TEXT") {
			echo $form->textAreaControlGroup($profile,$field->varname);
		} else {
			echo $form->textFieldControlGroup($profile,$field->varname);
		}
		 ?>				
			<?php
			}
		}
?>
	<?php if (UserModule::doCaptcha('registration')): ?>		
		
		<?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textFieldControlGroup($model, 'verifyCode',array(
                    'help'=>UserModule::t("Please enter the letters as they are shown in the image above.").'<br/>'.
                        UserModule::t("Letters are not case-sensitive."),
                )); ?>						
	<?php endif; ?>
    </fieldset>

    <?php echo BsHtml::formActions(array(
            BsHtml::submitButton(UserModule::t("Register"), array('color' => BsHtml::BUTTON_COLOR_PRIMARY)),      
    ));?>	

<?php $this->endWidget(); ?>

<?php endif; ?>