<?php

class OrderController extends GxController {

    
public $layout='//layouts/column1';

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow',
				'actions'=>array('index','view'),
				'users'=>array('*'),
				),
			array('allow', 
				'actions'=>array('minicreate', 'create','update', 'getRowForm'),
				'users'=>array('@'),
				),
			array('allow', 
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}
    
        public function actions() 
        {
            return array(
                'getRowForm' => array(
                    'class' => 'ext.dynamictabularform.actions.GetRowForm',
                    'view' => '_orderItemsForm',
                    'modelClass' => 'OrderItems'
                ),
            );
        }
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Order'),
		));
	}

	public function actionCreate() {
		$model = new Order;

		$this->performAjaxValidation($model, 'order-form');

		if (isset($_POST['Order'])) {
			$model->setAttributes($_POST['Order']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) 
        {
		$model = $this->loadModel($id, 'Order');
                
                $orderItems = $this->loadOrderItems($model);
                
                $defaultTab = null;   

		//$this->performAjaxValidation($model, 'order-form');

		if (isset($_POST['Order'])) {
                    $model->setAttributes($_POST['Order']);
                        
                    if (isset($_POST['OrderItems'])) {
                            $orderItems = array();
                            foreach ($_POST['OrderItems'] as $key => $value) {
                                /**
                                 * here we will take advantage of the updateType attribute so
                                 * that we will be able to determine what we want to do 
                                 * to a specific row
                                 */
                                if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_CREATE)
                                    $orderItems[$key] = new OrderItems();

                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_UPDATE)
                                    $orderItems[$key] = OrderItems::model()->findByPk($value['id']);

                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_DELETE) {
                                    $delete = OrderItems::model()->findByPk($value['id']);
                                    if ($delete->delete()) {
                                        unset($orderItems[$key]);
                                        continue;
                                    }
                                }
                                $orderItems[$key]->attributes = $value;
                        }
                        
			$valid = $model->validate();
                        foreach ($orderItems as $orderItem) {
                            $valid = $orderItem->validate() & $valid;
                        }
                        if (!$valid) {
                            $defaultTab = 'orderItems';
                            Yii::app()->user->setFlash('warning', 'Favor corrigir os campos assinalados e tente salvar novamente.');
                        } else {
                            $transaction = $model->getDbConnection()->beginTransaction();
                            try {
                                $model->save();
                                $model->refresh();

                                foreach ($orderItems as $orderItem) {
                                    $orderItem->ordem_id = $model->id;
                                    $orderItem->save();
                                }
                                $transaction->commit();
                            } catch (Exception $e) {
                                $transaction->rollback();
                            }
                            Yii::app()->user->setFlash('success', 'Os dados foram editados com sucesso.');
                            $this->redirect(array('update', 'id' => $model->id));
                        }
                    }
                }
                $this->render('update',array(
			'model'=>$model,
                        'orderItems'=>$orderItems,
                        'defaultTab'=>$defaultTab,
		));
                
	}
        
        private function loadOrderItems($model)
        {
            $orderItems = $model->orderItems;
            if (!$orderItems)
                $orderItems[0] = new OrderItems;
            return $orderItems;
        }
        
	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Order')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Order');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Order('search');
		$model->unsetAttributes();

		if (isset($_GET['Order']))
			$model->setAttributes($_GET['Order']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}