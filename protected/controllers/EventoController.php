<?php

class EventoController extends GxController
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'calendarEvents', 'updateCalendarAjax', 'addCalendarAjax', 'updateModalCalendarAjax'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id, 'Evento'),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate() 
        {
            $model = new Evento;


            if (isset($_POST['Evento'])) {
                $model->setAttributes($_POST['Evento']);
                $relatedData = array(
                    'tblUsers' => $_POST['Evento']['tblUsers'] === '' ? null : $_POST['Evento']['tblUsers'],
                    );

                if ($model->saveWithRelated($relatedData)) {
                    if (Yii::app()->getRequest()->getIsAjaxRequest())
                        Yii::app()->end();
                    else
                        $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('create', array( 'model' => $model));
        }

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id) 
        {
            $model = $this->loadModel($id, 'Evento');


            if (isset($_POST['Evento'])) {
                $model->setAttributes($_POST['Evento']);
                $relatedData = array(
                    'tblUsers' => $_POST['Evento']['tblUsers'] === '' ? null : $_POST['Evento']['tblUsers'],
                    );

                if ($model->saveWithRelated($relatedData)) {
                    $this->redirect(array('view', 'id' => $model->id));
                }
            }

            $this->render('update', array(
                    'model' => $model,
                    ));
        }


	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id) 
        {
            if (Yii::app()->getRequest()->getIsPostRequest()) {
                $this->loadModel($id, 'Evento')->delete();

                if (!Yii::app()->getRequest()->getIsAjaxRequest())
                    $this->redirect(array('admin'));
            } else
                throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
        }


	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
                $model=new Evento;
                
                $this->performAjaxValidation($model);
                
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Evento('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Evento']))
			$model->attributes=$_GET['Evento'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionCalendarEvents() 
        {
            $items = array();
            if (isset($_POST['filter'])){
                $criteria = new CDbCriteria();
                $criteria->compare('nome', $_POST['filter'], true);
                $criteria->compare('tipo', '');
                $criteria->compare('status', '');
                $criteria->addBetweenCondition('date_start', $_POST['start'], $_POST['end'] );
                $events = Evento::model()->findAll($criteria);
            } else {
                $events = Evento::model()->findAll();
            }
            $events = Evento::model()->findAll($criteria);
            $i = 0;
            foreach ($events as $event) {
                if ($event->group_id)
                    $items[$i]['id'] = $event->group_id;
                $items[$i]['eventId'] = $event->id;
                if ($event->order_id)
                    $items[$i]['order_id'] = $event->order_id;
                $items[$i]['title'] = $event->nome;
                if ($event->allday) {
                    $items[$i]['start'] = date('Y-m-d',strtotime($event->date_start));
                    if ($event->date_end)
                        $items[$i]['end'] = date('Y-m-d',strtotime($event->date_end));
                    $items[$i]['allDay'] = true;
                } else {
                    $items[$i]['start'] = date('Y-m-d\TH:i:s',strtotime($event->date_start));
                    if ($event->date_end)
                        $items[$i]['end'] = date('Y-m-d\TH:i:s',strtotime($event->date_end));
                }
                $items[$i]['className'] = array('event', 'fc-corner-left', 'fc-corner-right', 'fc-event-skin', (string) $event->color);
                $items[$i]['description'] = $event->descricao;
                if ($event->icon)
                    $items[$i]['icon'] = $event->icon;
                if ($event->color)
                    $items[$i]['color'] = $event->color;
                else
                    $items[$i]['color'] = 'bg-color-green';
                if ($event->url)
                    $items[$i]['url'] = $event->url;
                $i++;
            }
            echo CJSON::encode($items);
            Yii::app()->end();
        }
        
        public function actionUpdateCalendarAjax()
        {   
            if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
                throw new CHttpException('403', 'Acesso negado.');
            }
            if(isset($_POST['updateEvent']))
            {
                    $id = (int) $_POST['updateEvent']['id'];
                    $allday = $_POST['updateEvent']['allday'];
                    $model=$this->loadModel($id, 'Evento');

                    $model->attributes=$_POST['updateEvent'];
                    
                    $model->allday= (boolean) $allday;
                    if ($allday == 'false') {
                        $model->allday=0;
                        if (!$model->date_end or date_parse($model->date_end)['error_count']>0)
                            $model->date_end = date('Y-m-d H:i:s', strtotime('+1 hour', strtotime($model->date_start)));
                    }
                    $retorno = array();
                    if($model->save()){
                        $retorno[]='Evento ' . $model->nome . ' atualizado com sucesso!';
                        $retorno[]= $model->id;
                    } else {
                        $retorno[]='Ops! Ocorreu um erro ao salvar o evento';
                        $retorno[]=null;
                    }
                    echo CJSON::encode($retorno);
            }
            
            Yii::app()->end();
        }
        
        public function actionUpdateModalCalendarAjax($id)
        {
            if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
                throw new CHttpException('403', 'Acesso negado.');
            }
            $model=$this->loadModel($id, 'Evento');

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Evento']))
            {
                    $model->attributes=$_POST['Evento'];
                    $relatedData = array(
                        'tblUsers' => !isset($_POST['Evento']['tblUsers']) ? null : $_POST['Evento']['tblUsers'],
                        );

                    if($model->saveWithRelated($relatedData)) {
                            Yii::app()->user->setFlash('eventEditSuccess','Evento salvo com sucesso!');
                            $this->redirect(array('index'));
                    } else {
                            Yii::app()->user->setFlash('eventEditError','Ops, ocorreu um erro ao salvar');
                            $this->redirect(array('index')); 
                    }
            }

            echo $this->renderPartial('_form',array(
                        'model'=>$model,
                ));
            
            Yii::app()->end();
            
        }
        
        public function actionAddCalendarAjax()
        {
            if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
                throw new CHttpException('403', 'Acesso negado.');
            }
            if(isset($_POST['newEvent']))
            {
                    $model=new Evento;
                    
                    // Values received via ajax
                    $model->attributes=$_POST['newEvent'];
                    
                    // Resolve o problema de allday para novos eventos
                    if (date('H', strtotime($model->date_start)) != '00' )
                            $model->allday = 0;

                    $retorno = array();
                    if($model->save()){
                        $retorno[]='Evento ' . $model->nome . ' adicionado com sucesso!';
                        $retorno[]= $model->id;
                    } else {
                        $retorno[]='Ops! Ocorreu um erro ao salvar o evento';
                        $retorno[]=null;
                    }
                    echo CJSON::encode($retorno);
            }
            
            Yii::app()->end();
        }

	/**
	* Performs the AJAX validation.
	* @param Evento $model the model to be validated
	*/
	protected function performAjaxValidation($model, $form = NULL)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='add-event-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}