<?php

class InstrumentoController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'getRowForm'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actions()
        {
            return array(
                'getRowForm'=>array(
                    'class'=>'ext.dynamictabularform.actions.GetRowForm',
                    'view'=>'sistemaMedicao/_rowSistemaMedicao',
                    'modelClass'=>'SistemaMedicao'
                )
            );
        }
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Instrumento;
                $sistemaMedicoes = array(new SistemaMedicao);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $sistemaMedicoes);
                
		if(isset($_POST['Instrumento']))
		{
			$model->attributes=$_POST['Instrumento'];
                        if (isset($_POST['SistemaMedicao'])) {
                            $sistemaMedicoes = array();
                            foreach ($_POST['SistemaMedicao'] as $key => $value) {
                                $sistemaMedicao = new SistemaMedicao('batchSave');
                                $sistemaMedicao->attributes = $value;
                                $sistemaMedicoes[] = $sistemaMedicao; 
                            }
                        }
                        
                        $valid = $model->validate();
                        foreach ($sistemaMedicoes as $sistemaMedicao) {
                            $valid = $sistemaMedicao->validate() & $valid;
                        }
                        
			if($valid) {
                                $transaction = $model->getDbConnection()->beginTransaction();
                                try {
                                    $model->save();
                                    $model->refresh();
                                    
                                    foreach ($sistemaMedicoes as $sistemaMedicao) {
                                        $sistemaMedicao->instrumento_id = $model->id;
                                        $sistemaMedicao->save();
                                    }
                                    $transaction->commit();
                                } catch (Exception $e) {
                                    $transaction->rollback();
                                }
                                
				$this->redirect(array('view','id'=>$model->id));
                        }
		}

		$this->render('create',array(
                    'model'=>$model,
                    'sistemaMedicoes'=>$sistemaMedicoes,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $sistemaMedicoes = $model->sistemaMedicoes;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model, $sistemaMedicoes);

		if(isset($_POST['Instrumento']))
		{
			$model->attributes=$_POST['Instrumento'];
                        
                        if (isset($_POST['SistemaMedicao'])) {
                            $sistemaMedicoes = array();
                            foreach ($_POST['SistemaMedicao'] as $key => $value){
                                if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_CREATE)
                                    $sistemaMedicoes[$key] = new SistemaMedicao();
                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_UPDATE)
                                    $sistemaMedicoes[$key] = SistemaMedicao::model()->findByPk($value['id']);
                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_DELETE) {
                                    $delete = SistemaMedicao::model()->findByPk($value['id']);
                                    if ($delete->delete()) {
                                        unset($sistemaMedicoes[$key]);
                                        continue;
                                    }
                                }
                                $sistemaMedicoes[$key]->attributes = $value;
                            }
                        }
                        
                        $valid = $model->validate();
                        foreach ($sistemaMedicoes as $sistemaMedicao) {
                            $valid = $sistemaMedicao->validate() & $valid;
                        }
                        
			if($valid) {
                                $transaction = $model->getDbConnection()->beginTransaction();
                                try {
                                    $model->save();
                                    $model->refresh();
                                    
                                    foreach ($sistemaMedicoes as $sistemaMedicao) {
                                        $sistemaMedicao->instrumento_id = $model->id;
                                        $sistemaMedicao->save();
                                    }
                                    $transaction->commit();
                                } catch (Exception $e) {
                                    $transaction->rollback();
                                }

                                $this->redirect(array('view','id'=>$model->id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
                        'sistemaMedicoes' => $sistemaMedicoes
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Instrumento');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Instrumento('search');
                
                // set the columns to those posted
                if (!empty($_POST['columns'])) {
                    // name the cookies after the model or something unique so that other gridviews don't overwrite this one
                    unset(Yii::app()->request->cookies['instrumento-columns']);  // first unset cookie for columns
                    Yii::app()->request->cookies['instrumento-columns'] = new CHttpCookie('instrumento-columns', serialize($_POST['columns']));  // define cookie for columns
                    $columns = $_POST['columns'];
                } elseif (empty(Yii::app()->request->cookies['instrumento-columns'])) {
                    // if no columns are selected, set the default columns
                    $columns = array(
                        'categoria_instrumento_id',
                        'nome',
                        'fabricante_id',
                        'modelo',
                        'tag'
                    );
                } else {
                    $columns = unserialize(Yii::app()->request->cookies['instrumento-columns']);
                }
                
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Instrumento']))
			$model->attributes=$_GET['Instrumento'];
                
                $pageSize=Yii::app()->user->getState('pageSizeInstrumento', Yii::app()->params['defaultPageSize']);
		if(isset($_GET['pageSizeInstrumento']))
                {
                    Yii::app()->user->setState('pageSizeInstrumento', (int)$_GET['pageSizeInstrumento']);
                    unset($_GET['pageSizeInstrumento']);
                }
		$this->render('admin',array(
			'model'=>$model,
                        'columns'=>$columns,
                        'pageSize' => $pageSize,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Instrumento the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=Instrumento::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param Instrumento $model the model to be validated
        * @see http://www.yiiframework.com/forum/index.php/topic/21125-ajax-validation-for-tabular-or-dynamic-input/page__view__findpost__p__203398
	*/
	protected function performAjaxValidation($model, $tabularModel=array())
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='instrumento-form')
		{
			$modelResult = CJSON::decode(CActiveForm::validate($model));
                        $tabularModelResult = CJSON::decode(CActiveForm::validateTabular($tabularModel));
                        echo CJSON::encode(CMap::mergeArray($modelResult, $tabularModelResult));
			Yii::app()->end();
		}
	}        
}