<?php

class OrderItemsController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'OrderItems'),
		));
	}

	public function actionCreate() {
		$model = new OrderItems;


		if (isset($_POST['OrderItems'])) {
			$model->setAttributes($_POST['OrderItems']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'OrderItems');


		if (isset($_POST['OrderItems'])) {
			$model->setAttributes($_POST['OrderItems']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'OrderItems')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('OrderItems');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new OrderItems('search');
		$model->unsetAttributes();

		if (isset($_GET['OrderItems']))
			$model->setAttributes($_GET['OrderItems']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}