<?php

class EmpresaController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'admin','loadEnderecoByAjax', 'getRowForm', 'exportExcel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function actions() 
        {
            return array(
                'getRowForm' => array(
                    'class' => 'ext.dynamictabularform.actions.GetRowForm',
                    'view' => '_enderecoForm',
                    'modelClass' => 'Endereco'
                ),
            );
        }
        
        public function actionExportExcel()
        {
            //Query the Data
            $clientes = Yii::app()->db->createCommand('SELECT * FROM tbl_empresa')->queryAll();

            $r = new YiiReport(array('template'=> 'empresas.xlsx'));

            $r->load(array(
                    array(
                        'id'=>'empresa',
                        'repeat'=>true,
                        'data'=>$clientes,
                        'minRows'=>1
                    )
                )
            );
            echo $r->render('excel2007', 'Empresas');
        }
        
	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Empresa;
                $endereco = new Endereco;
                $configCertificado = new ConfigCertificado;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation(array($model, $endereco));
                
		if(isset($_POST['Empresa']) && isset($_POST['Endereco']))
		{
			$model->attributes=$_POST['Empresa'];
                        $endereco->attributes = $_POST['Endereco'];
			if($model->validate() && $endereco->validate())
                        {
                                    if ($model->save()) {
                                        $endereco->empresa_id = $model->id;
                                        $endereco->save();
                                        $configCertificado->empresa_id = $model->id;
                                        $configCertificado->nome_certificado = (!empty($model->razao_social)) ? $model->razao_social : $model->nome;
                                        $configCertificado->endereco = $endereco->endereco;
                                        $configCertificado->numero = $endereco->numero;
                                        $configCertificado->cidade = $endereco->cidade;
                                        $configCertificado->estado = $endereco->estado;
                                        $configCertificado->cep = $endereco->cep;
                                        $configCertificado->save();
                                    }
                                    Yii::app()->user->setFlash('inserted', 'Empresa Adicionada com sucesso.');
                                    $this->redirect(array('update','id'=>$model->id));
                        }
		}
                
		$this->render('create',array(
                    'model'=>$model,
                    'endereco'=>$endereco,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{       
		$model=$this->loadModel($id);
                
                $enderecos = $this->loadEndereco($model);
                
                $configCertificado = (isset($model->configCertificados[0]))?$model->configCertificados[0]:new ConfigCertificado;
                
                $defaultTab=null;
                
                // Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
                $this->performTabularAjaxValidation($enderecos);

		if(isset($_POST['Empresa']))
		{
			$model->attributes=$_POST['Empresa'];
                        
                        if (isset($_POST['Endereco'])) {
                            $enderecos = array();
                            foreach ($_POST['Endereco'] as $key => $value) {
                                /**
                                 * here we will take advantage of the updateType attribute so
                                 * that we will be able to determine what we want to do 
                                 * to a specific row
                                 */
                                if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_CREATE)
                                    $enderecos[$key] = new Endereco();

                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_UPDATE)
                                    $enderecos[$key] = Endereco::model()->findByPk($value['id']);

                                else if ($value['updateType'] == DynamicTabularForm::UPDATE_TYPE_DELETE) {
                                    $delete = Endereco::model()->findByPk($value['id']);
                                    if ($delete->delete()) {
                                        unset($enderecos[$key]);
                                        continue;
                                    }
                                }
                                $enderecos[$key]->attributes = $value;
                            }
                        }
                        
                        if (isset($_POST['ConfigCertificado'])) {
                            $configCertificado->attributes = $_POST['ConfigCertificado'];
                            $configCertificado->empresa_id = $model->id;
                        }
                        
                        $valid = $model->validate();
                        foreach ($enderecos as $endereco) {
                            $valid = $endereco->validate() & $valid;
                        }
                        if (!$valid)
                            $defaultTab = 'endereco';
                        if (!$configCertificado->validate()) {
                            $defaultTab = 'configCertificado';
                            $valid = $valid & false;
                        } else { 
                            $valid = $valid & true;
                        }
                        
                        if ($valid) {
                            $transaction = $model->getDbConnection()->beginTransaction();
                            try {
                                $model->save();
                                $model->refresh();

                                foreach ($enderecos as $endereco) {
                                    $endereco->empresa_id = $model->id;
                                    $endereco->save();
                                }
                                $configCertificado->save();
                                $transaction->commit();
                            } catch (Exception $e) {
                                $transaction->rollback();
                            }
                            Yii::app()->user->setFlash('success', 'Os dados foram editados com sucesso.');
                            $this->redirect(array('update', 'id' => $model->id));
                        } else {
                            Yii::app()->user->setFlash('warning', 'Favor corrigir os campos assinalados e tente salvar novamente.');
                        }
		}
                
                

		$this->render('update',array(
			'model'=>$model,
                        'enderecos'=>$enderecos,
                        'configCertificado' => $configCertificado,
                        'defaultTab'=>$defaultTab,
		));
	}
        
        private function loadEndereco($model)
        {
            $enderecos = $model->enderecos;
            if (!$enderecos)
                $enderecos[0] = new Endereco;
            return $enderecos;
        }
	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Empresa');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Empresa('search');
                
                // set the columns to those posted
                if (!empty($_POST['columns'])) {
                    // name the cookies after the model or something unique so that other gridviews don't overwrite this one
                    unset(Yii::app()->request->cookies['empresa-columns']);  // first unset cookie for columns
                    Yii::app()->request->cookies['empresa-columns'] = new CHttpCookie('empresa-columns', serialize($_POST['columns']));  // define cookie for columns
                    $columns = $_POST['columns'];
                } elseif (empty(Yii::app()->request->cookies['empresa-columns'])) {
                    // if no columns are selected, set the default columns
                    // let's assume MyModel has at least id, name, email, phone, birthday (date), and active (boolean)
                    $columns = array(
                        'id',
                        'nome',
                        'razao_social',
                        'ddd_tel_comercial',
                        'telefone_comercial',
                        'email',
                    );
                } else {
                    $columns = unserialize(Yii::app()->request->cookies['empresa-columns']);
                }

                
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Empresa']))
			$model->attributes=$_GET['Empresa'];
                
                $pageSize=Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']);
                if(isset($_GET['pageSize']))
                {
                    Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
                    unset($_GET['pageSize']);
                }
		$this->render('admin',array(
			'model'=>$model,
                        'columns'=>$columns,
                        'pageSize' => $pageSize,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Empresa the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=Empresa::model()->with('configCertificados')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	/**
	* Performs the AJAX validation.
	* @param Empresa $model the model to be validated
        * @param Endereco $endereco the model to be validated
	*/
	protected function performAjaxValidation($models)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='empresa-form')
		{
			echo CActiveForm::validate($models);
			Yii::app()->end();
		}
	}
        
        /**
	* Performs the TABULAR AJAX validation.
	* @param Empresa $model the model to be validated
        * @param Endereco $endereco the model to be validated
	*/
	protected function performTabularAjaxValidation($models)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='empresa-form')
		{
			echo CActiveForm::validateTabular($models);
			Yii::app()->end();
		}
	}
        
        public function actionLoadEnderecoByAjax()
        {
            $model = new Endereco;
            $this->renderPartial('endereco/_form', array(
                'model'=>$model,
            ), false, true);
        }
}