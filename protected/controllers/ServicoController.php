<?php

class ServicoController extends Controller
{       
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column1';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Servico;
                $instrumento = new Instrumento('search');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Servico']))
		{
                        if (isset($_POST['instrumento-grid_c0'])) {
                            $instrumento_list = $_POST['instrumento-grid_c0'];
                            foreach ($instrumento_list as $instrumento_id) {
                                $model->attributes=$_POST['Servico'];
                                $model->instrumento_id = $instrumento_id;
                                $valid = $model->validate();
                                if ($valid)
                                    $model->save();
                                $model = new Servico;
                            }
                        }
			if($valid)
				$this->redirect(array('admin'));
		}
                
                $instrumento->unsetAttributes();  // clear any default values
		if(isset($_GET['Instrumento']))
			$instrumento->attributes=$_GET['Instrumento'];
                else if (isset($_POST['Instrumento']))
                        $instrumento->attributes=$_POST['Instrumento'];
                
                
		$this->render('create',array(
                    'model'=>$model,
                    'instrumento'=>$instrumento,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Servico']))
		{
			$model->attributes=$_POST['Servico'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Servico');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Servico('search');
                
                // set the columns to those posted
                if (!empty($_POST['columns'])) {
                    // name the cookies after the model or something unique so that other gridviews don't overwrite this one
                    unset(Yii::app()->request->cookies['servico-columns']);  // first unset cookie for columns
                    Yii::app()->request->cookies['servico-columns'] = new CHttpCookie('servico-columns', serialize($_POST['columns']));  // define cookie for columns
                    $columns = $_POST['columns'];
                } elseif (empty(Yii::app()->request->cookies['servico-columns'])) {
                    // if no columns are selected, set the default columns
                    $columns = array(
                        'id',
                        'nome_servico',
                        'instrumento_id',
                        'custo',
                        'valor'
                    );
                } else {
                    $columns = unserialize(Yii::app()->request->cookies['servico-columns']);
                }
                
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Servico']))
			$model->attributes=$_GET['Servico'];

		$pageSize=Yii::app()->user->getState('pageSizeServico', Yii::app()->params['defaultPageSize']);
		if(isset($_GET['pageSizeServico']))
                {
                    Yii::app()->user->setState('pageSizeServico', (int)$_GET['pageSizeServico']);
                    unset($_GET['pageSizeServico']);
                }
		$this->render('admin',array(
			'model'=>$model,
                        'columns'=>$columns,
                        'pageSize' => $pageSize,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Servico the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
		$model=Servico::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param Servico $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='servico-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
}