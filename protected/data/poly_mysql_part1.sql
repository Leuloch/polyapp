/**
 * Database schema required by Polyafer Intranet - Part1.
 *
 * @author Ednei Oliveira <leuloch@gmail.com>
 * @link http://www.oliah.com.br
 * @copyright 2014 Oliah Tec
 * @version 1.3.0
 * @created April 07, 2014
 * @updated August 26, 2014
 */


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
-- -----------------------------------------------------
-- Table `tbl_user`
-- @module user
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `filename` varchar(1000) NULL DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `create_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`),
  KEY `superuser` (`superuser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

ALTER TABLE `tbl_users` 
ADD COLUMN `ddd_phone` INT UNSIGNED NULL DEFAULT NULL AFTER `email`,
ADD COLUMN `phone` BIGINT UNSIGNED NULL DEFAULT NULL AFTER `ddd_phone`,
ADD COLUMN `ddd_mobile` INT UNSIGNED NULL DEFAULT NULL AFTER `phone`,
ADD COLUMN `mobile` BIGINT UNSIGNED NULL DEFAULT NULL AFTER `ddd_mobile`,
ADD COLUMN `skype` VARCHAR(100) NULL DEFAULT NULL AFTER `mobile`,
ADD COLUMN `facebook` VARCHAR(255) NULL DEFAULT NULL AFTER `skype`;

CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

CREATE TABLE `tbl_profiles_fields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `field_type` varchar(50) NOT NULL,
  `field_size` varchar(15) NOT NULL DEFAULT '0',
  `field_size_min` varchar(15) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` varchar(5000) NOT NULL DEFAULT '',
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` varchar(5000) NOT NULL DEFAULT '',
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `varname` (`varname`,`widget`,`visible`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'leuloch@example.com', '9a24eff8c15a6a141ece27eb6947da0f', 1, 1),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@example.com', '099f825543f7850cc038b90aaff39fac', 0, 1);

INSERT INTO `tbl_profiles` (`user_id`, `lastname`, `firstname`) VALUES
(1, 'Admin', 'Administrator'),
(2, 'Demo', 'Demo');

INSERT INTO `tbl_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'lastname', 'Last Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'firstname', 'First Name', 'VARCHAR', 50, 3, 1, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 0, 3);

-- -----------------------------------------------------
-- Tables `AuthAssignment`, `AuthItemChild` and `AuthItem`
-- @module auth
-- -----------------------------------------------------

drop table if exists `AuthAssignment`;
drop table if exists `AuthItemChild`;
drop table if exists `AuthItem`;

create table `AuthItem`
(
   `name`                 varchar(64) not null,
   `type`                 integer not null,
   `description`          text,
   `bizrule`              text,
   `data`                 text,
   primary key (`name`)
) engine InnoDB;

create table `AuthItemChild`
(
   `parent`               varchar(64) not null,
   `child`                varchar(64) not null,
   primary key (`parent`,`child`),
   foreign key (`parent`) references `AuthItem` (`name`) on delete cascade on update cascade,
   foreign key (`child`) references `AuthItem` (`name`) on delete cascade on update cascade
) engine InnoDB;

create table `AuthAssignment`
(
   `itemname`             varchar(64) not null,
   `userid`               varchar(64) not null,
   `bizrule`              text,
   `data`                 text,
   primary key (`itemname`,`userid`),
   foreign key (`itemname`) references `AuthItem` (`name`) on delete cascade on update cascade
) engine InnoDB;

-- -----------------------------------------------------
-- Table `tbl_empresa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_empresa` ;

CREATE TABLE IF NOT EXISTS `tbl_empresa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_atualizacao` DATETIME NULL DEFAULT NULL,
  `usuario_criacao` INT NULL DEFAULT NULL,
  `usuario_atualizacao` INT NULL DEFAULT NULL,
  `nome` VARCHAR(255) NOT NULL,
  `cliente` TINYINT(1) NULL DEFAULT 1,
  `fornecedor` TINYINT(1) NULL DEFAULT 0,
  `num_cadastro` INT(5) NULL,
  `razao_social` VARCHAR(255) NULL DEFAULT NULL,
  `tipo_pessoa` TINYINT(1) NOT NULL DEFAULT 0,
  `cnpj_cpf` BIGINT(15) NULL DEFAULT NULL,
  `ddd_tel_comercial` INT NULL DEFAULT NULL,
  `telefone_comercial` BIGINT(14) NULL DEFAULT NULL,
  `ddd_celular` INT NULL DEFAULT NULL,
  `celular` BIGINT(14) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `website` VARCHAR(100) NULL DEFAULT NULL,
  `inscricao_estadual` BIGINT(14) NULL DEFAULT NULL,
  `isencao_ie` TINYINT(1) NULL DEFAULT NULL,
  `inscricao_municipal` BIGINT(18) NULL DEFAULT NULL,
  `inscricao_suframa` BIGINT(9) NULL DEFAULT NULL,
  `opta_simples` TINYINT(1) NULL DEFAULT NULL,
  `isencao_icms` TINYINT(1) NULL DEFAULT 1,
  `observacao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'Tabela de Cadastro de Empresas (Clientes e Forneceores)';

CREATE INDEX `NOME` ON `tbl_empresa` (`nome` ASC);

CREATE INDEX `CADASTRO` ON `tbl_empresa` (`num_cadastro` ASC);

CREATE INDEX `RAZAO_SOCIAL` ON `tbl_empresa` (`razao_social` ASC);


-- -----------------------------------------------------
-- Table `tbl_endereco`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_endereco` ;

CREATE TABLE IF NOT EXISTS `tbl_endereco` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `endereco` VARCHAR(150) NOT NULL,
  `empresa_id` INT NULL,
  `numero` VARCHAR(20) NULL DEFAULT NULL,
  `complemento` VARCHAR(50) NULL DEFAULT NULL,
  `bairro` VARCHAR(100) NULL DEFAULT NULL,
  `cidade` VARCHAR(100) NULL DEFAULT NULL,
  `estado` CHAR(2) NULL DEFAULT NULL,
  `cep` INT(8) NULL DEFAULT NULL,
  `tipo_endereco_faturamento` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '0-nada;\n1-endereço faturamento;\n2-endereço entrega;\n3-fat + entrega;\n4-endereço cobrança;\n5-fat + cobrança;\n6-entrega + cobrança;\n7-faturmento+entrega+cobrança.',
  `tipo_endereco_entrega` TINYINT(1) NOT NULL DEFAULT 1,
  `tipo_endereco_cobranca` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_endereco_tbl_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tbl_config_certificado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_config_certificado` ;

CREATE TABLE IF NOT EXISTS `tbl_config_certificado` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `nome_certificado` VARCHAR(255) NOT NULL,
  `endereco` VARCHAR(270) NULL,
  `numero` VARCHAR(20) NULL DEFAULT NULL,
  `cidade` VARCHAR(100) NULL,
  `estado` CHAR(2) NULL,
  `cep` INT(8) NULL,
  `validade` TINYINT(1) NULL DEFAULT 0,
  `aprovacao` TINYINT(1) NULL DEFAULT 0,
  `opta_digital` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_config_certificado_tbl_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_contato`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_contato` ;

CREATE TABLE IF NOT EXISTS `tbl_contato` (
  `id` INT NOT NULL,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_atualizacao` DATETIME NULL DEFAULT NULL,
  `usuario_criacao` INT NULL DEFAULT NULL,
  `usuario_atualizacao` INT NULL DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `cargo` VARCHAR(50) NULL DEFAULT NULL,
  `setor` VARCHAR(50) NULL DEFAULT NULL,
  `ddd_tel_comercial` INT NULL DEFAULT NULL,
  `tel_comercial` BIGINT(14) NULL DEFAULT NULL,
  `tel_comercial_ramal` INT(4) NULL DEFAULT NULL,
  `ddd_tel_outro` INT NULL DEFAULT NULL,
  `tel_outro` BIGINT(14) NULL DEFAULT NULL,
  `tel_outro_ramal` INT(4) NULL DEFAULT NULL,
  `ddd_cel_comercial` INT NULL DEFAULT NULL,
  `cel_comercial` BIGINT(14) NULL DEFAULT NULL,
  `ddd_cel_outro` INT NULL DEFAULT NULL,
  `cel_outro` BIGINT(14) NULL DEFAULT NULL,
  `email` VARCHAR(100) NULL DEFAULT NULL,
  `email_outro` VARCHAR(100) NULL DEFAULT NULL,
  `contato_administrativo` TINYINT(1) NULL DEFAULT '1',
  `contato_comercial` TINYINT(1) NULL DEFAULT '1',
  `contato_financeiro` TINYINT(1) NULL DEFAULT '1',
  `contato_tecnico` TINYINT(1) NULL DEFAULT '1',
  `empresa_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_contato_empresa`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table `tbl_fabricante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_fabricante` ;

CREATE TABLE IF NOT EXISTS `tbl_fabricante` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_fabricante` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table `tbl_categoria_instrumento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_categoria_instrumento` ;

CREATE TABLE IF NOT EXISTS `tbl_categoria_instrumento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_instrumento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_instrumento` ;

CREATE TABLE IF NOT EXISTS `tbl_instrumento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `tag` TEXT NULL COMMENT 'Para pesquisa.',
  `modelo` VARCHAR(50) NULL,
  `fabricante_id` INT NULL,
  `categoria_instrumento_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_instrumento_tbl_fabricante1`
    FOREIGN KEY (`fabricante_id`)
    REFERENCES `tbl_fabricante` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_instrumento_tbl_categoria_instrumento1`
    FOREIGN KEY (`categoria_instrumento_id`)
    REFERENCES `tbl_categoria_instrumento` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tbl_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_servico` ;

CREATE TABLE IF NOT EXISTS `tbl_servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_servico` VARCHAR(255) NOT NULL,
  `instrumento_id` INT NULL DEFAULT NULL,
  `custo` DECIMAL(6,2) NULL DEFAULT NULL,
  `valor` DECIMAL(6,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_servico_tbl_instrumento1`
    FOREIGN KEY (`instrumento_id`)
    REFERENCES `tbl_instrumento` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_produto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_produto` ;

CREATE TABLE IF NOT EXISTS `tbl_produto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_produto` VARCHAR(100) NOT NULL,
  `codigo_produto` VARCHAR(45) NULL DEFAULT NULL,
  `valor` DECIMAL(6,2) NULL DEFAULT NULL,
  `custo` DECIMAL(6,2) NULL DEFAULT NULL,
  `instrumento_id` INT NULL DEFAULT NULL,
  `codigo_EAN` BIGINT(14) NULL DEFAULT NULL,
  `ncm` INT(9) NULL DEFAULT NULL COMMENT 'tabela completa em http://www.sefaz.mt.gov.br/portal/download/arquivos/Tabela_NCM.pdf',
  `unidade_medida` TINYINT(1) NULL DEFAULT NULL,
  `peso_liquido` DECIMAL(7,3) NULL DEFAULT NULL,
  `peso_bruto` DECIMAL(7,3) NULL DEFAULT NULL,
  `fabricante` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_produto_tbl_fabricante1`
    FOREIGN KEY (`fabricante`)
    REFERENCES `tbl_fabricante` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_produto_tbl_instrumento1`
    FOREIGN KEY (`instrumento_id`)
    REFERENCES `tbl_instrumento` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `tbl_grandeza`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_grandeza` ;

CREATE TABLE IF NOT EXISTS `tbl_grandeza` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `grandeza` VARCHAR(45) NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `tbl_unidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_unidade` ;

CREATE TABLE IF NOT EXISTS `tbl_unidade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `simbolo` VARCHAR(5) NULL,
  `nome` VARCHAR(45) NULL DEFAULT NULL,
  `grandeza_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_unidade_tbl_grandeza1`
    FOREIGN KEY (`grandeza_id`)
    REFERENCES `tbl_grandeza` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = 'Foi Baixado arquivo sistema-internacional-unidades.pdf no dr' /* comment truncated */ /*opbox.*/;


-- -----------------------------------------------------
-- Table `tbl_sistema_medicao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_sistema_medicao` ;

CREATE TABLE IF NOT EXISTS `tbl_sistema_medicao` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome_sistema` VARCHAR(100) NOT NULL,
  `escala_min` VARCHAR(45) NULL DEFAULT NULL,
  `escala_max` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_escala_id` INT NOT NULL,
  `menor_divisao` VARCHAR(45) NULL DEFAULT NULL,
  `unidade_div_id` INT NOT NULL,
  `instrumento_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_sistema_medicao_tbl_instrumento1`
    FOREIGN KEY (`instrumento_id`)
    REFERENCES `tbl_instrumento` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_sistema_medicao_tbl_unidade1`
    FOREIGN KEY (`unidade_escala_id`)
    REFERENCES `tbl_unidade` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_sistema_medicao_tbl_unidade2`
    FOREIGN KEY (`unidade_div_id`)
    REFERENCES `tbl_unidade` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `tbl_fornecedor_produto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_fornecedor_produto` ;

CREATE TABLE IF NOT EXISTS `tbl_fornecedor_produto` (
  `produto_id` INT NOT NULL,
  `empresa_id` INT NOT NULL,
  PRIMARY KEY (`produto_id`, `empresa_id`),
  CONSTRAINT `fk_tbl_fornecedorProduto_tbl_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_fornecedorProduto_tbl_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `tbl_produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_orcamento_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_orcamento_servico` ;

CREATE TABLE IF NOT EXISTS `tbl_orcamento_servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data_criacao` DATETIME NULL DEFAULT NULL,
  `data_atualizacao` DATETIME NULL DEFAULT NULL,
  `usuario_criacao` INT NULL DEFAULT NULL,
  `usuario_atualizacao` INT NULL DEFAULT NULL,
  `data_emissao` DATETIME NOT NULL,
  `contato_id` INT NULL DEFAULT NULL,
  `previsao_entrega` VARCHAR(45) NULL DEFAULT NULL COMMENT 'e. g. \'7 a 10 dias úteis.\'',
  `emitir_boleto` TINYINT(1) NULL DEFAULT NULL,
  `condicao_pagamento` INT NULL COMMENT 'e. g. \'A vista\' ou \'0\', 7 d.d.l, 0+2x, etc',
  `condicao_pagamento_texto` TEXT NULL DEFAULT NULL,
  `historico_negociacao` TEXT NULL DEFAULT NULL,
  `validade_proposta` VARCHAR(45) NULL DEFAULT NULL,
  `observacao` VARCHAR(45) NULL DEFAULT NULL,
  `forma_cotacao` INT NULL DEFAULT NULL COMMENT 'email;telefone;pessoalmente;site',
  `num_revisao` INT UNSIGNED NULL DEFAULT 0,
  `tipo_frete` INT NULL COMMENT 'e. g.: Por conta do cliente;sedex à cobrar;etc',
  `desconto` DECIMAL(10,2) NULL DEFAULT NULL,
  `conta_bancaria_id` INT NULL DEFAULT NULL,
  `empresa_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_orcamentoServico_empresa`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_itens_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_itens_servico` ;

CREATE TABLE IF NOT EXISTS `tbl_itens_servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `orcamento_servico_id` INT NOT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `servico_id` INT NULL DEFAULT NULL,
  `valor` DECIMAL(6,2) NULL DEFAULT NULL,
  `quantidade` INT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_itensServico_orcamentoServico`
    FOREIGN KEY (`orcamento_servico_id`)
    REFERENCES `tbl_orcamento_servico` (`id`)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_tbl_itens_servico_tbl_servico1`
    FOREIGN KEY (`servico_id`)
    REFERENCES `tbl_servico` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_impostos_servico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_impostos_servico` ;

CREATE TABLE IF NOT EXISTS `tbl_impostos_servico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `orcamento_servico_id` INT NULL,
  `tipo_imposto_retido` INT NULL,
  `desconto_percentual` DECIMAL(6,3) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_impostosServico_tbl_orcamento_servico1`
    FOREIGN KEY (`orcamento_servico_id`)
    REFERENCES `tbl_orcamento_servico` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tbl_regra_tributacao_estado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_regra_tributacao_estado` ;

CREATE TABLE IF NOT EXISTS `tbl_regra_tributacao_estado` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `produto_id` INT NULL,
  `tipo_imposto` TINYINT(2) NULL COMMENT '0-ICMS;1-IPI;2-PIS;3-COFINS;4-Informações da NF',
  `cenario` TINYINT(2) NULL COMMENT '0-Venda para dentro do estado;1-Venda para fora do estado;2-Venda para dentro e fora do estado.',
  `icms_situacao_tributaria` INT NULL COMMENT 'e.g. ' /* comment truncated */ /*00-Tributada integralmente.
10-Tributada e com cobrança do ICMS por substituição tributária.
etc...*/,
  `icms_origem` INT NULL COMMENT 'e.g.' /* comment truncated */ /*0-Nacional;
1-Estrangeira - Importação Direta.
etc...*/,
  `icms_mod_bc` TINYINT(2) NULL COMMENT '0-Margem Valor Agregado;' /* comment truncated */ /*1-Pauta(Valor);
2-Preço Tabelado Máx.(Valor);
3-Valor da Operação.*/,
  `icms_aliquota` DECIMAL(6,2) NULL,
  `icms_credito` DECIMAL(6,2) NULL,
  `ipi_situacao_tributaria` INT NULL COMMENT 'e.g.' /* comment truncated */ /*00-Entrada com recuperacao de credito;
01-Entrada tributada com alíquota zero;
02-Entrada Isenta;
...
99-Outras Saídas.*/,
  `ipi_cod_enq` INT(3) NULL DEFAULT '999',
  `pis_situacao_tributaria` INT NULL COMMENT 'e.g.' /* comment truncated */ /*01-Operaçao Tributável (base de cálculo = valor da operação alíquota normal (cumulativo/não cumulativo));
02-Operação Tributável (base de cálculo = valor da operação (alíquota diferenciada));
...
99-Outras Operações.*/,
  `pis_aliquota` DECIMAL(6,2) NULL,
  `cofins_situacao_tributaria` INT NULL COMMENT 'a mesma lista de pis_situacao_tributaria',
  `cofins_aliquota` DECIMAL(8,2) NULL,
  `nf_informacoes` TEXT NULL,
  `regra_trib_empresa_id` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_regraTributacaoEstado_tbl_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `tbl_produto` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `tbl_regra_tributacao_empresa`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_regra_tributacao_empresa` ;

CREATE TABLE IF NOT EXISTS `tbl_regra_tributacao_empresa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `produto_id` INT NULL,
  `tipo_imposto` TINYINT(2) NULL COMMENT 'ver tbl_regraTributacaoEstado',
  `cenario` TINYINT(2) NULL COMMENT '0-Venda para Empresa Participante do Simples;' /* comment truncated */ /*1-Venda para empresa não participante do simples;
3-Venda para empresa participante ou não do simples.*/,
  `icms_cfop` INT NULL COMMENT 'e.g.' /* comment truncated */ /*1000 - ENTRADAS OU AQUISIÇÕES DE SERVIÇOS DO ESTADO;
1100 - Compras para industrialização, comercialização ou prestação de serviços;
...
1126 - Compra para utilização na prestação de serviço;
(como parecem mtos itens, no site
do compraAzul o combo tem opção de ir carregando aos poucos)*/,
  `icms_mod_bc` TINYINT(2) NULL COMMENT '0-Preço Tabelado ou Máximo Sugerid' /* comment truncated */ /*;
1-Lista Negativa(Valor);
2-Lista Positiva (Valor);
3-Lista Neutra (Valor);
4-Margem Valor Agregado (%);
5-Pauta(Valor).*/,
  `icms_aliquota` DECIMAL(8,2) NULL,
  `icms_credito` DECIMAL(8,2) NULL,
  `regra_trib_estado_id` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_regraTributacaoEmpresa_tbl_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `tbl_produto` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `tbl_conversao`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_conversao` ;

CREATE TABLE IF NOT EXISTS `tbl_conversao` (
  `id` INT NOT NULL,
  `unidade_id_de` INT NOT NULL,
  `unidade_id_para` INT NOT NULL,
  `fator_conversao` FLOAT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_conversao_tbl_unidade1`
    FOREIGN KEY (`unidade_id_de`)
    REFERENCES `tbl_unidade` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_conversao_tbl_unidade2`
    FOREIGN KEY (`unidade_id_para`)
    REFERENCES `tbl_unidade` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `tbl_produto_cliente_preco`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_produto_cliente_preco` ;

CREATE TABLE IF NOT EXISTS `tbl_produto_cliente_preco` (
  `produto_id` INT NOT NULL,
  `empresa_id` INT NOT NULL,
  `valor` DECIMAL(6,2) NOT NULL,
  PRIMARY KEY (`produto_id`, `empresa_id`),
  CONSTRAINT `fk_tbl_produto_has_tbl_empresa_tbl_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `tbl_produto` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_produto_has_tbl_empresa_tbl_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tbl_servico_empresa_preco`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_servico_empresa_preco` ;

CREATE TABLE IF NOT EXISTS `tbl_servico_empresa_preco` (
  `servico_id` INT NOT NULL,
  `empresa_id` INT NOT NULL,
  `valor` DECIMAL(6,2) NULL,
  PRIMARY KEY (`servico_id`, `empresa_id`),
  CONSTRAINT `fk_tbl_servico_has_tbl_empresa_tbl_servico1`
    FOREIGN KEY (`servico_id`)
    REFERENCES `tbl_servico` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_servico_has_tbl_empresa_tbl_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `tbl_empresa` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `tbl_order` ;

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `data_criacao` DATETIME NULL,
  `data_atualizacao` DATETIME NULL DEFAULT NULL,
  `usuario_criacao` INT NULL DEFAULT NULL,
  `usuario_atualizacao` INT NULL DEFAULT NULL,
  `cliente_id` INT NULL DEFAULT NULL,
  `contato_id` INT NULL DEFAULT NULL,
  `status` INT NULL DEFAULT NULL COMMENT 'Usado para guardar o codigo do status de uma ordem de serviço ou venda, por exemplo: aguardando, liberado, aprovado, etc.',
  `observacao` TEXT NULL DEFAULT NULL,
  `forma_entrada` INT NULL DEFAULT NULL,
  `nota_fiscal` INT UNSIGNED NULL DEFAULT NULL,
  `data_entrada` DATETIME NULL DEFAULT NULL,
  `data_prev_saida` DATETIME NULL DEFAULT NULL,
  `data_disponibilidade` DATETIME NULL DEFAULT NULL,
  `data_retirada` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_order_items`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_order_items` ;

CREATE TABLE IF NOT EXISTS `tbl_order_items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ordem_id` INT NOT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `quantidade` INT NULL DEFAULT NULL,
  `instrumento_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_itens_os_tbl_ordem_servico1`
    FOREIGN KEY (`ordem_id`)
    REFERENCES `tbl_order` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_order_items_tbl_instrumento1`
    FOREIGN KEY (`instrumento_id`)
    REFERENCES `tbl_instrumento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Table `tbl_evento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_evento` ;

CREATE TABLE IF NOT EXISTS `tbl_evento` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `group_id` INT NULL DEFAULT NULL,
  `nome` VARCHAR(100) NOT NULL,
  `descricao` TEXT NULL DEFAULT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  `icon` VARCHAR(45) NULL DEFAULT NULL,
  `allday` TINYINT NULL DEFAULT 1,
  `tipo` INT NULL DEFAULT NULL,
  `status` INT NULL DEFAULT NULL,
  `color` VARCHAR(255) NULL DEFAULT NULL,
  `date_start` DATETIME NULL DEFAULT NULL,
  `date_end` DATETIME NULL DEFAULT NULL,
  `order_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_tbl_evento_tbl_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `tbl_order` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tbl_evento_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tbl_evento_users` ;

CREATE TABLE IF NOT EXISTS `tbl_evento_users` (
  `evento_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`evento_id`, `users_id`),
  CONSTRAINT `fk_tbl_evento_has_tbl_users_tbl_evento1`
    FOREIGN KEY (`evento_id`)
    REFERENCES `tbl_evento` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_evento_has_tbl_users_tbl_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `tbl_users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
