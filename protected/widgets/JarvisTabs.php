<?php

/**
 * jarvisTabs class file.
 *
 * @author Ednei Oliveira <leuloch@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2008-2013 Yii Software LLC
 */
Yii::import('zii.widgets.jui.CJuiWidget');

/**
 * jarvisTabs displays a tabs widget into Jarvis Widget.
 *
 * 
 * @author Ednei Oliveira <leuloch@gmail.com>
 * @since 1.1
 */
class JarvisTabs extends CJuiWidget {

    /**
     * @var array list of tabs (tab title=>tab content).
     * Note that the tab title will not be HTML-encoded.
     * The tab content can be either a string or an array. When it is an array, it can
     * be in one of the following two formats:
     * <pre>
     * array('id'=>'myTabID', 'content'=>'tab content')
     * array('id'=>'myTabID', 'ajax'=>URL)
     * </pre>
     * where the 'id' element is optional. The second format allows the tab content
     * to be dynamically fetched from the specified URL via AJAX. The URL can be either
     * a string or an array. If an array, it will be normalized into a URL using {@link CHtml::normalizeUrl}.
     */
    public $tabs = array();
    
    /**
     *
     * @var string The id of the default tab 
     */
    public $defaultTab = null;
    
    /**
     *
     * @var array html options for article tag.
     * The article tag define the width and position of the 
     * widget Jarvis.
     * <pre>
     * array('class'=>'col-sm-12 col-md-12 col-lg-6')
     * </pre> 
     */
    public $articleOptions = array();

    /**
     * @var string the name of the container element that contains all panels. Defaults to 'div'.
     */
    public $tagName = 'div';

    /**
     * @var string the template that is used to generated every panel title.
     * The token "{title}" in the template will be replaced with the panel title and
     * the token "{url}" will be replaced with "#TabID" or with the url of the ajax request.
     */
    public $headerTemplate = '<li class="{status}"><a data-toggle="tab" href="{url}" title="{id}">{title}</a></li>';

    /**
     * @var string the template that is used to generated every tab content.
     * The token "{content}" in the template will be replaced with the panel content
     * and the token "{id}" with the tab ID.
     */
    public $contentTemplate = '<div class="tab-pane {status}" id="{id}">{content}</div>';
    
    /**
     * @var array the list of buttons to include in the header widget jarvis 
     */
    public $buttons = array();
    
    public $buttonOptions = array();
    
    public $buttonLabel = '';
    

    /**
     * Run this widget.
     * This method registers necessary javascript and renders the needed HTML code.
     */
    public function run() {
        $id = $this->getId();
        if (isset($this->htmlOptions['id']))
            $id = $this->htmlOptions['id'];
        else
            $this->htmlOptions['id'] = $id;
        
        $this->htmlOptions['class'] = 'jarviswidget';
        
        echo CHtml::openTag('section', array('id'=>'widget-grid')) . "\n";
        echo "\t" . CHtml::openTag('div', array('class'=>'row')) . "\n";
        echo "\t\t" . CHtml::openTag('article', $this->articleOptions) ."\n";
        echo "\t\t\t" . CHtml::openTag('div', $this->htmlOptions) . "\n";
        echo "\t\t\t\t" . '<header>' ."\n";
        if (!empty($this->buttons))
        {
            echo CHtml::openTag('div', array('class'=>'widget-toolbar', 'role'=>'menu')) . "\n";
            echo BsHtml::buttonDropdown($this->buttonLabel, $this->buttons, $this->buttonOptions);
            echo "</div>\n";
        }
        
        $tabsOut = "";
        $contentOut = "";
        $tabCount = 0;

        foreach ($this->tabs as $title => $content) {
            $tabId = (is_array($content) && isset($content['id'])) ? $content['id'] : $id . '_tab_' . $tabCount++;
            if ($this->defaultTab){
                if ($this->defaultTab===$tabId)
                    $active = "active";
                else
                    $active = "";
            } else {
                if (!isset($active))
                    $active = "active";
                else
                    $active = "";
            }
            if (!is_array($content)) {
                $tabsOut.=strtr($this->headerTemplate, array('{status}' => $active,'{title}' => $title, '{url}' => '#' . $tabId, '{id}' => '#' . $tabId)) . "\n";
                $contentOut.=strtr($this->contentTemplate, array('{status}' => $active,'{content}' => $content, '{id}' => $tabId)) . "\n";
            } elseif (isset($content['ajax'])) {
                $tabsOut.=strtr($this->headerTemplate, array('{status}' => $active,'{title}' => $title, '{url}' => CHtml::normalizeUrl($content['ajax']), '{id}' => '#' . $tabId)) . "\n";
            } else {
                $tabsOut.=strtr($this->headerTemplate, array('{status}' => $active,'{title}' => $title, '{url}' => '#' . $tabId, '{id}' => $tabId)) . "\n";
                if (isset($content['content']))
                    $contentOut.=strtr($this->contentTemplate, array('{status}' => $active,'{content}' => $content['content'], '{id}' => $tabId)) . "\n";
            }
        }
        echo "\t\t\t\t\t" . '<ul class="nav nav-tabs pull-left in">' ."\n" . $tabsOut . "</ul>\n";
        echo "\t\t\t\t" . '</header>' ."\n";
        echo "\t\t\t\t\t" . '<div>' ."\n";
        echo "\t\t\t\t\t\t" . '<div class="jarviswidget-editbox">' ."\n";
        echo "\t\t\t\t\t\t" . '<!-- This area used as dropdown edit box -->' ."\n";
        echo "\t\t\t\t\t\t" . '</div>';
        echo "\t\t\t\t\t" . '<div class="widget-body">' ."\n";
        echo "\t\t\t\t\t\t" . '<div class="tab-content">' ."\n";
        echo $contentOut;
        echo "\t\t\t\t\t\t" . '</div>' ."\n";
        echo "\t\t\t\t\t" . '</div>' ."\n";
        echo "\t\t\t" . '</div>' ."\n";
        echo "\t\t" . '</div>' ."\n";
        echo "\t" . '</article>' ."\n";
        echo '</div>' ."\n";
        echo '</section>' ."\n";

        $options = CJavaScript::encode($this->options);
        //Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, "jQuery('#{$id}').tabs($options);");
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/smartwidgets/jarvis.widget.min.js',  CClientScript::POS_END);
    }

    /**
     * Registers the core script files.
     * This method overrides the parent implementation by registering the cookie plugin when cookie option is used.
     */
    protected function registerCoreScripts() {
        parent::registerCoreScripts();
        if (isset($this->options['cookie']))
            Yii::app()->getClientScript()->registerCoreScript('cookie');
    }

}
