<?php
/**
 * JarvisWidget class file
 * 
 * @author Ednei Oliveira <leuloch@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2014 Oliah Tecnologia
 * @see Helper JvHtml
 * 
 */
Yii::import('application.helpers.JvHtml');

class JarvisWidget extends CWidget
{
    /**
     * @var array additional HTML attributes that should be rendered for the section tag.
     */
    public $sectionOptions = array('id'=>'widget-grid');
    
    /**
     * Initializes the widget
     * This renders the jarvisWidget open tags
     */
    public function init() {
        echo JvHtml::beginJarvis($this->sectionOptions);
    }
    
    /**
     * Runs the widget
     * This registers the necessary javascript code and renders the jarvisWidget close tags
     */
    public function run() {
        echo JvHtml::endJarvis();
        Yii::app()->getClientScript()->registerScriptFile(Yii::app()->theme->baseUrl.'/js/smartwidgets/jarvis.widget.min.js',  CClientScript::POS_END);
    }
    
    /**
     * Initializes the Jarvis Widget inside de grid
     * 
     * @param string $widgetId the unique Id that jarvisWidget uses to save user configurations
     * @param string $header the title display for the current widget
     * @param array $htmlOptions the additional HTML attributes to render jarvisWidget
     * 
     * @return string the begin HTML tags for Jarvis Widget
     */
    public function beginJarvisWidget($widgetId, $header='', $htmlOptions=array(), $bodyHtmlOptions=array(), $articleOptions=array()) {
        echo JvHtml::beginJvWidget($widgetId, $header, $htmlOptions, $bodyHtmlOptions, $articleOptions);
    }
    
    /**
     * 
     * Close the Jarvis Widget
     * This close the HTML tags for de Jarvis Widget to insert another Jarvis Widgets
     * 
     * @return string the close HTML tags for Jarvis Widget
     */
    public function endJarvisWidget() {
        echo JvHtml::endJvWidget();
    }
}
