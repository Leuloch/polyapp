<?php
/**
 * 
 * JvHtml class file.
 * 
 * @author Ednei Oliveira <leuloch@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright 2014 Oliah Tecnologia
 * @see jarvisWidget
 * 
 */
class JvHtml extends CHtml
{

    // Jarvis widget configuration
    const DATA_WIDGET_COLORBUTTON = "data-widget-colorbutton";
    const DATA_WIDGET_EDITBUTTON = "data-widget-editbutton";
    const DATA_WIDGET_TOGGLEBUTTON = "data-widget-togglebutton";
    const DATA_WIDGET_DELETEBUTTON = "data-widget-deletebutton";
    const DATA_WIDGET_FULLSCREENBUTTON = "data-widget-fullscreenbutton";
    const DATA_WIDGET_CUSTOMBUTTON = "data-widget-custombutton";
    const DATA_WIDGET_COLLAPSED = "data-widget-collapsed";
    const DATA_WIDGET_SORTABLE = "data-widget-sortable";
    
    // Colors for widget
    const WIDGET_COLOR_MAGENTA = "jarviswidget-color-magenta";
    const WIDGET_COLOR_PINK = "jarviswidget-color-pink";
    const WIDGET_COLOR_PINKDARK = "jarviswidget-color-pinkDark";
    const WIDGET_COLOR_YELLOW = "jarviswidget-color-yellow";
    const WIDGET_COLOR_ORANGE = "jarviswidget-color-orange";
    const WIDGET_COLOR_ORANGEDARK = "jarviswidget-color-orangeDark";
    const WIDGET_COLOR_DARKEN = "jarviswidget-color-darken";
    const WIDGET_COLOR_PURPLE = "jarviswidget-color-purple";
    const WIDGET_COLOR_TEAL = "jarviswidget-color-teal";
    const WIDGET_COLOR_BLUE = "jarviswidget-color-blue";
    const WIDGET_COLOR_BLUEDARK = "jarviswidget-color-blueDark";
    const WIDGET_COLOR_BLUELIGHT = "jarviswidget-color-blueLight";
    const WIDGET_COLOR_RED = "jarviswidget-color-red";
    const WIDGET_COLOR_REDLIGHT = "jarviswidget-color-redLight";
    const WIDGET_COLOR_WHITE = "jarviswidget-color-white";
    const WIDGET_COLOR_GREEN = "jarviswidget-color-green";
    const WIDGET_COLOR_GREENDARK = "jarviswidget-color-greenDark";
    const WIDGET_COLOR_GREENLIGHT = "jarviswidget-color-greenLight";
    
    /**
     * Initializes the head for the Jarvis Widget
     * @param array $htmlOptions the additional HTML attributes for the Jarvis Widget
     * @return string
     */
    public static function beginJarvis($sectionOptions=array()) {
        $jarvis = self::openTag('section', $sectionOptions);
        $jarvis .= "\n" . self::openTag('div', array('class'=>'row'));
        return $jarvis;
    }
    
    /**
     * Close the Jarvis Widget
     * @return string
     */
    public static function endJarvis() {
        $jarvis = "\n" . self::closeTag('div');
        $jarvis .= "\n" . self::closeTag('section');
        return $jarvis;
    }
    
    /**
     * Initializes the Jarvis Widget inside de grid
     * @param string $widgetId the unique Id that jarvisWidget uses to save user configurations
     * @param string $header the title display for the current widget
     * @param array $htmlOptions the additional HTML attributes to render jarvisWidget
     * @return string
     */
    public static function beginJvWidget($widgetId, $header='', $htmlOptions=array(), $bodyHtmlOptions=array(), $articleOptions=array()) {
        if (isset($htmlOptions['class'])) {
            $htmlOptions['class'] .= ' jarviswidget';
        } else {
            $htmlOptions['class'] = 'jarviswidget';
        }
        $htmlOptions['id'] = $widgetId;
        if (isset($bodyHtmlOptions['class'])) {
            $bodyHtmlOptions['class'] .= ' widget-body';
        } else {
            $bodyHtmlOptions['class'] = 'widget-body';
        }
        $widget = self::tag('article', $articleOptions, false, false);
        $widget .= "\n" . self::tag('div', $htmlOptions, false, false);
        $widget .= "\n" . self::tag('header', array(), self::tag('h2',array(),$header));
        $widget .= "\n" . self::openTag('div');
        $widget .= "\n" . self::openTag('div', array('class'=>'jarviswidget-editbox'));
        $widget .= '<input class="form-control" type="text">' . '<span class="note"><i class="fa fa-check text-success"></i> Change title to update and save instantly!</span>';
        $widget .= "\n" . self::closeTag('div');
        $widget .= "\n" . self::openTag('div', $bodyHtmlOptions);
        return $widget;
    }
    
    /**
     * 
     * Close the Jarvis Widget
     * This close the HTML tags for de Jarvis Widget to insert another Jarvis Widgets
     * 
     * @return string the close HTML tags for Jarvis Widget
     */
    public static function endJvWidget() {
        $widget = "\n" . self::closeTag('div');
        $widget .= "\n" . self::closeTag('div');
        $widget .= "\n" . self::closeTag('div');
        $widget .= self::closeTag('article');
        
        return $widget;
    }
}
