<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
?>

<?php
$this->breadcrumbs=array(
	'Empresas'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Empresa', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Empresa', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Empresa', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Empresa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Empresa', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Empresa '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'data_criacao',
		'data_atualizacao',
		'usuario_criacao',
		'usuario_atualizacao',
		'nome',
		'cliente',
		'fornecedor',
		'num_cadastro',
		'razao_social',
		'tipo_pessoa',
		'cnpj_cpf',
		'ddd_tel_comercial',
		'telefone_comercial',
		'ddd_celular',
		'celular',
		'email',
		'website',
		'inscricao_estadual',
		'isencao_ie',
		'inscricao_municipal',
		'inscricao_suframa',
		'opta_simples',
		'isencao_icms',
		'observacao',
	),
)); ?>