<?php
/* @var $this EmpresaController */
/* @var $model Empresa */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'data_criacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'data_atualizacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'usuario_criacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'usuario_atualizacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'cliente'); ?>
    <?php echo $form->textFieldControlGroup($model,'fornecedor'); ?>
    <?php echo $form->textFieldControlGroup($model,'num_cadastro'); ?>
    <?php echo $form->textFieldControlGroup($model,'razao_social',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_pessoa'); ?>
    <?php echo $form->textFieldControlGroup($model,'cnpj_cpf',array('maxlength'=>15)); ?>
    <?php echo $form->textFieldControlGroup($model,'ddd_tel_comercial'); ?>
    <?php echo $form->textFieldControlGroup($model,'telefone_comercial',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'ddd_celular'); ?>
    <?php echo $form->textFieldControlGroup($model,'celular',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'website',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'inscricao_estadual',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'isencao_ie'); ?>
    <?php echo $form->textFieldControlGroup($model,'inscricao_municipal',array('maxlength'=>18)); ?>
    <?php echo $form->textFieldControlGroup($model,'inscricao_suframa',array('maxlength'=>9)); ?>
    <?php echo $form->textFieldControlGroup($model,'opta_simples'); ?>
    <?php echo $form->textFieldControlGroup($model,'isencao_icms'); ?>
    <?php echo $form->textAreaControlGroup($model,'observacao',array('rows'=>6)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
