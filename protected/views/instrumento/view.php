<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */
?>

<?php
$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Instrumento', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Instrumento', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Instrumento', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Instrumento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Instrumento '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'fabricante_id',
		'nome',
		'tag',
		'modelo',
	),
)); ?>