<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */
?>

<?php
$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Instrumento', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Instrumento') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>