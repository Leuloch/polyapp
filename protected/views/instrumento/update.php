<?php
/* @var $this InstrumentoController */
/* @var $model Instrumento */
?>

<?php
$this->breadcrumbs=array(
	'Instrumentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Instrumento', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Instrumento', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Instrumento', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Instrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Instrumento '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>