<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'order-items-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'ordem_id'); ?>
		<?php echo $form->dropDownList($model, 'ordem_id', GxHtml::listDataEx(Order::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'ordem_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'descricao'); ?>
		<?php echo $form->textArea($model, 'descricao'); ?>
		<?php echo $form->error($model,'descricao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'quantidade'); ?>
		<?php echo $form->textField($model, 'quantidade'); ?>
		<?php echo $form->error($model,'quantidade'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'instrumento_id'); ?>
		<?php echo $form->dropDownList($model, 'instrumento_id', GxHtml::listDataEx(Instrumento::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'instrumento_id'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->