<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'ordem_id'); ?>
		<?php echo $form->dropDownList($model, 'ordem_id', GxHtml::listDataEx(Order::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'descricao'); ?>
		<?php echo $form->textArea($model, 'descricao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'quantidade'); ?>
		<?php echo $form->textField($model, 'quantidade'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'instrumento_id'); ?>
		<?php echo $form->dropDownList($model, 'instrumento_id', GxHtml::listDataEx(Instrumento::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
