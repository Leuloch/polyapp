<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('ordem_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->ordem)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('descricao')); ?>:
	<?php echo GxHtml::encode($data->descricao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('quantidade')); ?>:
	<?php echo GxHtml::encode($data->quantidade); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('instrumento_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->instrumento)); ?>
	<br />

</div>