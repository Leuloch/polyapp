<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_criacao'); ?>
		<?php echo $form->textField($model, 'data_criacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_atualizacao'); ?>
		<?php echo $form->textField($model, 'data_atualizacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usuario_criacao'); ?>
		<?php echo $form->textField($model, 'usuario_criacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'usuario_atualizacao'); ?>
		<?php echo $form->textField($model, 'usuario_atualizacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cliente_id'); ?>
		<?php echo $form->textField($model, 'cliente_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'contato_id'); ?>
		<?php echo $form->textField($model, 'contato_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'observacao'); ?>
		<?php echo $form->textArea($model, 'observacao'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'forma_entrada'); ?>
		<?php echo $form->textField($model, 'forma_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'nota_fiscal'); ?>
		<?php echo $form->textField($model, 'nota_fiscal', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_entrada'); ?>
		<?php echo $form->textField($model, 'data_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_prev_saida'); ?>
		<?php echo $form->textField($model, 'data_prev_saida'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_disponibilidade'); ?>
		<?php echo $form->textField($model, 'data_disponibilidade'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'data_retirada'); ?>
		<?php echo $form->textField($model, 'data_retirada'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
