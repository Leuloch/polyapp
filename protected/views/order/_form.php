<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'order-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'data_criacao'); ?>
		<?php echo $form->textField($model, 'data_criacao'); ?>
		<?php echo $form->error($model,'data_criacao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_atualizacao'); ?>
		<?php echo $form->textField($model, 'data_atualizacao'); ?>
		<?php echo $form->error($model,'data_atualizacao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'usuario_criacao'); ?>
		<?php echo $form->textField($model, 'usuario_criacao'); ?>
		<?php echo $form->error($model,'usuario_criacao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'usuario_atualizacao'); ?>
		<?php echo $form->textField($model, 'usuario_atualizacao'); ?>
		<?php echo $form->error($model,'usuario_atualizacao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'cliente_id'); ?>
		<?php echo $form->textField($model, 'cliente_id'); ?>
		<?php echo $form->error($model,'cliente_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contato_id'); ?>
		<?php echo $form->textField($model, 'contato_id'); ?>
		<?php echo $form->error($model,'contato_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'observacao'); ?>
		<?php echo $form->textArea($model, 'observacao'); ?>
		<?php echo $form->error($model,'observacao'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'forma_entrada'); ?>
		<?php echo $form->textField($model, 'forma_entrada'); ?>
		<?php echo $form->error($model,'forma_entrada'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nota_fiscal'); ?>
		<?php echo $form->textField($model, 'nota_fiscal', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'nota_fiscal'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_entrada'); ?>
		<?php echo $form->textField($model, 'data_entrada'); ?>
		<?php echo $form->error($model,'data_entrada'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_prev_saida'); ?>
		<?php echo $form->textField($model, 'data_prev_saida'); ?>
		<?php echo $form->error($model,'data_prev_saida'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_disponibilidade'); ?>
		<?php echo $form->textField($model, 'data_disponibilidade'); ?>
		<?php echo $form->error($model,'data_disponibilidade'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'data_retirada'); ?>
		<?php echo $form->textField($model, 'data_retirada'); ?>
		<?php echo $form->error($model,'data_retirada'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('eventos')); ?></label>
		<?php echo $form->checkBoxList($model, 'eventos', GxHtml::encodeEx(GxHtml::listDataEx(Evento::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('orderItems')); ?></label>
		<?php echo $form->checkBoxList($model, 'orderItems', GxHtml::encodeEx(GxHtml::listDataEx(OrderItems::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->