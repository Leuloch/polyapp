<?php
/* @var $this ConversaoController */
/* @var $data Conversao */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidade_id_de')); ?>:</b>
	<?php echo CHtml::encode($data->unidade_id_de); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidade_id_para')); ?>:</b>
	<?php echo CHtml::encode($data->unidade_id_para); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fator_conversao')); ?>:</b>
	<?php echo CHtml::encode($data->fator_conversao); ?>
	<br />


</div>