<?php
/* @var $this ConversaoController */
/* @var $model Conversao */
?>

<?php
$this->breadcrumbs=array(
	'Conversaos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Conversao', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Conversao', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Conversao', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Conversao', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Conversao '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>