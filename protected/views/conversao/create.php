<?php
/* @var $this ConversaoController */
/* @var $model Conversao */
?>

<?php
$this->breadcrumbs=array(
	'Conversaos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Conversao', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Conversao', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Conversao') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>