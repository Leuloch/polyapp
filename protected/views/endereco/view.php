<?php
/* @var $this EnderecoController */
/* @var $model Endereco */
?>

<?php
$this->breadcrumbs=array(
	'Enderecos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Endereco', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Endereco', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Endereco', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Endereco', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Endereco', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Endereco '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'endereco',
		'empresa_id',
		'numero',
		'complemento',
		'bairro',
		'cidade',
		'estado',
		'cep',
		'tipo_endereco_faturamento',
		'tipo_endereco_entrega',
		'tipo_endereco_cobranca',
	),
)); ?>