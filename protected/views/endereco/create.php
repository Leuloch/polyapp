<?php
/* @var $this EnderecoController */
/* @var $model Endereco */
?>

<?php
$this->breadcrumbs=array(
	'Enderecos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Endereco', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Endereco', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Endereco') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>