<?php
/* @var $this EnderecoController */
/* @var $model Endereco */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'endereco-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'endereco',array('maxlength'=>150)); ?>
    <?php echo $form->textFieldControlGroup($model,'empresa_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'numero',array('maxlength'=>20)); ?>
    <?php echo $form->textFieldControlGroup($model,'complemento',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'bairro',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'cidade',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'estado',array('maxlength'=>2)); ?>
    <?php echo $form->textFieldControlGroup($model,'cep'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_endereco_faturamento'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_endereco_entrega'); ?>
    <?php echo $form->textFieldControlGroup($model,'tipo_endereco_cobranca'); ?>

    <?php echo BsHtml::submitButton('Submit', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
