<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('group_id')); ?>:
	<?php echo GxHtml::encode($data->group_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nome')); ?>:
	<?php echo GxHtml::encode($data->nome); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('descricao')); ?>:
	<?php echo GxHtml::encode($data->descricao); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('url')); ?>:
	<?php echo GxHtml::encode($data->url); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('icon')); ?>:
	<?php echo GxHtml::encode($data->icon); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('allday')); ?>:
	<?php echo GxHtml::encode($data->allday); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('tipo')); ?>:
	<?php echo GxHtml::encode($data->tipo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('color')); ?>:
	<?php echo GxHtml::encode($data->color); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('date_start')); ?>:
	<?php echo GxHtml::encode($data->date_start); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('date_end')); ?>:
	<?php echo GxHtml::encode($data->date_end); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('order_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->order)); ?>
	<br />
	*/ ?>

</div>