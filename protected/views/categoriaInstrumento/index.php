<?php
/* @var $this CategoriaInstrumentoController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Categoria Instrumentos',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create CategoriaInstrumento', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage CategoriaInstrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Categoria Instrumentos') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>