<?php
/* @var $this CategoriaInstrumentoController */
/* @var $model CategoriaInstrumento */
?>

<?php
$this->breadcrumbs=array(
	'Categoria Instrumentos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List CategoriaInstrumento', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create CategoriaInstrumento', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View CategoriaInstrumento', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage CategoriaInstrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','CategoriaInstrumento '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>