<?php
/* @var $this CategoriaInstrumentoController */
/* @var $model CategoriaInstrumento */
?>

<?php
$this->breadcrumbs=array(
	'Categoria Instrumentos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List CategoriaInstrumento', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage CategoriaInstrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','CategoriaInstrumento') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>