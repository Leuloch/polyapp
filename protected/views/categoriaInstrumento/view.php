<?php
/* @var $this CategoriaInstrumentoController */
/* @var $model CategoriaInstrumento */
?>

<?php
$this->breadcrumbs=array(
	'Categoria Instrumentos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List CategoriaInstrumento', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create CategoriaInstrumento', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update CategoriaInstrumento', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete CategoriaInstrumento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage CategoriaInstrumento', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','CategoriaInstrumento '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome',
		'descricao',
	),
)); ?>