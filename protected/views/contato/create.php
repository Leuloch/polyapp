<?php
/* @var $this ContatoController */
/* @var $model Contato */
?>

<?php
$this->breadcrumbs=array(
	'Contatos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Contato', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Contato', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Contato') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>