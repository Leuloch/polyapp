<?php
/* @var $this ContatoController */
/* @var $model Contato */
?>

<?php
$this->breadcrumbs=array(
	'Contatos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Contato', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Contato', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Contato', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Contato', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Contato', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Contato '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'data_criacao',
		'data_atualizacao',
		'usuario_criacao',
		'usuario_atualizacao',
		'nome',
		'cargo',
		'setor',
		'ddd_tel_comercial',
		'tel_comercial',
		'tel_comercial_ramal',
		'ddd_tel_outro',
		'tel_outro',
		'tel_outro_ramal',
		'ddd_cel_comercial',
		'cel_comercial',
		'ddd_cel_outro',
		'cel_outro',
		'email',
		'email_outro',
		'contato_administrativo',
		'contato_comercial',
		'contato_financeiro',
		'contato_tecnico',
		'empresa_id',
	),
)); ?>