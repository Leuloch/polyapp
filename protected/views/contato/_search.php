<?php
/* @var $this ContatoController */
/* @var $model Contato */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'data_criacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'data_atualizacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'usuario_criacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'usuario_atualizacao'); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'cargo',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'setor',array('maxlength'=>50)); ?>
    <?php echo $form->textFieldControlGroup($model,'ddd_tel_comercial'); ?>
    <?php echo $form->textFieldControlGroup($model,'tel_comercial',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'tel_comercial_ramal'); ?>
    <?php echo $form->textFieldControlGroup($model,'ddd_tel_outro'); ?>
    <?php echo $form->textFieldControlGroup($model,'tel_outro',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'tel_outro_ramal'); ?>
    <?php echo $form->textFieldControlGroup($model,'ddd_cel_comercial'); ?>
    <?php echo $form->textFieldControlGroup($model,'cel_comercial',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'ddd_cel_outro'); ?>
    <?php echo $form->textFieldControlGroup($model,'cel_outro',array('maxlength'=>14)); ?>
    <?php echo $form->textFieldControlGroup($model,'email',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'email_outro',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'contato_administrativo'); ?>
    <?php echo $form->textFieldControlGroup($model,'contato_comercial'); ?>
    <?php echo $form->textFieldControlGroup($model,'contato_financeiro'); ?>
    <?php echo $form->textFieldControlGroup($model,'contato_tecnico'); ?>
    <?php echo $form->textFieldControlGroup($model,'empresa_id'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
