<?php
/* @var $this UnidadeController */
/* @var $model Unidade */
?>

<?php
$this->breadcrumbs=array(
	'Unidades'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Unidade', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Unidade', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Unidade', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Unidade', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Unidade', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Unidade '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'simbolo',
		'nome',
		'grandeza_id',
	),
)); ?>