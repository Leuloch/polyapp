<?php
/* @var $this UnidadeController */
/* @var $model Unidade */
?>

<?php
$this->breadcrumbs=array(
	'Unidades'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Unidade', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Unidade', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Unidade') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>