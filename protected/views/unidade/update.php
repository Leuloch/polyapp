<?php
/* @var $this UnidadeController */
/* @var $model Unidade */
?>

<?php
$this->breadcrumbs=array(
	'Unidades'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Unidade', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Unidade', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Unidade', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Unidade', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Unidade '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>