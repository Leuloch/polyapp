<?php
/* @var $this UnidadeController */
/* @var $model Unidade */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'id'); ?>
    <?php echo $form->textFieldControlGroup($model,'simbolo',array('maxlength'=>5)); ?>
    <?php echo $form->textFieldControlGroup($model,'nome',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'grandeza_id'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
