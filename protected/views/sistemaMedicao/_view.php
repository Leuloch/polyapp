<?php
/* @var $this SistemaMedicaoController */
/* @var $data SistemaMedicao */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_sistema')); ?>:</b>
	<?php echo CHtml::encode($data->nome_sistema); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('escala_min')); ?>:</b>
	<?php echo CHtml::encode($data->escala_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('escala_max')); ?>:</b>
	<?php echo CHtml::encode($data->escala_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidade_escala_id')); ?>:</b>
	<?php echo CHtml::encode($data->unidade_escala_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('menor_divisao')); ?>:</b>
	<?php echo CHtml::encode($data->menor_divisao); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidade_div_id')); ?>:</b>
	<?php echo CHtml::encode($data->unidade_div_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('instrumento_id')); ?>:</b>
	<?php echo CHtml::encode($data->instrumento_id); ?>
	<br />

	*/ ?>

</div>