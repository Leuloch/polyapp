<?php
/* @var $this SistemaMedicaoController */
/* @var $model SistemaMedicao */
?>

<?php
$this->breadcrumbs=array(
	'Sistema Medicaos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List SistemaMedicao', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create SistemaMedicao', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View SistemaMedicao', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage SistemaMedicao', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','SistemaMedicao '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>