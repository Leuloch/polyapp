<?php
/* @var $this SistemaMedicaoController */
/* @var $model SistemaMedicao */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'sistema-medicao-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'nome_sistema',array('maxlength'=>100)); ?>
    <?php echo $form->textFieldControlGroup($model,'escala_min',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'escala_max',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'unidade_escala_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'menor_divisao',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'unidade_div_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'instrumento_id'); ?>

    <?php echo BsHtml::submitButton('Submit', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
