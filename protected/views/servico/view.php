<?php
/* @var $this ServicoController */
/* @var $model Servico */
?>

<?php
$this->breadcrumbs=array(
	'Servicos'=>array('index'),
	$model->id,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Servico', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Servico', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Servico', 'url'=>array('update', 'id'=>$model->id)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Servico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Servico', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Servico '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nome_servico',
		'instrumento_id',
		'custo',
		'valor',
	),
)); ?>