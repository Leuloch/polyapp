<?php
/* @var $this ServicoController */
/* @var $model Servico */
?>

<?php
$this->breadcrumbs=array(
	'Servicos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Servico', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Servico', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Servico', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Servico', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Servico '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>