<?php
/* @var $this ServicoController */
/* @var $model Servico */
?>

<?php
$this->breadcrumbs=array(
	'Servicos'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Servico', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Servico', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Servico') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>