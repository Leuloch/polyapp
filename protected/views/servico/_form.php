<?php
/* @var $this ServicoController */
/* @var $model Servico */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'servico-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model,'nome_servico',array('maxlength'=>255)); ?>
    <?php echo $form->textFieldControlGroup($model,'instrumento_id'); ?>
    <?php echo $form->textFieldControlGroup($model,'custo',array('maxlength'=>6)); ?>
    <?php echo $form->textFieldControlGroup($model,'valor',array('maxlength'=>6)); ?>

    <?php echo BsHtml::submitButton('Submit', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
