<?php
/* @var $this ServicoController */
/* @var $data Servico */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_servico')); ?>:</b>
	<?php echo CHtml::encode($data->nome_servico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instrumento_id')); ?>:</b>
	<?php echo CHtml::encode($data->instrumento_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custo')); ?>:</b>
	<?php echo CHtml::encode($data->custo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor')); ?>:</b>
	<?php echo CHtml::encode($data->valor); ?>
	<br />


</div>