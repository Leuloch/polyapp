<?php
/* @var $this ConfigCertificadoController */
/* @var $model ConfigCertificado */
?>

<?php
$this->breadcrumbs=array(
	'Config Certificados'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List ConfigCertificado', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create ConfigCertificado', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View ConfigCertificado', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage ConfigCertificado', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','ConfigCertificado '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>