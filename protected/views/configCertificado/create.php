<?php
/* @var $this ConfigCertificadoController */
/* @var $model ConfigCertificado */
?>

<?php
$this->breadcrumbs=array(
	'Config Certificados'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List ConfigCertificado', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage ConfigCertificado', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','ConfigCertificado') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>