<?php
/* @var $this FabricanteController */
/* @var $model Fabricante */
?>

<?php
$this->breadcrumbs=array(
	'Fabricantes'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Fabricante', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Fabricante', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Fabricante') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>