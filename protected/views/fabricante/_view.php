<?php
/* @var $this FabricanteController */
/* @var $data Fabricante */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nome_fabricante')); ?>:</b>
	<?php echo CHtml::encode($data->nome_fabricante); ?>
	<br />


</div>