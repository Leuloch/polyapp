<?php
/* @var $this FabricanteController */
/* @var $model Fabricante */
?>

<?php
$this->breadcrumbs=array(
	'Fabricantes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Fabricante', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Fabricante', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Fabricante', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Fabricante', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Fabricante '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>