<?php
/* @var $this GrandezaController */
/* @var $model Grandeza */
?>

<?php
$this->breadcrumbs=array(
	'Grandezas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Grandeza', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Grandeza', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Grandeza', 'url'=>array('view', 'id'=>$model->id)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Grandeza', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Grandeza '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>