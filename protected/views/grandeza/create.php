<?php
/* @var $this GrandezaController */
/* @var $model Grandeza */
?>

<?php
$this->breadcrumbs=array(
	'Grandezas'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Grandeza', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Grandeza', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Grandeza') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>