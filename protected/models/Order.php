<?php

Yii::import('application.models._base.BaseOrder');

class Order extends BaseOrder
{
        const ENTRADA_CORREIO = 0;
        const ENTRADA_MOTOBOY = 1;
        const ENTRADA_TRANSPORTADORA = 2;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        /**
         * Retorna o meio de entrada dos instrumentos
         * @return array Opções para o meio de entrada dos instrumentos
         */
        public function getFormaEntrada() {
            return array(
                self::ENTRADA_CORREIO => 'Correio',
                self::ENTRADA_MOTOBOY => 'Motoboy',
                self::ENTRADA_TRANSPORTADORA => 'Transportadora'
            );
        }
        
        /**
         * Prepare usuario_criacao and usuario_atualizacao attributes
         * before saving.
         */
        protected function beforeSave()
        {
            if(null !== Yii::app()->user)
                $id=Yii::app()->user->id;
            else
                $id=1;
            
            if ($this->isNewRecord)
                $this->usuario_criacao=$id;
            
            $this->usuario_atualizacao=$id;
            
            return parent::beforeSave();
        }
        
        /**
         * Attaches the timestamp behavior to update the
         * create and update times
         */
        public function behaviors()
        {
            return array(
                'ESaveRelatedBehavior' => array(
                    'class'=>'application.components.ESaveRelatedBehavior'
                ),
                'CTimestampBehavior' => array(
                    'class'=>'zii.behaviors.CTimestampBehavior',
                    'createAttribute'=>'data_criacao',
                    'updateAttribute'=>'data_atualizacao',
                    'setUpdateOnCreate'=> true,
                )
            );
        }
        
        public function gridOrderItems()
        {
            $model = new OrderItems();
            return new CActiveDataProvider ($model, array(
                'criteria'=>array(
                    'condition'=>"ordem_id=$this->id"
                )
            ));
        }
        
        public function lastEntered()
        {
            return new CActiveDataProvider($this, array(
                'criteria'=>array(
                    'order'=>'data_criacao DESC',
                    'limit'=>5
                ),
                'pagination'=>false
            ));
            
        }
}