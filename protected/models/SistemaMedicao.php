<?php

/**
 * This is the model class for table "tbl_sistema_medicao".
 *
 * The followings are the available columns in table 'tbl_sistema_medicao':
 * @property integer $id
 * @property string $nome_sistema
 * @property string $escala_min
 * @property string $escala_max
 * @property integer $unidade_escala_id
 * @property string $menor_divisao
 * @property integer $unidade_div_id
 * @property integer $instrumento_id
 *
 * The followings are the available model relations:
 * @property Instrumento $instrumento
 * @property Unidade $unidadeEscala
 * @property Unidade $unidadeDiv
 */
class SistemaMedicao extends CActiveRecord
{
        /**
         *
         * @var string The type used by Dynamic Tabular Input extension 
         */
        public $updateType;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_sistema_medicao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome_sistema, unidade_escala_id, unidade_div_id', 'required'),
			array('unidade_escala_id, unidade_div_id, instrumento_id', 'numerical', 'integerOnly'=>true),
			array('nome_sistema', 'length', 'max'=>100),
			array('escala_min, escala_max, menor_divisao', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome_sistema, escala_min, escala_max, unidade_escala_id, menor_divisao, unidade_div_id, instrumento_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'instrumento' => array(self::BELONGS_TO, 'Instrumento', 'instrumento_id'),
			'unidadeEscala' => array(self::BELONGS_TO, 'Unidade', 'unidade_escala_id'),
			'unidadeDiv' => array(self::BELONGS_TO, 'Unidade', 'unidade_div_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome_sistema' => 'Nome Sistema',
			'escala_min' => 'Escala Min',
			'escala_max' => 'Escala Max',
			'unidade_escala_id' => 'Unidade Escala',
			'menor_divisao' => 'Menor Divisao',
			'unidade_div_id' => 'Unidade Div',
			'instrumento_id' => 'Instrumento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('instrumento', 'unidadeEscala');

		$criteria->compare('id',$this->id);
		$criteria->compare('nome_sistema',$this->nome_sistema,true);
		$criteria->compare('escala_min',$this->escala_min,true);
		$criteria->compare('escala_max',$this->escala_max,true);
		$criteria->compare('unidadeEscala.simbolo',$this->unidade_escala_id, true);
		$criteria->compare('menor_divisao',$this->menor_divisao,true);
		$criteria->compare('unidade_div_id',$this->unidade_div_id);
		$criteria->compare('instrumento.nome',$this->instrumento_id, true);
//                $criteria->compare('instrumento.modelo',$this->instrumento_id, true);
//                $criteria->compare('instrumento.fabricante.nome_fabricante',$this->instrumento_id, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SistemaMedicao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
