<?php

/**
 * This is the model class for table "tbl_impostos_servico".
 *
 * The followings are the available columns in table 'tbl_impostos_servico':
 * @property integer $id
 * @property integer $tipo_imposto_retido
 * @property string $desconto_percentual
 * @property integer $orcamento_servico_id
 *
 * The followings are the available model relations:
 * @property OrcamentoServico $orcamentoServico
 */
class ImpostosServico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_impostos_servico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orcamento_servico_id', 'required'),
			array('tipo_imposto_retido, orcamento_servico_id', 'numerical', 'integerOnly'=>true),
			array('desconto_percentual', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo_imposto_retido, desconto_percentual, orcamento_servico_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orcamentoServico' => array(self::BELONGS_TO, 'OrcamentoServico', 'orcamento_servico_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo_imposto_retido' => 'Tipo Imposto Retido',
			'desconto_percentual' => 'Desconto Percentual',
			'orcamento_servico_id' => 'Orcamento Servico',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo_imposto_retido',$this->tipo_imposto_retido);
		$criteria->compare('desconto_percentual',$this->desconto_percentual,true);
		$criteria->compare('orcamento_servico_id',$this->orcamento_servico_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ImpostosServico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
