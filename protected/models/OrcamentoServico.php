<?php

/**
 * This is the model class for table "tbl_orcamento_servico".
 *
 * The followings are the available columns in table 'tbl_orcamento_servico':
 * @property integer $id
 * @property string $data_criacao
 * @property string $data_atualizacao
 * @property integer $usuario_criacao
 * @property integer $usuario_atualizacao
 * @property string $data_emissao
 * @property integer $contato_id
 * @property string $previsao_entrega
 * @property integer $emitir_boleto
 * @property integer $condicao_pagamento
 * @property string $condicao_pagamento_texto
 * @property string $historico_negociacao
 * @property string $validade_proposta
 * @property string $observacao
 * @property integer $forma_cotacao
 * @property string $num_revisao
 * @property integer $tipo_frete
 * @property string $desconto
 * @property integer $conta_bancaria_id
 * @property integer $empresa_id
 *
 * The followings are the available model relations:
 * @property ImpostosServico[] $impostosServicos
 * @property Empresa $empresa
 */
class OrcamentoServico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_orcamento_servico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_emissao, empresa_id', 'required'),
			array('usuario_criacao, usuario_atualizacao, contato_id, emitir_boleto, condicao_pagamento, forma_cotacao, tipo_frete, conta_bancaria_id, empresa_id', 'numerical', 'integerOnly'=>true),
			array('previsao_entrega, validade_proposta, observacao', 'length', 'max'=>45),
			array('num_revisao, desconto', 'length', 'max'=>10),
			array('data_criacao, data_atualizacao, condicao_pagamento_texto, historico_negociacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_criacao, data_atualizacao, usuario_criacao, usuario_atualizacao, data_emissao, contato_id, previsao_entrega, emitir_boleto, condicao_pagamento, condicao_pagamento_texto, historico_negociacao, validade_proposta, observacao, forma_cotacao, num_revisao, tipo_frete, desconto, conta_bancaria_id, empresa_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'impostosServicos' => array(self::HAS_MANY, 'ImpostosServico', 'orcamento_servico_id'),
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'empresa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_criacao' => 'Data Criacao',
			'data_atualizacao' => 'Data Atualizacao',
			'usuario_criacao' => 'Usuario Criacao',
			'usuario_atualizacao' => 'Usuario Atualizacao',
			'data_emissao' => 'Data Emissao',
			'contato_id' => 'Contato',
			'previsao_entrega' => 'Previsao Entrega',
			'emitir_boleto' => 'Emitir Boleto',
			'condicao_pagamento' => 'Condicao Pagamento',
			'condicao_pagamento_texto' => 'Condicao Pagamento Texto',
			'historico_negociacao' => 'Historico Negociacao',
			'validade_proposta' => 'Validade Proposta',
			'observacao' => 'Observacao',
			'forma_cotacao' => 'Forma Cotacao',
			'num_revisao' => 'Num Revisao',
			'tipo_frete' => 'Tipo Frete',
			'desconto' => 'Desconto',
			'conta_bancaria_id' => 'Conta Bancaria',
			'empresa_id' => 'Empresa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('data_atualizacao',$this->data_atualizacao,true);
		$criteria->compare('usuario_criacao',$this->usuario_criacao);
		$criteria->compare('usuario_atualizacao',$this->usuario_atualizacao);
		$criteria->compare('data_emissao',$this->data_emissao,true);
		$criteria->compare('contato_id',$this->contato_id);
		$criteria->compare('previsao_entrega',$this->previsao_entrega,true);
		$criteria->compare('emitir_boleto',$this->emitir_boleto);
		$criteria->compare('condicao_pagamento',$this->condicao_pagamento);
		$criteria->compare('condicao_pagamento_texto',$this->condicao_pagamento_texto,true);
		$criteria->compare('historico_negociacao',$this->historico_negociacao,true);
		$criteria->compare('validade_proposta',$this->validade_proposta,true);
		$criteria->compare('observacao',$this->observacao,true);
		$criteria->compare('forma_cotacao',$this->forma_cotacao);
		$criteria->compare('num_revisao',$this->num_revisao,true);
		$criteria->compare('tipo_frete',$this->tipo_frete);
		$criteria->compare('desconto',$this->desconto,true);
		$criteria->compare('conta_bancaria_id',$this->conta_bancaria_id);
		$criteria->compare('empresa_id',$this->empresa_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrcamentoServico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
