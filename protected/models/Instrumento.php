<?php

/**
 * This is the model class for table "tbl_instrumento".
 *
 * The followings are the available columns in table 'tbl_instrumento':
 * @property integer $id
 * @property integer $fabricante_id
 * @property string $nome
 * @property string $tag
 * @property string $modelo
 * @property integer $categoria_instrumento_id
 *
 * The followings are the available model relations:
 * @property CategoriaInstrumento $categoriaInstrumento
 * @property Fabricante $fabricante
 * @property Produto[] $produtos
 * @property Servico[] $servicos
 * @property SistemaMedicao[] $sistemaMedicaos
 */
class Instrumento extends CActiveRecord
{       
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_instrumento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, fabricante_id, categoria_instrumento_id', 'required'),
			array('fabricante_id, categoria_instrumento_id', 'numerical', 'integerOnly'=>true),
			array('nome', 'length', 'max'=>100),
			array('modelo', 'length', 'max'=>50),
			array('tag', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fabricante_id, nome, tag, modelo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'categoriaInstrumento' => array(self::BELONGS_TO, 'CategoriaInstrumento', 'categoria_instrumento_id'),
			'fabricante' => array(self::BELONGS_TO, 'Fabricante', 'fabricante_id'),
			'produtos' => array(self::HAS_MANY, 'Produto', 'instrumento_id'),
			'servicos' => array(self::HAS_MANY, 'Servico', 'instrumento_id'),
			'sistemaMedicoes' => array(self::HAS_MANY, 'SistemaMedicao', 'instrumento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fabricante_id' => 'Fabricante',
			'nome' => 'Nome',
			'tag' => 'Tag',
			'modelo' => 'Modelo',
                        'categoria_instrumento_id' => 'Categoria Instrumento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('fabricante','sistemaMedicoes', 'categoriaInstrumento');

		$criteria->compare('id',$this->id);
		$criteria->compare('fabricante.nome_fabricante',$this->fabricante_id, true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('tag',$this->tag,true);
		$criteria->compare('modelo',$this->modelo,true);
                $criteria->compare('categoriaInstrumento.nome',$this->categoria_instrumento_id, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Instrumento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function listData()
        {
            $result = '';
            $sistemasMedicao = $this->sistemaMedicoes;
            foreach ($sistemasMedicao as $sistemaMedicao) {
                // Define the Escala string if exist
                $escala = '';
                if ($sistemaMedicao->escala_min || $sistemaMedicao->escala_max || $sistemaMedicao->unidade_escala_id) {
                    if((int)$sistemaMedicao->escala_min == 0) {
                        $escala = " (" . $sistemaMedicao->escala_max . " " . $sistemaMedicao->unidadeEscala->simbolo . ")";
                    } else {
                        $escala = " (" . $sistemaMedicao->escala_min . " à " . $sistemaMedicao->escala_max . " " . $sistemaMedicao->unidadeEscala->simbolo . ")";
                    }
                }
                $result = $result . ' ' . CHtml::tag('li', array('class'=>'text-primary'),'<strong><u>' . $sistemaMedicao->nome_sistema . '</u>'. $escala .'</strong>',true);
            }
            return CHtml::tag('ul', array('class'=>'list-inline'), $result, true);
        }
}
