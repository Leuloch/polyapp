<?php

/**
 * This is the model class for table "tbl_empresa".
 *
 * The followings are the available columns in table 'tbl_empresa':
 * @property integer $id
 * @property string $data_criacao
 * @property string $data_atualizacao
 * @property integer $usuario_criacao
 * @property integer $usuario_atualizacao
 * @property string $nome
 * @property integer $cliente
 * @property integer $fornecedor
 * @property integer $num_cadastro
 * @property string $razao_social
 * @property integer $tipo_pessoa
 * @property string $cnpj_cpf
 * @property integer $ddd_tel_comercial
 * @property string $telefone_comercial
 * @property integer $ddd_celular
 * @property string $celular
 * @property string $email
 * @property string $website
 * @property string $inscricao_estadual
 * @property integer $isencao_ie
 * @property string $inscricao_municipal
 * @property string $inscricao_suframa
 * @property integer $opta_simples
 * @property integer $isencao_icms
 * @property string $observacao
 *
 * The followings are the available model relations:
 * @property ConfigCertificado[] $configCertificados
 * @property Endereco[] $enderecos
 */
class Empresa extends CActiveRecord
{
    
        const PESSOA_JURIDICA = 0;
        const PESSOA_FISICA = 1;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_empresa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome', 'required'),
			array('usuario_criacao, usuario_atualizacao, cliente, fornecedor, num_cadastro, tipo_pessoa, ddd_tel_comercial, ddd_celular, isencao_ie, opta_simples, isencao_icms', 'numerical', 'integerOnly'=>true),
			array('nome, razao_social', 'length', 'max'=>255),
			array('cnpj_cpf', 'length', 'max'=>15),
			array('inscricao_estadual', 'length', 'max'=>14),
                        array('telefone_comercial', 'length', 'max'=>8),
                        array('celular', 'length', 'max'=>9),
                        array('telefone_comercial, celular', 'numerical'),
			array('email, website', 'length', 'max'=>100),
                        array('email', 'email'),
			array('inscricao_municipal', 'length', 'max'=>18),
			array('inscricao_suframa', 'length', 'max'=>9),
			array('data_criacao, data_atualizacao, observacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_criacao, data_atualizacao, usuario_criacao, usuario_atualizacao, nome, cliente, fornecedor, num_cadastro, razao_social, tipo_pessoa, cnpj_cpf, ddd_tel_comercial, telefone_comercial, ddd_celular, celular, email, website, inscricao_estadual, isencao_ie, inscricao_municipal, inscricao_suframa, opta_simples, isencao_icms, observacao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'configCertificados' => array(self::HAS_MANY, 'ConfigCertificado', 'empresa_id'),
			'enderecos' => array(self::HAS_MANY, 'Endereco', 'empresa_id'),
                        'contatos' => array(self::HAS_MANY, 'Contato', 'empresa_id'),
                        'tblProdutos' => array(self::MANY_MANY, 'Produto', 'tbl_fornecedor_produto(empresa_id, produto_id)'),
			'orcamentoServicos' => array(self::HAS_MANY, 'OrcamentoServico', 'empresa_id'),
			'tblProdutos1' => array(self::MANY_MANY, 'Produto', 'tbl_produto_cliente_preco(empresa_id, produto_id)'),
			'tblServicos' => array(self::MANY_MANY, 'Servico', 'tbl_servico_empresa_preco(empresa_id, servico_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_criacao' => 'Data Criação',
			'data_atualizacao' => 'Data Atualização',
			'usuario_criacao' => 'Usuario Criação',
			'usuario_atualizacao' => 'Usuario Atualização',
			'nome' => 'Nome',
			'cliente' => 'Cliente',
			'fornecedor' => 'Fornecedor',
			'num_cadastro' => 'NCC',
			'razao_social' => 'Razão Social',
			'tipo_pessoa' => 'Tipo Pessoa',
			'cnpj_cpf' => 'Cnpj ou Cpf',
			'ddd_tel_comercial' => 'DDD',
			'telefone_comercial' => 'Telefone',
			'ddd_celular' => 'DDD',
			'celular' => 'Celular',
			'email' => 'email',
			'website' => 'website',
			'inscricao_estadual' => 'Inscrição Estadual',
			'isencao_ie' => 'Isento',
			'inscricao_municipal' => 'Inscrição Municipal',
			'inscricao_suframa' => 'Inscrição Suframa',
			'opta_simples' => 'Opta pelo Simples',
			'isencao_icms' => 'Isento Icms',
			'observacao' => 'Observação',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('data_atualizacao',$this->data_atualizacao,true);
		$criteria->compare('usuario_criacao',$this->usuario_criacao);
		$criteria->compare('usuario_atualizacao',$this->usuario_atualizacao);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('cliente',$this->cliente);
		$criteria->compare('fornecedor',$this->fornecedor);
		$criteria->compare('num_cadastro',$this->num_cadastro);
		$criteria->compare('razao_social',$this->razao_social,true);
		$criteria->compare('tipo_pessoa',$this->tipo_pessoa);
		$criteria->compare('cnpj_cpf',$this->cnpj_cpf,true);
		$criteria->compare('ddd_tel_comercial',$this->ddd_tel_comercial);
		$criteria->compare('telefone_comercial',$this->telefone_comercial,true);
		$criteria->compare('ddd_celular',$this->ddd_celular);
		$criteria->compare('celular',$this->celular,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('inscricao_estadual',$this->inscricao_estadual,true);
		$criteria->compare('isencao_ie',$this->isencao_ie);
		$criteria->compare('inscricao_municipal',$this->inscricao_municipal,true);
		$criteria->compare('inscricao_suframa',$this->inscricao_suframa,true);
		$criteria->compare('opta_simples',$this->opta_simples);
		$criteria->compare('isencao_icms',$this->isencao_icms);
		$criteria->compare('observacao',$this->observacao,true);

		return new CActiveDataProvider($this, array(
                        'pagination'=>array(
                            'pageSize'=>Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                        ),
			'criteria'=>$criteria,
		));
	}
        
        public function lastEntered()
        {
            return new CActiveDataProvider($this, array(
                'criteria'=>array(
                    'order'=>'data_criacao DESC',
                    'limit'=>5
                ),
                'pagination'=>false
            ));
            
        }
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Empresa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * Retorna o tipo de pessoa para cadastro de clientes/fornecedores
         * @return array tipo de pessoa fisica ou juridica
         */
        public function getPessoaOptions() {
            return array(
                self::PESSOA_JURIDICA => 'Jurídica',
                self::PESSOA_FISICA => 'Física'
            );
        }
        
        /**
         * Prepare usuario_criacao and usuario_atualizacao attributes
         * before saving.
         */
        protected function beforeSave()
        {
            if(null !== Yii::app()->user)
                $id=Yii::app()->user->id;
            else
                $id=1;
            
            if ($this->isNewRecord)
                $this->usuario_criacao=$id;
            
            $this->usuario_atualizacao=$id;
            
            return parent::beforeSave();
        }
        
        /**
         * Attaches the timestamp behavior to update the
         * create and update times
         */
        public function behaviors()
        {
            return array(
                'ESaveRelatedBehavior' => array(
                    'class'=>'application.components.ESaveRelatedBehavior'
                ),
                'CTimestampBehavior' => array(
                    'class'=>'zii.behaviors.CTimestampBehavior',
                    'createAttribute'=>'data_criacao',
                    'updateAttribute'=>'data_atualizacao',
                    'setUpdateOnCreate'=> true,
                )
            );
        }
}
