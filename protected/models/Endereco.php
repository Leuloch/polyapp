<?php

/**
 * This is the model class for table "tbl_endereco".
 *
 * The followings are the available columns in table 'tbl_endereco':
 * @property integer $id
 * @property string $endereco
 * @property integer $empresa_id
 * @property string $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property integer $cep
 * @property integer $tipo_endereco_faturamento
 * @property integer $tipo_endereco_entrega
 * @property integer $tipo_endereco_cobranca
 *
 * The followings are the available model relations:
 * @property Empresa $empresa
 */
class Endereco extends CActiveRecord
{
        /**
         *
         * @var attribute the valiable to use with de extension DynamicTabularInput 
         */
        public $updateType;
        
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_endereco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('endereco', 'required'),
			array('empresa_id, cep, tipo_endereco_faturamento, tipo_endereco_entrega, tipo_endereco_cobranca', 'numerical', 'integerOnly'=>true),
			array('endereco', 'length', 'max'=>150),
			array('numero', 'length', 'max'=>20),
			array('complemento', 'length', 'max'=>50),
			array('bairro, cidade', 'length', 'max'=>100),
			array('estado', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, endereco, empresa_id, numero, complemento, bairro, cidade, estado, cep, tipo_endereco_faturamento, tipo_endereco_entrega, tipo_endereco_cobranca', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'empresa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'endereco' => 'Endereco',
			'empresa_id' => 'Empresa',
			'numero' => 'Numero',
			'complemento' => 'Complemento',
			'bairro' => 'Bairro',
			'cidade' => 'Cidade',
			'estado' => 'Estado',
			'cep' => 'Cep',
			'tipo_endereco_faturamento' => 'Tipo Endereco Faturamento',
			'tipo_endereco_entrega' => 'Tipo Endereco Entrega',
			'tipo_endereco_cobranca' => 'Tipo Endereco Cobranca',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('endereco',$this->endereco,true);
		$criteria->compare('empresa_id',$this->empresa_id);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('complemento',$this->complemento,true);
		$criteria->compare('bairro',$this->bairro,true);
		$criteria->compare('cidade',$this->cidade,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('cep',$this->cep);
		$criteria->compare('tipo_endereco_faturamento',$this->tipo_endereco_faturamento);
		$criteria->compare('tipo_endereco_entrega',$this->tipo_endereco_entrega);
		$criteria->compare('tipo_endereco_cobranca',$this->tipo_endereco_cobranca);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Endereco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
