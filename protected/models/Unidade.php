<?php

/**
 * This is the model class for table "tbl_unidade".
 *
 * The followings are the available columns in table 'tbl_unidade':
 * @property integer $id
 * @property string $simbolo
 * @property string $nome
 * @property integer $grandeza_id
 *
 * The followings are the available model relations:
 * @property Conversao[] $conversaos
 * @property Conversao[] $conversaos1
 * @property SistemaMedicao[] $sistemaMedicaos
 * @property SistemaMedicao[] $sistemaMedicaos1
 * @property Grandeza $grandeza
 */
class Unidade extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_unidade';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('grandeza_id', 'numerical', 'integerOnly'=>true),
			array('simbolo', 'length', 'max'=>5),
			array('nome', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, simbolo, nome, grandeza_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'conversaos' => array(self::HAS_MANY, 'Conversao', 'unidade_id_de'),
			'conversaos1' => array(self::HAS_MANY, 'Conversao', 'unidade_id_para'),
			'sistemaMedicaos' => array(self::HAS_MANY, 'SistemaMedicao', 'unidade_escala_id'),
			'sistemaMedicaos1' => array(self::HAS_MANY, 'SistemaMedicao', 'unidade_div_id'),
			'grandeza' => array(self::BELONGS_TO, 'Grandeza', 'grandeza_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'simbolo' => 'Simbolo',
			'nome' => 'Nome',
			'grandeza_id' => 'Grandeza',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('simbolo',$this->simbolo,true);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('grandeza_id',$this->grandeza_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Unidade the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
