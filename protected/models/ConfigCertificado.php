<?php

/**
 * This is the model class for table "tbl_config_certificado".
 *
 * The followings are the available columns in table 'tbl_config_certificado':
 * @property integer $id
 * @property integer $empresa_id
 * @property string $nome_certificado
 * @property string $endereco
 * @property string $numero
 * @property string $cidade
 * @property string $estado
 * @property integer $cep
 * @property integer $validade
 * @property integer $aprovacao
 * @property integer $opta_digital
 *
 * The followings are the available model relations:
 * @property Empresa $empresa
 */
class ConfigCertificado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_config_certificado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('empresa_id, nome_certificado', 'required'),
			array('empresa_id, cep, validade, aprovacao, opta_digital', 'numerical', 'integerOnly'=>true),
			array('nome_certificado', 'length', 'max'=>255),
			array('endereco', 'length', 'max'=>270),
                        array('numero', 'length', 'max'=>20),
			array('cidade', 'length', 'max'=>100),
			array('estado', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, empresa_id, nome_certificado, endereco, numero, cidade, estado, cep, validade, aprovacao, opta_digital', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'empresa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'empresa_id' => 'Empresa',
			'nome_certificado' => 'Nome Certificado',
			'endereco' => 'Endereco',
                        'numero'=> 'Numero',
			'cidade' => 'Cidade',
			'estado' => 'Estado',
			'cep' => 'Cep',
			'validade' => 'Validade',
			'aprovacao' => 'Aprovacao',
			'opta_digital' => 'Opta Digital',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('empresa_id',$this->empresa_id);
		$criteria->compare('nome_certificado',$this->nome_certificado,true);
		$criteria->compare('endereco',$this->endereco,true);
                $criteria->compare('numero',$this->numero,true);
		$criteria->compare('cidade',$this->cidade,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('cep',$this->cep);
		$criteria->compare('validade',$this->validade);
		$criteria->compare('aprovacao',$this->aprovacao);
		$criteria->compare('opta_digital',$this->opta_digital);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConfigCertificado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
