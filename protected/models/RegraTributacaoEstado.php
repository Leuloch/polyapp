<?php

/**
 * This is the model class for table "tbl_regra_tributacao_estado".
 *
 * The followings are the available columns in table 'tbl_regra_tributacao_estado':
 * @property integer $id
 * @property integer $produto_id
 * @property integer $tipo_imposto
 * @property integer $cenario
 * @property integer $icms_situacao_tributaria
 * @property integer $icms_origem
 * @property integer $icms_mod_bc
 * @property string $icms_aliquota
 * @property string $icms_credito
 * @property integer $ipi_situacao_tributaria
 * @property integer $ipi_cod_enq
 * @property integer $pis_situacao_tributaria
 * @property string $pis_aliquota
 * @property integer $cofins_situacao_tributaria
 * @property string $cofins_aliquota
 * @property string $nf_informacoes
 * @property integer $regra_trib_empresa_id
 *
 * The followings are the available model relations:
 * @property Produto $produto
 */
class RegraTributacaoEstado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_regra_tributacao_estado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('produto_id, tipo_imposto, cenario, icms_situacao_tributaria, icms_origem, icms_mod_bc, ipi_situacao_tributaria, ipi_cod_enq, pis_situacao_tributaria, cofins_situacao_tributaria, regra_trib_empresa_id', 'numerical', 'integerOnly'=>true),
			array('icms_aliquota, icms_credito, pis_aliquota', 'length', 'max'=>6),
			array('cofins_aliquota', 'length', 'max'=>8),
			array('nf_informacoes', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, produto_id, tipo_imposto, cenario, icms_situacao_tributaria, icms_origem, icms_mod_bc, icms_aliquota, icms_credito, ipi_situacao_tributaria, ipi_cod_enq, pis_situacao_tributaria, pis_aliquota, cofins_situacao_tributaria, cofins_aliquota, nf_informacoes, regra_trib_empresa_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'produto' => array(self::BELONGS_TO, 'Produto', 'produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'produto_id' => 'Produto',
			'tipo_imposto' => 'Tipo Imposto',
			'cenario' => 'Cenario',
			'icms_situacao_tributaria' => 'Icms Situacao Tributaria',
			'icms_origem' => 'Icms Origem',
			'icms_mod_bc' => 'Icms Mod Bc',
			'icms_aliquota' => 'Icms Aliquota',
			'icms_credito' => 'Icms Credito',
			'ipi_situacao_tributaria' => 'Ipi Situacao Tributaria',
			'ipi_cod_enq' => 'Ipi Cod Enq',
			'pis_situacao_tributaria' => 'Pis Situacao Tributaria',
			'pis_aliquota' => 'Pis Aliquota',
			'cofins_situacao_tributaria' => 'Cofins Situacao Tributaria',
			'cofins_aliquota' => 'Cofins Aliquota',
			'nf_informacoes' => 'Nf Informacoes',
			'regra_trib_empresa_id' => 'Regra Trib Empresa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('produto_id',$this->produto_id);
		$criteria->compare('tipo_imposto',$this->tipo_imposto);
		$criteria->compare('cenario',$this->cenario);
		$criteria->compare('icms_situacao_tributaria',$this->icms_situacao_tributaria);
		$criteria->compare('icms_origem',$this->icms_origem);
		$criteria->compare('icms_mod_bc',$this->icms_mod_bc);
		$criteria->compare('icms_aliquota',$this->icms_aliquota,true);
		$criteria->compare('icms_credito',$this->icms_credito,true);
		$criteria->compare('ipi_situacao_tributaria',$this->ipi_situacao_tributaria);
		$criteria->compare('ipi_cod_enq',$this->ipi_cod_enq);
		$criteria->compare('pis_situacao_tributaria',$this->pis_situacao_tributaria);
		$criteria->compare('pis_aliquota',$this->pis_aliquota,true);
		$criteria->compare('cofins_situacao_tributaria',$this->cofins_situacao_tributaria);
		$criteria->compare('cofins_aliquota',$this->cofins_aliquota,true);
		$criteria->compare('nf_informacoes',$this->nf_informacoes,true);
		$criteria->compare('regra_trib_empresa_id',$this->regra_trib_empresa_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegraTributacaoEstado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
