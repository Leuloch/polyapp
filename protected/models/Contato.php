<?php

/**
 * This is the model class for table "tbl_contato".
 *
 * The followings are the available columns in table 'tbl_contato':
 * @property integer $id
 * @property string $data_criacao
 * @property string $data_atualizacao
 * @property integer $usuario_criacao
 * @property integer $usuario_atualizacao
 * @property string $nome
 * @property string $cargo
 * @property string $setor
 * @property integer $ddd_tel_comercial
 * @property string $tel_comercial
 * @property integer $tel_comercial_ramal
 * @property integer $ddd_tel_outro
 * @property string $tel_outro
 * @property integer $tel_outro_ramal
 * @property integer $ddd_cel_comercial
 * @property string $cel_comercial
 * @property integer $ddd_cel_outro
 * @property string $cel_outro
 * @property string $email
 * @property string $email_outro
 * @property integer $contato_administrativo
 * @property integer $contato_comercial
 * @property integer $contato_financeiro
 * @property integer $contato_tecnico
 * @property integer $empresa_id
 *
 * The followings are the available model relations:
 * @property Empresa $empresa
 */
class Contato extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_contato';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome', 'required'),
			array('usuario_criacao, usuario_atualizacao, ddd_tel_comercial, tel_comercial_ramal, ddd_tel_outro, tel_outro_ramal, ddd_cel_comercial, ddd_cel_outro, contato_administrativo, contato_comercial, contato_financeiro, contato_tecnico, empresa_id', 'numerical', 'integerOnly'=>true),
			array('nome, email, email_outro', 'length', 'max'=>100),
			array('cargo, setor', 'length', 'max'=>50),
			array('tel_comercial, tel_outro, cel_comercial, cel_outro', 'length', 'max'=>14),
			array('data_criacao, data_atualizacao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, data_criacao, data_atualizacao, usuario_criacao, usuario_atualizacao, nome, cargo, setor, ddd_tel_comercial, tel_comercial, tel_comercial_ramal, ddd_tel_outro, tel_outro, tel_outro_ramal, ddd_cel_comercial, cel_comercial, ddd_cel_outro, cel_outro, email, email_outro, contato_administrativo, contato_comercial, contato_financeiro, contato_tecnico, empresa_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'empresa' => array(self::BELONGS_TO, 'Empresa', 'empresa_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_criacao' => 'Data Criação',
			'data_atualizacao' => 'Data Atualização',
			'usuario_criacao' => 'Usuario Criação',
			'usuario_atualizacao' => 'Usuario Atualização',
			'nome' => 'Nome',
			'cargo' => 'Cargo',
			'setor' => 'Setor',
			'ddd_tel_comercial' => 'DDD',
			'tel_comercial' => 'Tel Principal',
			'tel_comercial_ramal' => 'Ramal',
			'ddd_tel_outro' => 'DDD',
			'tel_outro' => 'Tel Secundário',
			'tel_outro_ramal' => 'Ramal',
			'ddd_cel_comercial' => 'DDD',
			'cel_comercial' => 'Cel Principal',
			'ddd_cel_outro' => 'DDD',
			'cel_outro' => 'Cel Secundário',
			'email' => 'Email Principal',
			'email_outro' => 'Email Secundário',
			'contato_administrativo' => 'Contato Administrativo',
			'contato_comercial' => 'Contato Comercial',
			'contato_financeiro' => 'Contato Financeiro',
			'contato_tecnico' => 'Contato Tecnico',
			'empresa_id' => 'Empresa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_criacao',$this->data_criacao,true);
		$criteria->compare('data_atualizacao',$this->data_atualizacao,true);
		$criteria->compare('usuario_criacao',$this->usuario_criacao);
		$criteria->compare('usuario_atualizacao',$this->usuario_atualizacao);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('cargo',$this->cargo,true);
		$criteria->compare('setor',$this->setor,true);
		$criteria->compare('ddd_tel_comercial',$this->ddd_tel_comercial);
		$criteria->compare('tel_comercial',$this->tel_comercial,true);
		$criteria->compare('tel_comercial_ramal',$this->tel_comercial_ramal);
		$criteria->compare('ddd_tel_outro',$this->ddd_tel_outro);
		$criteria->compare('tel_outro',$this->tel_outro,true);
		$criteria->compare('tel_outro_ramal',$this->tel_outro_ramal);
		$criteria->compare('ddd_cel_comercial',$this->ddd_cel_comercial);
		$criteria->compare('cel_comercial',$this->cel_comercial,true);
		$criteria->compare('ddd_cel_outro',$this->ddd_cel_outro);
		$criteria->compare('cel_outro',$this->cel_outro,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('email_outro',$this->email_outro,true);
		$criteria->compare('contato_administrativo',$this->contato_administrativo);
		$criteria->compare('contato_comercial',$this->contato_comercial);
		$criteria->compare('contato_financeiro',$this->contato_financeiro);
		$criteria->compare('contato_tecnico',$this->contato_tecnico);
		$criteria->compare('empresa_id',$this->empresa_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contato the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
         /**
         * Prepare usuario_criacao and usuario_atualizacao attributes
         * before saving.
         */
        protected function beforeSave()
        {
            if(null !== Yii::app()->user)
                $id=Yii::app()->user->id;
            else
                $id=1;
            
            if ($this->isNewRecord)
                $this->usuario_criacao=$id;
            
            $this->usuario_atualizacao=$id;
            
            return parent::beforeSave();
        }
        
        /**
         * Attaches the timestamp behavior to update the
         * create and update times
         */
        public function behaviors()
        {
            return array(
                'CTimestampBehavior' => array(
                    'class'=>'zii.behaviors.CTimestampBehavior',
                    'createAttribute'=>'data_criacao',
                    'updateAttribute'=>'data_atualizacao',
                    'setUpdateOnCreate'=> true,
                )
            );
        }
}
