<?php

/**
 * This is the model class for table "tbl_itens_servico".
 *
 * The followings are the available columns in table 'tbl_itens_servico':
 * @property integer $id
 * @property integer $orcamento_servico_id
 * @property string $descricao
 * @property integer $servico_id
 * @property string $valor
 * @property string $quantidade
 *
 * The followings are the available model relations:
 * @property OrcamentoServico $orcamentoServico
 * @property Servico $servico
 */
class ItensServico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_itens_servico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orcamento_servico_id', 'required'),
			array('orcamento_servico_id, servico_id', 'numerical', 'integerOnly'=>true),
			array('valor', 'length', 'max'=>6),
			array('quantidade', 'length', 'max'=>10),
			array('descricao', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, orcamento_servico_id, descricao, servico_id, valor, quantidade', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orcamentoServico' => array(self::BELONGS_TO, 'OrcamentoServico', 'orcamento_servico_id'),
			'servico' => array(self::BELONGS_TO, 'Servico', 'servico_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'orcamento_servico_id' => 'Orcamento Servico',
			'descricao' => 'Descricao',
			'servico_id' => 'Servico',
			'valor' => 'Valor',
			'quantidade' => 'Quantidade',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('orcamento_servico_id',$this->orcamento_servico_id);
		$criteria->compare('descricao',$this->descricao,true);
		$criteria->compare('servico_id',$this->servico_id);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('quantidade',$this->quantidade,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ItensServico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
