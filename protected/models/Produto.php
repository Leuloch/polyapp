<?php

/**
 * This is the model class for table "tbl_produto".
 *
 * The followings are the available columns in table 'tbl_produto':
 * @property integer $id
 * @property string $nome_produto
 * @property string $codigo_produto
 * @property string $valor
 * @property string $custo
 * @property integer $instrumento_id
 * @property string $codigo_EAN
 * @property integer $ncm
 * @property integer $unidade_medida
 * @property string $peso_liquido
 * @property string $peso_bruto
 * @property integer $fabricante
 *
 * The followings are the available model relations:
 * @property Empresa[] $tblEmpresas
 * @property Fabricante $fabricante0
 * @property Instrumento $instrumento
 * @property RegraTributacaoEmpresa[] $regraTributacaoEmpresas
 * @property RegraTributacaoEstado[] $regraTributacaoEstados
 */
class Produto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_produto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome_produto', 'required'),
			array('instrumento_id, ncm, unidade_medida, fabricante', 'numerical', 'integerOnly'=>true),
			array('nome_produto', 'length', 'max'=>100),
			array('codigo_produto', 'length', 'max'=>45),
			array('valor, custo', 'length', 'max'=>6),
			array('codigo_EAN', 'length', 'max'=>14),
			array('peso_liquido, peso_bruto', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome_produto, codigo_produto, valor, custo, instrumento_id, codigo_EAN, ncm, unidade_medida, peso_liquido, peso_bruto, fabricante', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tblEmpresas' => array(self::MANY_MANY, 'Empresa', 'tbl_produto_cliente_preco(produto_id, empresa_id)'),
			'fabricante0' => array(self::BELONGS_TO, 'Fabricante', 'fabricante'),
			'instrumento' => array(self::BELONGS_TO, 'Instrumento', 'instrumento_id'),
			'regraTributacaoEmpresas' => array(self::HAS_MANY, 'RegraTributacaoEmpresa', 'produto_id'),
			'regraTributacaoEstados' => array(self::HAS_MANY, 'RegraTributacaoEstado', 'produto_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome_produto' => 'Nome Produto',
			'codigo_produto' => 'Codigo Produto',
			'valor' => 'Valor',
			'custo' => 'Custo',
			'instrumento_id' => 'Instrumento',
			'codigo_EAN' => 'Codigo Ean',
			'ncm' => 'Ncm',
			'unidade_medida' => 'Unidade Medida',
			'peso_liquido' => 'Peso Liquido',
			'peso_bruto' => 'Peso Bruto',
			'fabricante' => 'Fabricante',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome_produto',$this->nome_produto,true);
		$criteria->compare('codigo_produto',$this->codigo_produto,true);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('custo',$this->custo,true);
		$criteria->compare('instrumento_id',$this->instrumento_id);
		$criteria->compare('codigo_EAN',$this->codigo_EAN,true);
		$criteria->compare('ncm',$this->ncm);
		$criteria->compare('unidade_medida',$this->unidade_medida);
		$criteria->compare('peso_liquido',$this->peso_liquido,true);
		$criteria->compare('peso_bruto',$this->peso_bruto,true);
		$criteria->compare('fabricante',$this->fabricante);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Produto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
