<?php

Yii::import('application.models._base.BaseOrderItems');

class OrderItems extends BaseOrderItems
{
        /**
         *
         * @var attribute The variable to use with de extension DynamicTabularInput 
         */
        public $updateType;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}