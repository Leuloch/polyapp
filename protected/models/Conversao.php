<?php

/**
 * This is the model class for table "tbl_conversao".
 *
 * The followings are the available columns in table 'tbl_conversao':
 * @property integer $id
 * @property integer $unidade_id_de
 * @property integer $unidade_id_para
 * @property double $fator_conversao
 *
 * The followings are the available model relations:
 * @property Unidade $unidadeIdDe
 * @property Unidade $unidadeIdPara
 */
class Conversao extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_conversao';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, unidade_id_de, unidade_id_para, fator_conversao', 'required'),
			array('id, unidade_id_de, unidade_id_para', 'numerical', 'integerOnly'=>true),
			array('fator_conversao', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, unidade_id_de, unidade_id_para, fator_conversao', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unidadeIdDe' => array(self::BELONGS_TO, 'Unidade', 'unidade_id_de'),
			'unidadeIdPara' => array(self::BELONGS_TO, 'Unidade', 'unidade_id_para'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unidade_id_de' => 'Unidade Id De',
			'unidade_id_para' => 'Unidade Id Para',
			'fator_conversao' => 'Fator Conversao',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('unidade_id_de',$this->unidade_id_de);
		$criteria->compare('unidade_id_para',$this->unidade_id_para);
		$criteria->compare('fator_conversao',$this->fator_conversao);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Conversao the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
