<?php

/**
 * This is the model class for table "tbl_servico".
 *
 * The followings are the available columns in table 'tbl_servico':
 * @property integer $id
 * @property string $nome_servico
 * @property integer $instrumento_id
 * @property string $custo
 * @property string $valor
 *
 * The followings are the available model relations:
 * @property Instrumento $instrumento
 * @property Empresa[] $tblEmpresas
 */
class Servico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_servico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome_servico', 'required'),
			array('instrumento_id', 'numerical', 'integerOnly'=>true),
			array('nome_servico', 'length', 'max'=>255),
			array('custo, valor', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome_servico, instrumento_id, custo, valor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'instrumento' => array(self::BELONGS_TO, 'Instrumento', 'instrumento_id'),
			'tblEmpresas' => array(self::MANY_MANY, 'Empresa', 'tbl_servico_empresa_preco(servico_id, empresa_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome_servico' => 'Nome Servico',
			'instrumento_id' => 'Instrumento',
			'custo' => 'Custo',
			'valor' => 'Valor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome_servico',$this->nome_servico,true);
		$criteria->compare('instrumento_id',$this->instrumento_id);
		$criteria->compare('custo',$this->custo,true);
		$criteria->compare('valor',$this->valor,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Servico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
